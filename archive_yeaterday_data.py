from weather_0_3_2 import get_weather_data, calculate_process
from thunder_0_1_2 import get_thunder_data, process_thunder_data
from utility_0_1_3 import ten_secs_countdown
import datetime
import os
clear = lambda: os.system('cls' if os.name == 'nt' else 'clear')

def main(n_days_before:int = None):
    Yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    if n_days_before is not None:
        Yesterday = datetime.datetime.now() - datetime.timedelta(days=n_days_before)
    already_done = False
    success = False
    retry_count = 0
    while(not success):
        try:
            success = get_thunder_data(Yesterday)
            if success:
                already_done = process_thunder_data(Yesterday) == 1
        except Exception as ex:
            print(ex.__str__())
            ten_secs_countdown()
    ten_secs_countdown()
    clear()

    success = False
    while(not success):
        try:
            success = get_weather_data(Yesterday)
            if success:
                already_done = calculate_process(Yesterday) == 1
        except Exception as ex:
            print(ex.__str__())
            ten_secs_countdown()
    ten_secs_countdown()
    return already_done

if __name__.__eq__("__main__"):
    main()

