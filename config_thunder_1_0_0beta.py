import pathlib

from config_1_1_6 import *


class Switches(Switches):
    save_render_data = False


class Name(Name):
    """
    Refer config file for more detailed explanation
    * unless it needs a override configuration, use the same value as in config
    """
    thunder_report_template_filename = 'thunder_report_template.docx'
    thunder_data_file = '*.geojson'
    thunder_mxd_template = "thunder.mxd"

    def get_thunder_nc_report_name(self, specific_time=None):
        pre_defined_name = '{0}年{1}月{2}日{3}时{4}分未来两小时昭通曲靖雷电.docx'
        if specific_time is None:
            time = datetime.datetime.now()
        elif not isinstance(specific_time, datetime.datetime):
            time = datetime.datetime.now()
            print(f"ERROR type of given time <{type(specific_time)}> is not <{type(datetime.datetime)}>, "
                  f"using current time instead")
        else:
            time = specific_time
        return pre_defined_name.format(time.year, time.month, time.day, time.hour, time.minute)


class Directory(Directory):
    expected_remote_thunder_data_dir_w_date_format = "JT7J2UKR/{0}/"
    thunder_report_extract_directory = str(pathlib.Path(Directory.temporary_directory).joinpath('thunder_docx'))
    thunder_mxd_template_file_path = str(pathlib.Path(Directory.reference_directory).joinpath(
        'mxds', 'thunder', Name.thunder_mxd_template
    ))

    def get_expected_remote_thunder_data(self, date):
        if isinstance(date, datetime.datetime):
            return self.expected_remote_thunder_data_dir_w_date_format.format(
                datetime.datetime.strftime(date, CannotClassify.remote_directory_date_format)
            )
        else:
            print(f"ERROR given date must be an instance of datetime\n"
                  f"\texpected {type(datetime.datetime)}, got {type(date)}")

    def get_thunder_geojson_directory(self, date):
        if isinstance(date, datetime.datetime):
            sub_path = 'thunder_geojson/{0}/{1}'.format(
                datetime.datetime.strftime(date, CannotClassify.year_format),
                datetime.datetime.strftime(date, CannotClassify.month_day_format)
            )
            path = os.path.join(self.input_directory, sub_path)
            if not os.path.exists(path):
                os.makedirs(path)
            return path
        else:
            print(f"ERROR given date must be an instance of datetime\n"
                  f"\texpected {type(datetime.datetime)}, got {type(date)}")

    def get_thunder_geojson_report_path(self, date):
        if isinstance(date, datetime.datetime):
            sub_path = 'thunder_geojson_report/{0}/{1}'.format(
                datetime.datetime.strftime(date, CannotClassify.year_format),
                datetime.datetime.strftime(date, CannotClassify.month_day_format)
            )
            path = os.path.join(self.output_directory, sub_path)
            if not os.path.exists(path):
                os.makedirs(path)
            return path
        else:
            print(f"ERROR given date must be an instance of datetime\n"
                  f"\texpected {type(datetime.datetime)}, got {type(date)}")

    def get_thunder_mxd_template_directory(self):
        return os.path.join(Directory.reference_directory, 'mxds/thunder')


class ReportProperties(ReportProperties):
    class Thunder:
        class EmptyReport:
            title = '云南电网短时雷电预警预报'
            location_string = '云南省昭通曲靖地区'
            time_string = '{}年{}月{}日{}时{}分'
            release_time_string = "发布时间：{}年{}月{}日{}时{}分"
            content_string = '未来两小时无雷电预测区域。'

            def get_formatted_release_time_string(self, date: datetime.datetime) -> str:
                return self.release_time_string.format(date.year, date.month, date.day, date.hour, date.minute)

            def get_formatted_time_string(self, date: datetime.datetime) -> str:
                return self.time_string.format(date.year, date.month, date.day, date.hour, date.minute)
