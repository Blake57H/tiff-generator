import sys
import version_checker
if not version_checker.VersionChecker().compare(sys.version_info):
    exit(-1)

import os.path
from shutil import copy

from ftp_file_fetcher_0_2_3 import FTPLoginInfo
from ftp_file_fetcher_0_2_3 import main_process as ftp_download_process
from utility_0_1_3 import *
from config_thunder_1_0_0beta import *
from docx import Document

"""
This python script is developed using python version 3.9.6 64bit

Package used in this script and related scripts are listed as follows:
(* at the time of doing thunder_0_1_1.py)
(PS if my memory serve me correct, these should be the necessary package)
zip-files==0.4.1
geopandas==0.9.0
Fiona @ file:///C:/path_to/Fiona-1.8.20-cp39-cp39-win_amd64.whl

changes (compared to 0.1.1)
adding empty report generate for empty geojson data
"""


def create_empty_report(date: datetime.datetime, save_directory: str, file_name: str) -> None:
    full_path = pathlib.Path(save_directory).joinpath(file_name)
    empty_report_config = ReportProperties.Thunder.EmptyReport()
    report = Document()
    report.add_heading(empty_report_config.title, level=0)
    report.add_paragraph(empty_report_config.get_formatted_release_time_string(date))
    report.add_paragraph(
        empty_report_config.location_string +
        empty_report_config.get_formatted_time_string(date) +
        empty_report_config.content_string
    )
    report.save(full_path.__str__())
    print_success_text(f"empty report [{file_name}] saved")


def get_thunder_data(date=datetime.datetime.now(), local_path: str = None, remote_path: str = None) -> bool:
    """
    download thunder data from a certain ftp server
    leave everything as default would be just fine
    :param date: specify a date which data's date should be, default is [now]
    :param local_path: where data should be downloaded to, default is a subfolder in [source data] as config file says
    :param remote_path: where data should be downloaded from, refer config file for default directory
    :return: Nope
    """
    login_info = FTPLoginInfo(
        port=CannotClassify.ftp_port,
        address=CannotClassify.ftp_address,
        username=CannotClassify.ftp_username,
        password=CannotClassify.ftp_password
    )
    # date = date - datetime.timedelta(days=20)  # test only
    dir_set = Directory()
    if not isinstance(date, datetime.datetime):
        print(f"date format incorrect, expect {type(datetime.datetime)}, got {type(date)}")
        return False
    else:
        if remote_path is None:
            remote_path = dir_set.get_expected_remote_thunder_data(date)
        if local_path is None:
            local_path = dir_set.get_thunder_geojson_directory(date)
        return ftp_download_process(local_path, login_info, remote_path)


def process_thunder_data(date=datetime.datetime.now()) -> int:
    # return value: 0 = all data processed; 1 = all data are already processed
    return_value = 1
    geojson_directory = Directory().get_thunder_geojson_directory(date)
    temp_directory = os.path.join(Directory.temporary_directory, 'thunder_shp_temp')
    output_directory = Directory().get_thunder_geojson_report_path(date)
    if not os.path.exists(temp_directory):
        os.makedirs(temp_directory)
    geojson_files = glob.glob(os.path.join(geojson_directory, Name.thunder_data_file))
    counter = 0
    counter_max = len(geojson_files)
    for geojson_file in geojson_files:
        counter += 1
        print(f"\nProgress: {counter} of {counter_max} in progress")
        data_date_str = geojson_file.split('_')[-1:][0].replace(Name.thunder_data_file[1:], '')
        data_datetime = datetime.datetime.strptime(data_date_str, CannotClassify.thunder_data_datetime_format)
        report_name = Name().get_thunder_nc_report_name(data_datetime)
        report_path = Directory().get_thunder_geojson_report_path(data_datetime)
        has_things_to_do = Switches.save_render_data or Switches.save_png_image or Switches.save_input_data
        if not has_things_to_do:
            if Switches.need_report:
                if give_warning_of_existed_file(
                        os.path.join(report_path, report_name)) and not Switches.ignore_existing_report:
                    print_info_text(f"report for dataset [{geojson_file}] exists, ignore and process next dataset")
                    continue
            else:
                print_warning_text(f"Current processing settings will do nothing to dataset [{geojson_file}]. "
                                   f"Check your config and try again.")
                exit(-1)
        return_value = 0
        shape_file_name = geojson_to_shape_converter_single(geojson_file, None, temp_directory)
        if shape_file_name is None:
            print_error_text("shape file not created")
            continue
        elif shape_file_name.__eq__(''):
            print_info_text("geojson data has no data, thus shape file will not be created")
            if Switches.need_report and Switches.create_empty_report:
                if give_warning_of_existed_file(os.path.join(report_path, report_name)):
                    if not Switches.ignore_existing_report:
                        print_info_text(f"[{report_name}] exists, report generate skipped")
                        continue
                    else:
                        print_warning_text(f"[{report_name}] exists, report will be over written")
                create_empty_report(data_datetime, report_path, report_name)
                if Switches.convert_pdf:
                    docx_to_pdf(os.path.join(report_path, report_name))
            continue
        shape_file_full_path = os.path.join(temp_directory, shape_file_name)
        shape_file_abs_path = os.path.abspath(shape_file_full_path)
        png_filename = shape_file_name.replace(shape_file_name.split('.')[-1:][0], Name.png_file_ext)
        png_filepath = temp_directory
        png_file_full_path = os.path.join(png_filepath, png_filename)
        mxd_path = os.path.abspath(
            os.path.join(Directory().get_thunder_mxd_template_directory(), Name.thunder_mxd_template)
        )
        r_code = arcpy_mxd_to_png_helper(mxd_absolute_path=mxd_path,
                                         input_file_absolute_path=[shape_file_abs_path],
                                         image_output_absolute_path=os.path.abspath(png_file_full_path),
                                         replace_index=[1])
        if Switches.save_render_data or Switches.save_input_data:
            print_info_text(f"Saving [{shape_file_name}]...")
            geojson_to_shape_converter_single(geojson_file, None, output_directory)
        if Switches.save_png_image:
            if r_code != 0 or not os.path.exists(png_file_full_path):
                print_error_text(f"cannot save image, png export failed or png not exist")
            else:
                print_info_text(f"Saving [{png_filename}]...")
                copy(png_file_full_path, os.path.join(output_directory, png_filename))
        if Switches.need_report:
            if r_code != 0:
                print_error_text(f"png export process did not finish correctly, abort report output")
            elif give_warning_of_existed_file(
                    os.path.join(report_path, report_name)) and not Switches.ignore_existing_report:
                print_info_text(f"[{report_name}] exists, report generate skipped")
            else:
                if give_warning_of_existed_file(os.path.join(report_path, report_name)):
                    print_warning_text(f"[{report_name}] exists, report will be over written")
                extracted_report_directory = load_docx_template(
                    os.path.join(Directory.reference_directory, Name.thunder_report_template_filename)
                )
                report_image_replace(png_file_full_path, 'image2.png', extracted_report_directory)
                thunder_day_update(extracted_report_directory, data_datetime)
                zip_docx_report(report_path, report_name, extracted_report_directory)
                clean_temporary_folder(extracted_report_directory)
                if Switches.convert_pdf:
                    docx_to_pdf(os.path.join(report_path, report_name))
    if len(geojson_files) == 0:
        print_info_text("No file to process")
    clean_temporary_folder(temp_directory)
    return return_value


def main_process(date:datetime.datetime=datetime.datetime.now()) -> int:
    print("thunder is ready and get set go")
    if not get_thunder_data(date):
        print_warning_text("thunder data download process has encountered some error")
    return_value: int = process_thunder_data(date)
    print("and stop")
    return return_value


if __name__ == "__main__":
    main_process()
    ten_secs_countdown_exit()
