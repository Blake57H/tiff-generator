import datetime
import ftplib
import os.path
import pathlib
from abc import ABC
from ftplib import FTP
from typing import Iterator

import dateutil.parser

from config_1_1_6 import CannotClassify
from utility_0_1_3 import print_error_text, print_warning_text, print_info_text, print_success_text

"""
some code are copied from FTP_download.py by [__author__ = "Maylon"]
"""


# ftp连接
def ftp_connect(ftp_address, port, username, password, timeout=60) -> FTP or None:
    """
    参数：
        ip_addr：ip地址
        port：端口号
        username：用户名
        password：密码
        timeout: seconds to wait before aborting action (avoid process hang)
    """
    if timeout is None or not isinstance(timeout, int):
        ftp = FTP()
    else:
        ftp = FTP(timeout=timeout)
    try:
        print(f"Connecting to {ftp_address}....", end="", flush=True)
        ftp.connect(ftp_address, port)  # 连接FTP服务器
        print_success_text("Connected")
        print(ftp.getwelcome())
        print(f"Logging in as {username}....", end="", flush=True)
        ftp.login(username, password)  # 登录
        print_success_text("Logged in")
        ftp.sendcmd('epsv')  # for some reason that server doesn't like list command without passive set
        return ftp
    except Exception as e:
        print_error_text(f"Connect to {ftp_address} failed, reason: {e}")
        return None  # return NONE when connection failed


retries_count = 0
retries_max = CannotClassify.max_retries


# above are old codes copied from one of my senpai...
# unused code are deleted
# ===================SPLITER===================
# below are new codes by me (use MLSD to reduce time in size check)

class FTPLoginInfo:
    def __init__(self, address: str, port: int = 21, username: str = 'anonymous', password: str = ''):
        self.port = port
        self.address = address
        self.username = username
        self.password = password


def size_formatter(size: int) -> str:
    """
    give a file size in byte, convert it to a more readable format
    :param size: int size in byte
    :return: string, like 10MB, 10B, 10 KB...
    """
    suffix = ['B', 'KB', 'MB', 'GB', 'TB']
    index = 0
    while index < len(suffix) - 1 and size > 1024:
        size = round(size / 1024, ndigits=2)
        index += 1
    return '{0}{1}'.format(str(size), suffix[index])


class FtpRemoteFileInfo(ABC):
    def __init__(self, file_name: str, file_type: str, file_size: str, file_modify: datetime.datetime or None):
        self.file_name: str = file_name
        self.file_type: str = file_type
        self.file_size: str = file_size
        self.file_modify: datetime.datetime = file_modify

    class Constant:
        file_type = 'type'
        file_size = 'size'
        file_modify = 'modify'
        file_type_directory = 'dir'
        file_type_file = 'file'

    def get_name(self) -> str:
        return self.file_name

    def get_modify(self) -> datetime.datetime:
        return self.file_modify

    def get_type(self) -> str:
        return self.file_type

    def get_size(self) -> str or None:
        """
        return ftp remote file size, return None if given 'file' is a directory
        :return: size string or None
        """
        return self.file_size

    def check_if_is_directory(self) -> bool:
        return self.file_type == self.Constant.file_type_directory

    def check_if_same_modify(self, modify: datetime.datetime) -> bool:
        """
        check if given file/directory object has the same modify time
        difference less than 1 second is considered 'same'
        :param modify: given file/directory
        :return: is same?
        """
        return abs((self.file_modify - modify).total_seconds()) < 1


class FtpRemoteFileInfoMLSD(FtpRemoteFileInfo):
    def __init__(self, info: tuple[str, dict[str, str]]):
        file_name: str = info[0]
        file_type: str = info[1][self.Constant.file_type]
        if self.Constant.file_type_directory == file_type:
            file_size = None
        elif self.Constant.file_type_file == file_type:
            file_size = info[1][self.Constant.file_size]
        else:
            raise print_error_text(f"unknown object type: {file_type}")
        try:
            file_modify: datetime.datetime = dateutil.parser.parse(info[1][self.Constant.file_modify])
        except Exception as ex:
            raise print_error_text(f"Cannot set file/directory modify, reason: {ex}")
        super().__init__(file_name=file_name, file_type=file_type, file_size=file_size, file_modify=file_modify)


class FtpRemoteFileInfoLIST(FtpRemoteFileInfo):
    def __init__(self, info: str):
        info = info.split()
        file_name: str = info[-1]
        file_type: str
        file_size: str or None
        if info[0].startswith('d'):
            file_type = self.Constant.file_type_directory
            file_size = None
        else:
            file_type = self.Constant.file_type_file
            file_size = info[4]
        file_modify = f"{info[5]} {info[6]} {info[7]}"
        file_modify = dateutil.parser.parse(file_modify)
        super().__init__(file_name=file_name, file_type=file_type, file_size=file_size, file_modify=file_modify)


def size_formatter_new(size: str) -> str:
    """
    same as size formatter but take string as input
    TODO: I know it is duplicated, because it does the exact same thing as size_formatter(), but for now I am too lazy to fix it
    example:
        before: 587186809 B,
        after: 559 MB
    isn't it more readable?
    :param size: string size value in byte
    :return: a more readable size string
    """
    suffix = ['B', 'KB', 'MB', 'GB', 'TB']
    index = 0
    if not (str.isdigit(size)):
        raise RuntimeError(f"invalid file size type, expect {type(str)}, got {type(size)}")
    size = int(size)
    while index < len(suffix) - 1 and size > 1024:
        size = round(size / 1024, ndigits=2)
        index += 1
    return '{0}{1}'.format(str(size), suffix[index])


def check_file_is_same(remote_file_info: FtpRemoteFileInfo, local_file_full_path: str) -> bool:
    """
    check remote file and local file's property and see if there is any difference in modify time and size as well as file existence
    :param remote_file_info: an item of ftplib.mlst or ftplib.dir results
    :param local_file_full_path:
    :return: True if modify and size is the same, or otherwise
    """
    if not pathlib.Path(local_file_full_path).exists():
        return False
    local_file_stat = pathlib.Path(local_file_full_path).stat()
    local_file_size = str(local_file_stat.st_size)
    local_file_modify = datetime.datetime.fromtimestamp(local_file_stat.st_mtime)
    # print(f"local: size={local_file_size}, modify={local_file_modify}, sec={local_file_modify.second}")
    remote_file_size = remote_file_info.get_size()
    # print(f"remote: size={remote_file_size}, modify={remote_file_modify}, sec={remote_file_modify.second}")
    if remote_file_info.check_if_is_directory() or not pathlib.Path(local_file_full_path).is_file():
        print_error_text("remote path or local path is not a file")
        return False
    # print((remote_file_modify - local_file_modify).total_seconds())
    is_modify_same = remote_file_info.check_if_same_modify(local_file_modify)
    return remote_file_size == local_file_size and is_modify_same


def download_file_new(remote_info: FtpRemoteFileInfo, remote_full_path: str, local_full_path: str,
                      ftp_object: ftplib.FTP) -> bool:
    remote_file_size_txt = size_formatter_new(remote_info.get_size())
    remote_file_name = remote_info.get_name()
    with open(local_full_path, 'wb') as local_file_stream:
        global progress
        progress = 0

        def write_data(data):
            global progress
            local_file_stream.write(data)
            progress += len(data)
            print(
                f"\rDownloading：{remote_file_name}...({size_formatter(progress)}/{remote_file_size_txt})",
                end=""
            )

        ftp_object.retrbinary(
            f'RETR {remote_full_path}',
            write_data,
            CannotClassify.ftp_download_buff_size)
    if remote_info.get_modify() is not None:
        os.utime(local_full_path, (remote_info.get_modify().timestamp(), remote_info.get_modify().timestamp()))
    if check_file_is_same(remote_info, local_full_path):
        print_success_text(f"\rDownloading：{remote_info.get_name()}...downloaded")
        return True
    else:
        print_error_text(f"\rDownloading：{remote_info.get_name()}...file check failed")
        return False


def new_way(local_path_to_download: str,
            login_info_obj: FTPLoginInfo,
            remote_path_to_download: str,
            include_parent_dir: bool = False
            ) -> int:
    print("ftp file fetcher starting")
    download_status = DownloaderReturnCode()

    # make sure login info is valid
    if not isinstance(login_info_obj, FTPLoginInfo):
        print_error_text(f"invalid login info, expected {type(FTPLoginInfo)} got {type(login_info_obj)}")
        return False
    print(f"FTP address: {login_info_obj.address}:{login_info_obj.port}\n"
          f"Login username: {login_info_obj.username}")

    # make an connection, with limited retries
    max_retries_loop = max(retries_max, 1)
    ftp_object = None
    for i in range(max_retries_loop):
        if i > 0:
            print_warning_text(f"Retrying ({i}/{max_retries_loop - 1})....")
        ftp_object = ftp_connect(ftp_address=login_info_obj.address, port=login_info_obj.port,
                                 username=login_info_obj.username, password=login_info_obj.password)
        if ftp_object is not None:
            break

    mlsd_yes = False  # does server support MLSD?
    if ftp_object is not None:

        features: list[str] = list()
        try:
            feats = ftp_object.sendcmd('feat')  # get a list of supported command from server
            feats = feats.split('\n')
            for feat in feats:
                feat = feat.strip()
                if feat[:3].__eq__('211'):
                    continue  # ignore server message code
                features.append(feat)
                if 'MLSD' in feat.upper():
                    mlsd_yes = True
        except Exception as ex:
            raise print_error_text(f"fatal error: {ex}")

        print(f"Server features: {features}")
        if not mlsd_yes:
            print_warning_text("This server does not support 'MLSD' command, download process would be slower")

        # create a parent folder in download directory (on demand)
        if include_parent_dir:
            parent, child = os.path.split(remote_path_to_download)
            if child != '':
                child = child.strip('/').strip('\\')
                parent = parent.strip('/').strip('\\')
                is_success = True
                for tries in range(max(retries_max, 1)):
                    try:
                        # if mlsd is not available
                        if not mlsd_yes:
                            temp1 = list()
                            ftp_object.dir(parent, lambda x: temp1.append(x.split))
                            for item in temp1:
                                temp = FtpRemoteFileInfoLIST(item)
                                if temp.get_name() == child:
                                    if temp.check_if_is_directory():
                                        local_path_to_download = os.path.join(local_path_to_download, parent)
                                    else:
                                        local_path_to_download = os.path.join(local_path_to_download, child)
                        # if mlsd is available
                        else:
                            # this part may get deleted because the above works for all ftp servers
                            # to me mlsd is a better and easy to manage solution compared to the above
                            temp1 = ftp_object.mlsd(parent)
                            for item in temp1:
                                temp = FtpRemoteFileInfoMLSD(info=item)
                                if temp.get_name() == child:
                                    if temp.check_if_is_directory():
                                        local_path_to_download = os.path.join(local_path_to_download, parent)
                                    else:
                                        local_path_to_download = os.path.join(local_path_to_download, child)
                        '''
                        # directories will be created when downloading files, making this part seemed redundant
                        if not os.path.exists(local_path_to_download):
                            print_info_text(f"Creating download directory [{local_path_to_download}]...")
                            os.makedirs(local_path_to_download)
                            is_success = True
                        '''
                        break
                    except ftplib.error_perm as er:
                        print_error_text(f"An error occurred: {er}")
                        is_success = False
                if not is_success:
                    print_error_text("max retries reached while finding parent folder")
                    download_status.update_status(DownloaderStatus.UNKNOWN_ERROR)
                    return download_status.to_int()

        download_list: list[str] = list()  # directories needed to be downloaded as a list
        download_list.append(remote_path_to_download)

        # this download process uses stack, while my senpai's uses nested functions
        while len(download_list) > 0:
            object_to_download = download_list.pop()

            # download item with limited retries
            for tries in range(max_retries_loop):
                remote_file_info_list: list[FtpRemoteFileInfo] = list()
                if tries > 0:
                    print_warning_text(f"Retrying.... ({tries}/{max_retries_loop-1})")
                try:
                    name_list = ftp_object.nlst(object_to_download)
                    if mlsd_yes:
                        better_name_list: Iterator[tuple[str, dict[str, str]]] = ftp_object.mlsd(object_to_download)
                        for item in better_name_list:
                            remote_file_info_list.append(FtpRemoteFileInfoMLSD(item))
                    else:
                        better_name_list: list[str] = list()
                        ftp_object.dir(object_to_download, lambda x: better_name_list.append(x))
                        for item in better_name_list:
                            remote_file_info_list.append(FtpRemoteFileInfoLIST(item))
                except ftplib.error_perm as resp:
                    if "550" in str(resp):
                        print_warning_text(f"given file or directory not found: {object_to_download}")
                        download_status.update_status(DownloaderStatus.OBJECT_NOT_EXIST)
                        break
                    else:
                        print_error_text(f"Ftp fatal error, reason: {resp}")
                        download_status.update_status(DownloaderStatus.UNKNOWN_ERROR)
                        continue
                except Exception as ex:
                    print_error_text(f"Ftp fatal error, reason: {ex}")
                    download_status.update_status(DownloaderStatus.UNKNOWN_ERROR)
                    continue

                # get relative path (relative to download target)

                relative_path = object_to_download.replace(remote_path_to_download, '').strip('/').strip('\\')
                file_count = len(name_list)
                if file_count == 0:
                    print_warning_text(f"[{object_to_download}] is empty directory or not exist")
                    break
                for remote_file_info in remote_file_info_list:
                    the_name = remote_file_info.get_name()

                    if remote_file_info.check_if_is_directory():
                        # if object is a path, append it to download list
                        path = os.path.join(remote_path_to_download, relative_path, the_name)
                        if not os.path.exists(path):
                            os.makedirs(path)
                        download_list.append(path)
                    else:
                        # if object is a file, proceed to download
                        remote_file = os.path.join(remote_path_to_download, relative_path, the_name)
                        local_file = os.path.join(local_path_to_download, relative_path, the_name)
                        if check_file_is_same(remote_file_info, local_file):
                            # of course we don't need to download if that file is already downloaded
                            print_info_text("An existing file already in local directory, skipped download")
                            continue

                        # download a file with limited retries
                        is_success = False
                        for retry in range(max_retries_loop):
                            if retry > 0:
                                print_warning_text(f"retrying... {retry}/{max_retries_loop}")
                            if download_file_new(
                                    remote_info=remote_file_info,
                                    remote_full_path=remote_file,
                                    local_full_path=local_file,
                                    ftp_object=ftp_object
                            ):
                                is_success = True
                                break

                        if not is_success:
                            download_status.update_status(DownloaderStatus.DOWNLOAD_UNSUCCESSFUL)
                            print_error_text(f"max retries reached")
                if not download_status.to_boolean():
                    print_warning_text("some error occurred during download")
                else:
                    break
            if not download_status.to_boolean():
                print_error_text("max retries reached")
    else:
        print_error_text("An error occurred while communicating with the server")
        download_status.update_status(DownloaderStatus.CONNECTION_UNSUCCESSFUL)
    print("ftp file fetcher stopping")
    return download_status.to_int()


def main_process(local_path_to_download, login_info_obj, remote_path_to_download,
                 include_parent_directory=False) -> bool:
    return new_way(local_path_to_download, login_info_obj, remote_path_to_download, include_parent_directory) < 0
    # return old_way(local_path_to_download, login_info_obj, remote_path_to_download)


class DownloaderStatus:
    """
    prefer non negative value for non error return code, and negative value for error return code
    """
    UNKNOWN_ERROR: int = -1  # an unknown error occurred
    DOWNLOAD_SUCCESSFUL: int = 0  # no error
    OBJECT_NOT_EXIST: int = 1  # given object not exist (it is not considered an error here because for weather/thunder component it will ignore this error)
    DOWNLOAD_UNSUCCESSFUL: int = -2  # failed to download a file (timeout, connection error or something when downloading a file. it is considered an error)
    CONNECTION_UNSUCCESSFUL: int = -3  # failed to communicate to the server (timeout, connection error or something when connecting to the server, considered an error)


class DownloaderReturnCode:
    def __init__(self):
        self.status = DownloaderStatus.DOWNLOAD_SUCCESSFUL

    def update_status(self, status: int) -> None:
        if status < 0:
            self.status = status
        elif self.status >= 0 and status > 0:
            self.status = status

    def to_boolean(self) -> bool:
        if self.status >= 0:
            return True
        else:
            return False

    def to_int(self) -> int:
        return self.status


progress = 0


if __name__ == "__main__":
    print("i donno what to download so have a nice day ;)")
