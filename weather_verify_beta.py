from ftp_file_fetcher_0_2_2 import main_process as ftp_download_process
from ftp_file_fetcher_0_2_2 import FTPLoginInfo
from utility_0_1_3 import *
from config_weather_verify_beta import *
import numpy as np
import numpy
import netCDF4
import re
import json
import sys
from osgeo import osr
from typing import Union

# todo: though functions shouldn't be called before import, but I don't think I have a choice (Any suggestions?)
# todo: only wanna make sure my "import __future__" in utility is working (require 3.7 or above)
import version_checker

version_checker.VersionChecker().compare(sys.version_info)

master_data_property = DataProperty.WeatherStationPlainText()
DataHeader = master_data_property.DataHeader
NecessaryColumn = master_data_property.NecessaryColumn()
NcNecessaryColumns = master_data_property.NcNecessaryColumns
ResultDeviationColumns = master_data_property.ResultDeviationColumns
ResultOutput = master_data_property.ResultOutput

extracted_sheet_header_map = HashMap()


def get_weather_data(date: datetime.datetime):
    # this function is used to download today's weather data only (default)
    # subject 'today' is defined as 'at the date of running this script'
    # or can set to some specific date as wish
    print(f"Downloading weather data for date {date}")
    login_info = FTPLoginInfo(
        port=CannotClassify.ftp_port,
        address=CannotClassify.ftp_address,
        username=CannotClassify.ftp_username,
        password=CannotClassify.ftp_password
    )
    # date = date - datetime.timedelta(days=20)  # test only
    dir_set = Directory()
    remote_path = dir_set.get_expected_remote_weather_data(date)
    local_path = dir_set.get_weather_nc_directory(date)
    ftp_download_process(local_path, login_info, remote_path)


def forge_weather_station_plain_text_data(
        path: str or pathlib.Path,
        header: DataHeader = None
) -> Union[list[list[str]], list[str]] or Union[None, None]:
    """
    Convert a plain text data to a sheet-like dataset
    :param path:
    :param header:
    :return:
    """
    if not pathlib.Path(path).exists():
        print_error_text(f"file [{path}] not found")
        return None, None
    with open(path, 'r') as file:
        # read data set header (match from pre-defined header set)
        header_info = None
        for line in file.readlines():
            # strip()方法用于移除字符串头尾指定的字符（默认为空格或换行符）或字符序列
            # split()函数默认可以按空格分割，并且把结果中的空字符串删除掉，留下有用信息
            line_data = line.strip().split()
            # 将列表转为numpy数组
            # temp = np.array(data)
            # print(line_data)
            id_column_index = None
            header_info = master_data_property.get_defined_header_by_header(
                line_data)
            if header_info is not None:
                for i in range(len(header_info.sheet_header)):
                    item = header_info.sheet_header[i]
                    if item in NecessaryColumn.value_pair.get(NecessaryColumn.id):
                        id_column_index = i
                        break
                break
        assert header_info is not None, "sheet header is unknown, cannot continue"

        # once header is determined, read dataset's data
        file.seek(0)  # (need to reset file pointer to beginning)
        data_length_check: int = -1
        data_content: list[list[str]] = []
        sheet_column_size = header_info.get_sheet_column_length()
        is_data_read_in_progress = False
        is_data_read_finished = False
        for line in file.readlines():
            line_string = line.strip()
            line_data = line_string.split()
            if len(line_data) == sheet_column_size and not line_data.__eq__(header_info.sheet_header):
                if is_data_read_finished and not is_data_read_in_progress:
                    # expect one hour length per file.
                    # or print warning text
                    print_warning_text(
                        "Data read interrupted, is there multiple dataset in one file?")
                if not is_data_read_in_progress:
                    is_data_read_in_progress = True
                    print_info_text("Data read begin")
                for i in range(len(line_data)):
                    if i != id_column_index and line_data[i] in header_info.none_values:
                        line_data[i] = np.nan  # none? todo
                data_content.append(line_data)
            elif line_string.startswith(header_info.data_size_header):
                temp = re.findall(r'[\d]+', line_string)[0]
                assert str.isdigit(temp), f'type err data_size={temp}'
                if data_length_check == -1:
                    data_length_check = int(temp)
                    print_info_text(
                        f"This dataset's size is {data_length_check}")
                else:
                    # expect one hour length per dataset.
                    # or print warning text
                    print_warning_text(
                        f"Duplicate dataset size metadata. Current size is {data_length_check} and got {int(temp)}")
            else:
                if is_data_read_in_progress and not is_data_read_finished:
                    # data read complete
                    is_data_read_in_progress = False
                    is_data_read_finished = True
                    print_success_text("Data read complete")
                elif is_data_read_in_progress and is_data_read_finished:
                    is_data_read_in_progress = False
                    print_warning_text("Data read complete")
        rows = len(data_content)
        if data_length_check.__eq__(-1):
            print_info_text(f"data length not specified, ignore length check")
        elif not data_length_check.__eq__(rows):
            print_warning_text(
                f"data length check failed, expect {data_length_check}, read {rows}")
        else:
            print_success_text(f"data length checked")
        return data_content, header_info.sheet_header


def data_convert(start_date: datetime.datetime,
                 time_step: datetime.timedelta,
                 time_range: datetime.timedelta):
    """
    weather station plain text data process, file name is determined by time
    the loop is basically (in form of java): for(time = start_date; time < start_date + time_range; time+=time_step)
    *if time_step is 0 (default value), then a full day of data in start_date is assumed
    :param start_date: yes
    :param time_step: yes
    :param time_range: yes
    :return: no
    """
    input_file_list = list()
    end_date = start_date + time_range
    output_path = Directory().get_weather_verify_output_directory(start_date)
    if time_step.__eq__(datetime.timedelta()):
        input_path = Directory().get_weather_station_data_directory(start_date)
        pathlib_input_path = pathlib.Path(input_path)
        name = Name().get_weather_station_data_file_name(start_date)
        # print(f"DEBUG = name={name}") # debug only
        input_file_list = pathlib_input_path.glob(name)
    else:
        assert time_range > datetime.timedelta(), "invalid time range input"
        assert time_step / time_range > 0, "invalid time step input"
        count = start_date
        while count < end_date:
            path = Directory().get_weather_station_data_directory(count)
            name = Name().get_weather_station_data_file_name(count, hour=count.hour)
            input_file_list.append(str(pathlib.Path(path).joinpath(name)))
            count += time_step

    hash_map = HashMap()

    # doing something to source file (parsing, probably)
    input_data = []
    input_header = []
    no_file = True
    last_header = None
    expected_data_shape = None
    for item in input_file_list:
        no_file = False
        file = pathlib.Path(item)
        filename = file.name
        print_info_text(f"Reading [{filename}]...")
        data, header = forge_weather_station_plain_text_data(item)
        if data is None or header is None:
            print_error_text("No data is generated")
            input_data.append(None)
            input_header.append(None)
            continue
        else:
            input_data.append(data)
            input_header.append(header)

        # header check (I would be mad if somehow they decided to mess with input data's structure)
        if last_header is None:
            last_header = header
        elif header != last_header:
            print_warning_text(
                f"Header difference detected: current: {header}, last: {last_header}")
            last_header = header

        # need to save source data as xlsx?
        if save_input:
            save_data_sheet_to_xlsx(
                data, header, output_path, f'{filename}.xlsx')

    # no input file? hell yeh im gonna sleep
    if no_file:
        print_info_text("no input file")
        return None, None

    return input_data, input_header


def data_extract(
        data_array, header_array,
        output_folder: str, time_start: datetime.datetime, time_step: datetime.timedelta
):
    assert len(data_array) == len(header_array), \
        f"length of data_array ({len(data_array)}) and length of header_array ({len(header_array)}) mismatch"
    extracted_header: list[str] = NecessaryColumn.get_column_header()
    global extracted_sheet_header_map
    extracted_header_map = HashMap()
    for i in range(len(extracted_header)):
        extracted_header_map.put(extracted_header[i], i)
    extracted_data: list[list[list[str]]] = []

    header_map = HashMap()
    length = len(header_array)  # numbers of dataset
    last_header = None

    # loop through every dataset
    for i in range(length):
        print_info_text(
            f"Extracting necessary data columns from date {time_start}")
        data = data_array[i]
        header = header_array[i]

        # checking if data is empty
        if data is None or header is None:
            print_info_text(
                f"Empty data found on index {i}, ignore and proceed to next data")
            extracted_data.append(None)
            continue
        temp = []

        # update header_map if a different header is found OR header_map is empty (it is empty if last_header is None)
        if last_header is None or last_header != header:
            print_info_text(
                f"new column header found, now: {header}, was: {last_header}")
            last_header = header
            header_map.clear()
            for index in range(len(header)):
                header_map.put(header[index], index)

        # find data header index based on expected header text which is based on extracted_header
        # a expected header -> find its text that should be in source data header -> find its index
        last_known_row_length = None
        for o in range(len(extracted_header)):
            header_index = None
            expected_header_text_array = NecessaryColumn.value_pair.get(
                extracted_header[o])
            for item in expected_header_text_array:
                header_index = header_map.get(item)
                if header_index is not None:
                    break
            # copy column to new array
            if header_index is not None:
                # print([a[header_index] for a in data])  # debug test
                if last_known_row_length is None:
                    last_known_row_length = len(data)
                temp.append([a[header_index] for a in data])
            else:
                temp.append(None)
        assert last_known_row_length is not None, "data set has no rows?"

        # checking if there is a missing column, and delete it if missing
        # todo known issue: an empty column will cause inconsistent row length and eventually error out xlsx export and numpy
        # todo, if id column not found, use longitude&latitude as id. if lon&lat not found, throw exception
        '''
        size = len(extracted_header)
        index = 0
        while index < size:
            print(f"cur {index}")  # debug test todo
            if temp[index] is None:
                extracted_header = np.delete(extracted_header, index)
                temp = np.delete(temp, index)  
                size -= 1
            index += 1
        temp = matrix(temp)
        '''
        temp = np.transpose(temp)
        # print(temp)  # test debug todo
        extracted_data.append(temp)
        if save_other:
            if time_step.__eq__(datetime.timedelta()):
                sub_name = i
            else:
                sub_name = f'{time_start.hour}-{time_start.minute}-{time_start.second}'
            name = f'weather_station_plain_text_{time_start.date()}_{sub_name}_extracted'
            save_data_sheet_to_xlsx(
                temp, extracted_header, output_folder, name)
        time_start += time_step
    return extracted_data, extracted_header


def data_merge(start_date: datetime.datetime,
               time_step: datetime.timedelta,
               time_range: datetime.timedelta,
               merge_range: datetime.timedelta,
               data_array, header_array, output_path):
    groups_of_data = int(time_range.total_seconds() /
                         merge_range.total_seconds())
    sets_of_data_per_group = int(
        merge_range.total_seconds() / time_step.total_seconds())
    total_sets_of_data = groups_of_data * sets_of_data_per_group
    header_array_length = len(header_array)
    print_info_text(
        f"Expecting: {groups_of_data} data group(s) for output, with {sets_of_data_per_group} set(s) of data per group"
    )
    row_hashmap = HashMap()
    merged_data = []
    id_column_index = None
    rain_column_index = None
    temperature_column_index = None
    wind_column_index = None
    for i in range(header_array_length):
        item = header_array[i]
        # print(item)  # debug
        if NecessaryColumn.id.__eq__(item):
            id_column_index = i
        elif NecessaryColumn.rain.__eq__(item):
            rain_column_index = i
        elif NecessaryColumn.max_wind.__eq__(item):
            wind_column_index = i
        elif NecessaryColumn.max_temperature.__eq__(item):
            temperature_column_index = i
    assert id_column_index is not None, f"id column must present\n id header: {NecessaryColumn.id}\n column: {header_array}"
    assert len(
        data_array) >= total_sets_of_data, f"invalid data set count, expect no less than {total_sets_of_data} got {len(data_array)}"
    for group_num in range(groups_of_data):
        row_hashmap.clear()
        for set_num in range(group_num * sets_of_data_per_group, (group_num + 1) * sets_of_data_per_group):
            data = data_array[set_num]
            assert data is not None, f"Unexpected null data, check source data"
            for row in data:
                the_id = row[id_column_index]
                hashed_row = row_hashmap.get(the_id)
                if hashed_row is None:
                    the_row = []
                    for i in range(header_array_length):
                        if (rain_column_index is not None and rain_column_index == i) or \
                                (wind_column_index is not None and wind_column_index == i) or \
                                (temperature_column_index is not None and temperature_column_index == i):
                            the_row.append(float(row[i]))
                        else:
                            the_row.append(row[i])
                    hashed_row = the_row
                else:
                    if rain_column_index is not None and row[rain_column_index] == float(row[rain_column_index]):
                        hashed_row[rain_column_index] += float(
                            row[rain_column_index])
                    if temperature_column_index is not None and row[temperature_column_index] == row[
                        temperature_column_index] and hashed_row[temperature_column_index] < float(
                        row[temperature_column_index]):
                        hashed_row[temperature_column_index] = float(
                            row[temperature_column_index])
                    if wind_column_index is not None and row[wind_column_index] == row[wind_column_index] and \
                            hashed_row[wind_column_index] < float(row[wind_column_index]):
                        hashed_row[wind_column_index] = float(
                            row[wind_column_index])
                row_hashmap.put(the_id, hashed_row)
        merged_data.append(row_hashmap.list())
    if save_other:
        for index in range(len(merged_data)):
            item = merged_data[index]
            name = Name().get_weather_station_plain_text_cache_file_base_name(
                start_date=start_date, time_offset=merge_range * index, time_range=merge_range
            ) + '_merged'
            save_data_sheet_to_xlsx(item, header_array, output_path, name)
    return merged_data


def nc_data_append(
        start_date: datetime.datetime,
        time_step: datetime.timedelta,
        time_range: datetime.timedelta,
        set_range: datetime.timedelta,
        data_array, header_array
):
    nc_folder = Directory().get_weather_nc_directory(start_date)
    nc_files_full_path = pathlib.Path(nc_folder).glob('*.nc')
    for nc_file_full_path in nc_files_full_path:
        nc_filename = pathlib.Path(nc_file_full_path).name
        nc_start_time = nc_filename.split('_')[-2:-1][0]
        nc_start_time = datetime.datetime.strptime(
            nc_start_time, CannotClassify.weather_filename_start_time_format)
        if nc_start_time - start_date != datetime.timedelta():
            continue
        quantity_in_set = int(set_range / time_step)
        quantity_in_range = int(time_range / set_range)
        nc_dataset = netCDF4.Dataset(nc_file_full_path)
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size -
                                                          1], nc_lat_array[0]
        nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / \
                      (nc_lat_array_size - 1)
        nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size -
                                                          1], nc_lon_array[0]
        nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / \
                      (nc_lon_array_size - 1)

        new_data_array = []
        latitude_index = longitude_index = rain_index = wind_index = temperature_index = None

        for i in range(len(header_array)):
            if header_array[i] == NecessaryColumn.latitude:
                latitude_index = i
            elif header_array[i] == NecessaryColumn.longitude:
                longitude_index = i
            elif header_array[i] == NecessaryColumn.rain:
                rain_index = i
            elif header_array[i] == NecessaryColumn.max_wind:
                wind_index = i
            elif header_array[i] == NecessaryColumn.max_temperature:
                temperature_index = i
        assert latitude_index is not None and longitude_index is not None, \
            "column [longitude] and [latitude] must present"
        data_array_length = len(data_array)
        if rain_index is not None:
            header_array.append(NcNecessaryColumns.rain)
        if wind_index is not None:
            header_array.append(NcNecessaryColumns.max_wind)
        if temperature_index is not None:
            header_array.append(NcNecessaryColumns.max_temperature)
        assert quantity_in_range == data_array_length, "time range mismatch with data array"
        for i in range(data_array_length):
            # print(f"{i * quantity_in_set}:{(i + 1) * quantity_in_set}")  # debug only
            data = data_array[i]
            new_data = []
            data_length = len(data)
            for o in range(data_length):
                row = data[o]
                row_longitude = float(row[longitude_index])
                nc_longitude_index = lonlat_to_index_v2(
                    nc_lon_index_min, row_longitude, nc_lon_step)
                if nc_longitude_index < 0 or nc_longitude_index >= nc_lon_array_size:
                    continue
                row_latitude = float(row[latitude_index])
                nc_latitude_index = lonlat_to_index_v2(
                    nc_lat_index_min, row_latitude, nc_lat_step)
                if nc_latitude_index < 0 or nc_latitude_index >= nc_lat_array_size:
                    continue
                if rain_index is not None:
                    rain_array = nc_dataset[DataProperty.Rain.NCFieldTag][
                                 i * quantity_in_set:(i + 1) * quantity_in_set,
                                 nc_latitude_index, nc_longitude_index
                                 ]
                    rain_value = np.sum(rain_array)
                    row.append(rain_value)
                if wind_index is not None:
                    wind_array = nc_dataset[DataProperty.WindSpeed.NCFieldTag][
                                 i * quantity_in_set:(i + 1) * quantity_in_set, 0, nc_latitude_index,
                                 nc_longitude_index]
                    wind_value = np.max(wind_array)
                    row.append(wind_value)
                if temperature_index is not None:
                    temperature_array = nc_dataset[DataProperty.Temperature.NCFieldTag][
                                        i * quantity_in_set:(i + 1) * quantity_in_set,
                                        nc_latitude_index, nc_longitude_index]
                    temperature_value = np.max(temperature_array)
                    row.append(temperature_value)
                new_data.append(row)
            new_data_array.append(new_data)
            if save_other:
                name = Name().get_weather_station_plain_text_cache_file_base_name(start_date, set_range * i,
                                                                                  set_range) + '_nc_appended'
                save_data_sheet_to_xlsx(new_data, header_array,
                                        Directory().get_weather_verify_output_directory(start_date), name)
        return new_data_array, header_array
    return None, None


def quick_group_result(data_array, header_array, start_time: datetime.datetime, time_range: datetime.timedelta,
                       set_coverage: datetime.timedelta):
    """
    function development aborted
    :param data_array:
    :param header_array:
    :param start_time:
    :param time_range:
    :param set_coverage:
    :return:
    """
    # columns of rain
    actual_rain_index = extracted_sheet_header_map.get(NecessaryColumn.rain)
    nc_rain_index = extracted_sheet_header_map.get(NcNecessaryColumns.rain)
    deviation_rain_index = extracted_sheet_header_map.get(
        ResultDeviationColumns.rain)

    # columns of wind
    actual_wind_index = extracted_sheet_header_map.get(
        NecessaryColumn.max_wind)
    nc_wind_index = extracted_sheet_header_map.get(NcNecessaryColumns.max_wind)
    deviation_wind_index = extracted_sheet_header_map.get(
        ResultDeviationColumns.max_wind)

    # columns of temperature
    actual_temperature_index = extracted_sheet_header_map.get(
        NecessaryColumn.max_temperature)
    nc_temperature_index = extracted_sheet_header_map.get(
        NcNecessaryColumns.max_temperature)
    deviation_temperature_index = extracted_sheet_header_map.get(
        ResultDeviationColumns.max_temperature)

    # id column
    id_column_index: int = extracted_sheet_header_map.get(NecessaryColumn.id)

    new_data_array = []
    new_header_array = []
    current_time = start_time
    for data in data_array:
        new_data = []

        # no rain < 0.1 <= little rain <= heavy rain
        # correct forecast count = is prediction value and actual value fall in the same category
        correct_little_rain_forecast_count = correct_heavy_rain_forecast_count = correct_no_rain_forecast_count = 0
        correct_wind_forecast_count = 0
        correct_temperature_forecast_count = 0
        rain_ts_total = rain_ts_valid = 0
        data_length = len(data)
        no_rain_spliter = DataProperty.Rain.Split[0]
        heavy_rain_spliter = DataProperty.Rain.Split[2]

        deviation_rain_data = deviation_wind_data = deviation_temperature_data = None
        actual_rain_data = actual_wind_data = actual_temperature_data = None
        nc_rain_data = nc_wind_data = nc_temperature_data = None

        new_header_array.append(header_array[id_column_index])

        if deviation_rain_index is not None:
            deviation_rain_data = [row[deviation_rain_index] for row in data]
            new_header_array.append(header_array[actual_rain_index])
            new_header_array.append(header_array[nc_rain_index])
            new_header_array.append(header_array[deviation_rain_index])
        if deviation_wind_index is not None:
            deviation_wind_data = [row[deviation_wind_index] for row in data]
            new_header_array.append(header_array[deviation_wind_index])
        if deviation_temperature_index is not None:
            deviation_temperature_data = [
                row[deviation_temperature_index] for row in data]
            new_header_array.append(header_array[deviation_temperature_index])

        start = 0
        while start < len(data):
            new_row = []
            end = len(data) if start + CannotClassify.group_of > len(
                data) else start + CannotClassify.group_of

            new_row.append([row[id_column_index]
                            for row in data[start:end]].__str__())
            if deviation_rain_index is not None:
                value = np.average(
                    [value for value in deviation_rain_data[start:end]])
                actual_rain_value = np.average(
                    [value[actual_rain_index] for value in data[start:end]])
                nc_rain_value = np.average(
                    [value[nc_rain_index] for value in data[start:end]])
                new_row.append(actual_rain_value)
                new_row.append(nc_rain_value)
                new_row.append(value)

                # data & nc both >= 0.1
                if actual_rain_value >= no_rain_spliter and nc_rain_value >= no_rain_spliter:
                    correct_no_rain_forecast_count += 1  # predicted is rain, and actual is rain
                    # data & nc both >= 25
                    if actual_rain_value >= heavy_rain_spliter and nc_rain_value >= heavy_rain_spliter:
                        # predicted heavy rain, actually heavy rain
                        correct_heavy_rain_forecast_count += 1
                        # predicted not little rain, actually not little rain (cuz it is heavy rain)
                        correct_little_rain_forecast_count += 1
                    # data & nc in 0.1~25
                    elif actual_rain_value < heavy_rain_spliter and nc_rain_value < heavy_rain_spliter:
                        # predicted little rain, actually little rain
                        correct_little_rain_forecast_count += 1

                # data & nc both < 25
                if actual_rain_value < heavy_rain_spliter and nc_rain_value < heavy_rain_spliter:
                    # predicted not heavy rain, actually not heavy rain (heavy rain complete)
                    correct_heavy_rain_forecast_count += 1
                    # data & nc both < 1; if data<1 && nc<1 then data<25 && nc<25
                    if actual_rain_value < no_rain_spliter and nc_rain_value < no_rain_spliter:
                        # predicted no rain, actually no rain (no rain complete)
                        correct_no_rain_forecast_count += 1
                        # predicted not little rain, actually not little rain (cuz it is no rain)
                        correct_little_rain_forecast_count += 1
                    elif actual_rain_value >= no_rain_spliter and nc_rain_value >= no_rain_spliter:
                        # correct_little_rain_forecast_count += 1  # predicted little rain, actually little rain
                        pass  # it has been calculated above, no need to re-calculate, and little rain has complete

                # something named 'ts', idk what it is
                a = actual_rain_value >= 1
                b = nc_rain_value >= 1
                if a or b:
                    rain_ts_total += 1
                    if a and b:
                        rain_ts_valid += 1

            if deviation_wind_index is not None:
                new_row.append(np.average(
                    [value for value in deviation_wind_data[start:end]]))
            if deviation_temperature_index is not None:
                new_row.append(np.average(
                    [value for value in deviation_temperature_data[start:end]]))

            new_data.append(new_row)
            start += CannotClassify.group_of


def the_result(data_array, header_array, start_time: datetime.datetime, time_range: datetime.timedelta,
               set_coverage: datetime.timedelta):
    rain_index = nc_rain_index = None
    wind_index = nc_wind_index = None
    temperature_index = nc_temperature_index = None
    for i in range(len(header_array)):
        if header_array[i] == NecessaryColumn.rain:
            rain_index = i
            extracted_sheet_header_map.put(NecessaryColumn.rain, rain_index)
        elif header_array[i] == NecessaryColumn.max_wind:
            wind_index = i
            extracted_sheet_header_map.put(
                NecessaryColumn.max_wind, wind_index)
        elif header_array[i] == NecessaryColumn.max_temperature:
            temperature_index = i
            extracted_sheet_header_map.put(NecessaryColumn.max_temperature, i)
        elif header_array[i] == NcNecessaryColumns.rain:
            nc_rain_index = i
            extracted_sheet_header_map.put(NcNecessaryColumns.rain, i)
        elif header_array[i] == NcNecessaryColumns.max_wind:
            nc_wind_index = i
            extracted_sheet_header_map.put(NcNecessaryColumns.max_wind, i)
        elif header_array[i] == NcNecessaryColumns.max_temperature:
            nc_temperature_index = i
            extracted_sheet_header_map.put(
                NcNecessaryColumns.max_temperature, i)
        elif header_array[i] == NecessaryColumn.id:
            extracted_sheet_header_map.put(NecessaryColumn.id, i)
    if rain_index is not None and nc_rain_index is not None:
        extracted_sheet_header_map.put(
            ResultDeviationColumns.rain, len(header_array))
        header_array.append(ResultDeviationColumns.rain)
    if wind_index is not None and nc_wind_index is not None:
        extracted_sheet_header_map.put(
            ResultDeviationColumns.max_wind, len(header_array))
        header_array.append(ResultDeviationColumns.max_wind)
    if temperature_index is not None and nc_temperature_index is not None:
        extracted_sheet_header_map.put(
            ResultDeviationColumns.max_temperature, len(header_array))
        header_array.append(ResultDeviationColumns.max_temperature)

    path = Directory().get_weather_verify_output_directory(start_time)
    data_array_length = len(data_array)
    time_offset = datetime.timedelta()
    new_data_array = []
    output_array = []
    for data_array_index in range(data_array_length):
        data = data_array[data_array_index]
        new_data = []
        # no rain < 0.1 <= little rain <= heavy rain
        # correct forecast count = is prediction value and actual value fall in the same category
        correct_little_rain_forecast_count = correct_heavy_rain_forecast_count = correct_no_rain_forecast_count = 0
        # correct_rain_count = correct_no_rain_count = correct_heavy_rain_count = correct_not_heavy_rain_count = 0
        correct_wind_forecast_count = 0
        correct_temperature_forecast_count = 0
        rain_ts_total = rain_ts_valid = 0
        data_length = len(data)
        no_rain_spliter = DataProperty.Rain.Split[0]
        heavy_rain_spliter = DataProperty.Rain.Split[2]
        deviations_rain = []
        deviations_wind = []
        deviations_temperature = []
        output = {}
        for row in data:
            if rain_index is not None and nc_rain_index is not None:
                value = row[rain_index] - row[nc_rain_index]
                deviations_rain.append(value)
                row.append(value)

                # data & nc both >= 0.1
                if row[rain_index] >= no_rain_spliter and row[nc_rain_index] >= no_rain_spliter:
                    correct_no_rain_forecast_count += 1  # predicted is rain, and actual is rain
                    # data & nc both >= 25
                    if row[rain_index] >= heavy_rain_spliter and row[nc_rain_index] >= heavy_rain_spliter:
                        # predicted heavy rain, actually heavy rain
                        correct_heavy_rain_forecast_count += 1
                        # predicted not little rain, actually not little rain (the bigger side)
                        correct_little_rain_forecast_count += 1
                    # data & nc in 0.1~25
                    elif row[rain_index] < heavy_rain_spliter and row[nc_rain_index] < heavy_rain_spliter:
                        # predicted little rain, actually little rain
                        correct_little_rain_forecast_count += 1

                # data & nc both < 25
                if row[rain_index] < heavy_rain_spliter and row[nc_rain_index] < heavy_rain_spliter:
                    # predicted not heavy rain, actually not heavy rain (heavy rain complete)
                    correct_heavy_rain_forecast_count += 1
                    # data & nc both < 1; if data<1 && nc<1 then data<25 && nc<25
                    if row[rain_index] < no_rain_spliter and row[nc_rain_index] < no_rain_spliter:
                        # predicted no rain, actually no rain (no rain complete)
                        correct_no_rain_forecast_count += 1
                        # predicted not little rain, actually not little rain (the smaller side)
                        correct_little_rain_forecast_count += 1
                    elif row[rain_index] >= no_rain_spliter and row[nc_rain_index] >= no_rain_spliter:
                        # correct_little_rain_forecast_count += 1  # predicted little rain, actually little rain
                        pass  # it has been calculated above, no need to re-calculate, and little rain has complete

                '''
                if (row[rain_index] > no_rain_spliter and row[nc_rain_index] > no_rain_spliter) or \
                        (row[rain_index] <= no_rain_spliter and row[nc_rain_index] <= no_rain_spliter):
                    correct_rain_forecast_count += 1
                if (row[rain_index] > heavy_rain_spliter and row[nc_rain_index] > heavy_rain_spliter) or \
                        (row[rain_index] <= heavy_rain_spliter and row[nc_rain_index] <= heavy_rain_spliter):
                    correct_heavy_rain_forecast_count += 1
                '''

                a = row[rain_index] >= 1
                b = row[nc_rain_index] >= 1
                if a or b:
                    rain_ts_total += 1
                    if a and b:
                        rain_ts_valid += 1
            if wind_index is not None and nc_wind_index is not None:
                value = row[wind_index] - row[nc_wind_index]
                if -5 <= value <= 5:
                    correct_wind_forecast_count += 1
                deviations_wind.append(value)
                row.append(value)
            if temperature_index is not None and nc_temperature_index is not None:
                value = row[temperature_index] - row[nc_temperature_index]
                if -2 <= value <= 2:
                    correct_temperature_forecast_count += 1
                deviations_temperature.append(value)
                row.append(value)
            new_data.append(row)
        new_data_array.append(new_data)
        if save_export:
            name = Name().get_weather_station_plain_text_cache_file_base_name(start_time,
                                                                              data_array_index * set_coverage,
                                                                              set_coverage) + "_exporting"
            save_data_sheet_to_xlsx(new_data, header_array, path, name)

        # rain part
        rain_std = numpy.std(deviations_rain)
        output[ResultOutput.rain_std] = rain_std

        rain_variance = rain_std * rain_std
        output[ResultOutput.rain_variance] = rain_variance

        correct_no_rain_forecast_percentage = correct_no_rain_forecast_count / data_length
        output[ResultOutput.correct_no_rain_forecast] = correct_no_rain_forecast_percentage
        correct_little_rain_forecast_percentage = correct_little_rain_forecast_count / data_length
        output[ResultOutput.correct_little_rain_forecast] = correct_little_rain_forecast_percentage
        correct_heavy_rain_forecast_percentage = correct_heavy_rain_forecast_count / data_length
        output[ResultOutput.correct_heavy_rain_forecast] = correct_heavy_rain_forecast_percentage

        rain_ts = 1 if rain_ts_total == 0 else rain_ts_valid / rain_ts_total
        output[ResultOutput.rain_ts] = rain_ts

        # wind part
        wind_std = numpy.std(deviations_wind)
        output[ResultOutput.max_wind_std] = wind_std

        wind_variance = wind_std * wind_std
        output[ResultOutput.max_wind_variance] = wind_variance

        correct_wind_forecast_percentage = correct_wind_forecast_count / data_length
        output[ResultOutput.correct_max_wind_forecast] = correct_wind_forecast_percentage

        # temperature part
        temperature_std = numpy.std(deviations_temperature)
        output[ResultOutput.max_temperature_std] = temperature_std

        temperature_variance = temperature_std * temperature_std
        output[ResultOutput.max_temperature_variance] = temperature_variance

        correct_temperature_forecast_percentage = correct_temperature_forecast_count / data_length
        output[ResultOutput.correct_max_temperature_forecast] = correct_temperature_forecast_percentage

        '''
        # debug purpose 
        print(output)
        print("++++++++++++++++")
        for item in output.items():
            print(item)
        print("++++++++++++++++++")
        '''

        output_array.append(output)
        name = Name().get_weather_station_plain_text_cache_file_base_name(start_time, data_array_index * set_coverage,
                                                                          set_coverage) + '.json'
        file_path = pathlib.Path(path)
        if not file_path.exists():
            os.makedirs(file_path.__str__())
        file_full_path = file_path.joinpath(name)
        with open(file_full_path.__str__(), mode='w') as writer:
            json.dump(output, writer)
            print_success_text(f"{name} saved")
    return new_data_array


def main_process(date=datetime.datetime.now(), time_range=datetime.timedelta(), time_step=datetime.timedelta()):
    # preparation
    output_directory = Directory().get_weather_verify_output_directory(date)
    merge_range = master_data_property.merge_range
    get_weather_data(date)

    # convert plain text to sheet
    data_array, header_array = data_convert(date, time_step, time_range)

    # remove unnecessary column
    if data_array is not None and header_array is not None:
        data_array, header = data_extract(
            data_array, header_array, output_directory, date, time_step)
        del header_array
    else:
        return

    # merge data by time
    data_array = data_merge(data_array=data_array, header_array=header, output_path=output_directory,
                            start_date=date, time_range=time_range, time_step=time_step, merge_range=merge_range)

    # append nc data
    data_array, header = nc_data_append(
        start_date=date, time_step=time_step, time_range=time_range, set_range=merge_range,
        data_array=data_array, header_array=header
    )

    assert data_array is not None and header is not None, "no data for export, are weather station plain text data and nc data okay?"

    # calculate result and save
    data_array = the_result(data_array, header, date, time_range, merge_range)


def sub_process__extract_wind_direction(
        date: datetime.datetime = datetime.datetime.now(),
        time_step: datetime.timedelta = datetime.timedelta(hours=1),
        time_range: datetime.timedelta = datetime.timedelta(days=3),
        set_range: datetime.timedelta = datetime.timedelta(days=1),
):
    """
    Wind direction data extraction
    :param date: start date of nc file (second date string in its name)
    :param time_step: time that each data sheet covers (Unused)
    :param time_range: time that a nc dataset covers (Unused)
    :param set_range: (unused)
    :return: nothing
    """
    output_directory = Directory().get_weather_verify_output_directory(date)
    nc_folder = Directory().get_weather_nc_directory(date)
    nc_files_full_path = pathlib.Path(nc_folder).glob('*.nc')
    wind_direction_data_output: list = list()
    for nc_file_full_path in nc_files_full_path:
        nc_filename = pathlib.Path(nc_file_full_path).name
        nc_start_time = nc_filename.split('_')[-2:-1][0]
        nc_start_time = datetime.datetime.strptime(
            nc_start_time, CannotClassify.weather_filename_start_time_format
        )
        if nc_start_time - date != datetime.timedelta():
            continue
        nc_dataset = netCDF4.Dataset(nc_file_full_path)
        nc_time_size = np.array(nc_dataset.variables["time"][:]).size
        wind_direction_data = nc_dataset[DataProperty.WindDirection.NCFieldTag][:, 0, :, :]
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size - 1], nc_lat_array[0]
        nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / (nc_lat_array_size - 1)
        nc_lat_need_flip = nc_lat_step > 0
        nc_lat_top = nc_lat_index_max if nc_lat_need_flip else nc_lat_index_min
        nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size - 1], nc_lon_array[0]
        nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / (nc_lon_array_size - 1)
        nc_lon_need_flip = nc_lon_step < 0
        nc_lon_left = nc_lon_index_min if not nc_lon_need_flip else nc_lon_index_max
        geo_transformation = (nc_lon_left, nc_lon_step if not nc_lon_need_flip else -nc_lon_step, 0, nc_lat_top, 0,
                              -nc_lat_step if nc_lat_need_flip else nc_lat_step)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
        geo_projection = srs.ExportToWkt()
        for i in range(nc_time_size):
            wind_direction_data_sheet = np.array(wind_direction_data[i, :, :])
            if nc_lat_need_flip:
                wind_direction_data_sheet = np.flip(wind_direction_data_sheet, 0)  # flip vertically
            if nc_lon_need_flip:
                wind_direction_data_sheet = np.flip(wind_direction_data_sheet, 1)  # flip horizontally
            filename = Name().get_wind_direction_sheet_name(nc_start_time, datetime.timedelta(hours=i))
            save_data_sheet_to_xlsx(wind_direction_data_sheet, None, output_directory, filename)
            save_tif(wind_direction_data_sheet, output_directory, geo_transformation, geo_projection, filename)
            formatted_wind_direction_data_sheet = wind_direction_data_sheet
            formatted_wind_direction_data_sheet[
                ((0 <= formatted_wind_direction_data_sheet) & (formatted_wind_direction_data_sheet <= 22.5)) |
                ((360 >= formatted_wind_direction_data_sheet) & (formatted_wind_direction_data_sheet > 337.5))
                ] = 0  # 0/360 +- 22.5 (north)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 67.5) & (formatted_wind_direction_data_sheet > 22.5)
                ] = 1  # 45 +- 22.5 (north east)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 112.5) & (formatted_wind_direction_data_sheet > 67.5)
                ] = 2  # 90 +- 22.5 (east)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 157.5) & (formatted_wind_direction_data_sheet > 112.5)
                ] = 3  # 135 +- 22.5 (south east)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 202.5) & (formatted_wind_direction_data_sheet > 157.5)
                ] = 4  # 180 +- 22.5 (south)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 247.5) & (formatted_wind_direction_data_sheet > 202.5)
                ] = 5  # 225 +- 22.5 (south west)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 292.5) & (formatted_wind_direction_data_sheet > 247.5)
                ] = 6  # 270 +- 22.5 (west)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 337.5) & (formatted_wind_direction_data_sheet > 292.5)
                ] = 7  # 315 +- 22.5 (north west)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet < 0) | (formatted_wind_direction_data_sheet > 7)
                ] = np.nan  # else (invalid)
            filename += '_formatted'
            save_data_sheet_to_xlsx(formatted_wind_direction_data_sheet, None, output_directory, filename)


save_input = Switches.save_input_data
# Save temporary/middle data, excluding input and output/render
save_other = Switches.save_other_data
save_export = Switches.save_render_data

if __name__ == '__main__':
    main_process()
    ten_secs_countdown_exit()
