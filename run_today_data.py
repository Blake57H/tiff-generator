from weather_0_3_2 import get_weather_data, calculate_process
from thunder_0_1_2 import get_thunder_data, process_thunder_data
import datetime
import os
clear = lambda: os.system('cls' if os.name == 'nt' else 'clear')
Today = datetime.datetime.now()

success = False
while(not success):
    try:
        success = get_weather_data()
        if success:
            calculate_process()
    except Exception as ex:
        print(ex.with_traceback)

success = False
while(not success):
    try:
        success = get_thunder_data()
        if success:
            process_thunder_data()
    except Exception as ex:
        print(ex.with_traceback)
