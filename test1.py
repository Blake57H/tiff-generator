import datetime

import config_1_1_4 as config
import landslide_0_1_1
import thunder_0_1_2 as thunder
import weather_0_3_1
import weather_verify_beta

a = 3
# 1=thunder, 3=weather, 4=weather_verify
# download > run > full
download_only = False
run_only = True
full_course = True
July31 = datetime.datetime.strptime("20210731", config.CannotClassify.remote_directory_date_format)
July30 = datetime.datetime.strptime("20210730", config.CannotClassify.remote_directory_date_format)
July28 = datetime.datetime.strptime("20210728", config.CannotClassify.remote_directory_date_format)
Aug2 = datetime.datetime(year=2021, month=8, day=2)
Aug2_8 = datetime.datetime(year=2021, month=8, day=2, hour=8)
Yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
DayBeforeYesterday = datetime.datetime.now() - datetime.timedelta(days=2)
Today = datetime.datetime.now()
Any = datetime.datetime(year=2021, month=9, day=6, hour=8)

if a == 2:
    landslide_0_1_1.main_process(Yesterday)
elif a == 1:
    if download_only:
        thunder.get_thunder_data(Any)
    elif run_only:
        thunder.process_thunder_data(Any)
    elif full_course:
        thunder.main_process(Yesterday)
elif a == 3:
    if download_only:
        weather_0_3_1.get_weather_data(Any)
    elif run_only:
        weather_0_3_1.calculate_process(Any)
    elif full_course:
        weather_0_3_1.main(Yesterday)
elif a == 4:
    weather_verify_beta.main_process(Any, datetime.timedelta(days=3), datetime.timedelta(hours=1))
    weather_verify_beta.sub_process__extract_wind_direction(date=Any)
elif a == 5:
    weather_verify_beta.sub_process__extract_wind_direction(date=Any)
