import pathlib
import time
from shutil import copy

import netCDF4 as nC
import numpy as np
from osgeo import osr

from config_landslide_1_0_0 import *
from ftp_file_fetcher_0_2_2 import FTPLoginInfo
from ftp_file_fetcher_0_2_2 import main_process as ftp_download_process
from utility_0_1_2 import *


"""
Modification are tagged with [# changes]
"""


def get_weather_nc_data(date):
    # this function is used to download today's weather data only (default)
    # subject 'today' is defined as 'at the date of running this script'
    # or can set to some specific date as wish
    if not isinstance(date, datetime.datetime):
        print_error_text(f"[date] input type error, expect {type(datetime.datetime)}, got {type(date)}")
        return
    login_info = FTPLoginInfo(
        port=CannotClassify.ftp_port,
        address=CannotClassify.ftp_address,
        username=CannotClassify.ftp_username,
        password=CannotClassify.ftp_password
    )
    # date = date - datetime.timedelta(days=20)  # test only
    dir_set = Directory()
    remote_path = dir_set.get_expected_remote_weather_data(date)
    local_path = dir_set.get_weather_nc_directory(date)
    ftp_download_process(local_path, login_info, remote_path)


# changes
def gaussian_function(ndarray):
    '''
    高斯函数
    :param ndarray:
    :return:
    '''
    array = 1 / (0.4 * np.sqrt(2 * np.pi)) * (np.e) ** (-0.5 * ((ndarray - 1) / 0.4) ** 2)
    return array


def process_landslide_data(date):
    if not isinstance(date, datetime.datetime):
        print_error_text(f"[date] input type error, expect {type(datetime.datetime)}, got {type(date)}")
        return
    output_directory = Directory().get_landslide_report_directory(date)
    input_directory = Directory().get_weather_nc_directory(date)
    reference_directory = Directory.reference_directory  # background image should be placed here
    reference_directory_path = pathlib.Path(reference_directory)
    temporary_directory = os.path.join(Directory.temporary_directory, 'landslide_workplace')
    if not give_warning_of_non_exist_directory(output_directory):
        print_info_text("output folder is being created")
        os.makedirs(output_directory)
    if not give_warning_of_non_exist_directory(temporary_directory):
        print_info_text("creating temporary workplace...")
        os.makedirs(temporary_directory)
    elif len(os.listdir(temporary_directory)) > 0:
        print_info_text("cleaning working directory")
        clean_temporary_folder(temporary_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print_error_text("input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print_error_text("reference folder not found")
        exit(2)

    nc_data_path = glob.glob(os.path.join(input_directory, Name.weather_data_file))

    if not os.path.exists(os.path.join(reference_directory, Name.landslide_DEM)):
        raise Exception(f"DEM reference [{Name.landslide_DEM}] not found")
    dem = read_tif_data(os.path.join(reference_directory, Name.landslide_DEM))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_slope)):
        raise Exception(f"Slope reference [{Name.landslide_slope}] not found")
    slope = read_tif_data(os.path.join(reference_directory, Name.landslide_slope))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_terrain)):
        raise Exception(f"Terrain reference [{Name.landslide_terrain}] not found")
    terrain = read_tif_data(os.path.join(reference_directory, Name.landslide_terrain))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_NDVI)):
        raise Exception(f"NDVI reference [{Name.landslide_NDVI}] not found")
    ndvi = read_tif_data(os.path.join(reference_directory, Name.landslide_NDVI))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_river)):
        raise Exception(f"River reference [{Name.landslide_river}] not found")
    river = read_tif_data(os.path.join(reference_directory, Name.landslide_river))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_history_density)):
        raise Exception(f"Landslide history reference [{Name.landslide_history_density}] not found")
    landslipe = read_tif_data(os.path.join(reference_directory, Name.landslide_history_density))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_town)):
        raise Exception(f"Town reference [{Name.landslide_town}] not found")
    town = read_tif_data(os.path.join(reference_directory, Name.landslide_town))
    if not os.path.exists(os.path.join(reference_directory, Name.landslide_road)):
        raise Exception(f"Road reference [{Name.landslide_road}] not found")
    road = read_tif_data(os.path.join(reference_directory, Name.landslide_road))
    # changes
    if not reference_directory_path.joinpath(Name.landslide_cable).exists():
        raise Exception(f"Distance to cable [{Name.landslide_cable}] reference not found")
    cable = read_tif_data(reference_directory_path.joinpath(Name.landslide_cable).__str__())

    template = return_dataset(os.path.join(reference_directory, Name.landslide_road))
    geo_transformation = template.GetGeoTransform()
    geo_projection = template.GetProjection()

    mask_abs_path = os.path.abspath(os.path.join(reference_directory, Name.landslide_DEM))

    # 高程 ＜３００，＞２０００ ３００～５００ ５００～１０００ １０００～１５００ １５００～２０００
    dem[dem == dem[0][0]] = np.nan
    dem[(dem >= 2000) | (dem < 300)] = 1
    dem[(dem >= 300) & (dem < 500)] = 2
    dem[(dem >= 500) & (dem < 1000)] = 3
    dem[(dem >= 100) & (dem < 1500)] = 4
    dem[(dem >= 1500) & (dem < 2000)] = 5

    # 坡度 ＜８，＞５０ ８～１５ １５～２５ ２５～３０ ３０～５０
    slope[slope == slope[0][0]] = np.nan
    slope[(slope >= 50) | (slope < 8)] = 1
    slope[(slope >= 8) & (slope < 15)] = 2
    slope[(slope >= 15) & (slope < 25)] = 3
    slope[(slope >= 25) & (slope < 30)] = 4
    slope[(slope >= 30) & (slope < 50)] = 5

    # 地形起伏度 ０～５０ ５０～１００ １００～１８０ １８０～２５０ ＞２５０
    terrain[terrain == terrain[0][0]] = np.nan
    terrain[(terrain >= 0) & (terrain < 50)] = 1
    terrain[(terrain >= 50) & (terrain < 100)] = 2
    terrain[(terrain >= 100) & (terrain < 180)] = 3
    terrain[(terrain >= 180) & (terrain < 250)] = 4
    terrain[terrain >= 250] = 5

    # NDVI ＞０．７ ０．５～０．７ ０．４～０．５ ０．３～０．４ ＜０．３
    ndvi[ndvi == ndvi[0][0]] = np.nan
    ndvi[ndvi >= 0.7] = 1
    ndvi[(ndvi >= 0.5) & (ndvi < 0.7)] = 2
    ndvi[(ndvi >= 0.4) & (ndvi < 0.5)] = 3
    ndvi[(ndvi >= 0.3) & (ndvi < 0.4)] = 4
    ndvi[ndvi < 0.3] = 5

    # 到河流的距离 ＞５００ ３００～５００ １００～３００ ５０～１００ ＜５０
    river[river == river[0][0]] = np.nan
    river[river < 50] = 5
    river[river >= 500] = 1
    river[(river >= 300) & (river < 500)] = 2
    river[(river >= 100) & (river < 300)] = 3
    river[(river >= 50) & (river < 100)] = 4

    # 滑坡点核密度 <174 174~364 364~560 560~810 >810
    landslipe[landslipe == landslipe[0][0]] = np.nan
    landslipe[landslipe < 174] = 1
    landslipe[(landslipe >= 174) & (landslipe < 364)] = 2
    landslipe[(landslipe >= 364) & (landslipe < 560)] = 3
    landslipe[(landslipe >= 560) & (landslipe < 810)] = 4
    landslipe[landslipe >= 810] = 5

    # 到居民地距离 ＞２００ １５０～２００ １００～１５０ ５０～１００ ＜５０
    town[town == town[0][0]] = np.nan
    town[town < 50] = 5
    town[town >= 200] = 1
    town[(town >= 150) & (town < 200)] = 2
    town[(town >= 100) & (town < 150)] = 3
    town[(town >= 50) & (town < 100)] = 4

    # 到公路距离  ＞２００ １５０～２００ １００～１５０ ５０～１００ ＜５０
    road[road == road[0][0]] = np.nan
    road[road < 50] = 5
    road[road >= 200] = 1
    road[(road >= 150) & (road < 200)] = 2
    road[(road >= 100) & (road < 150)] = 3
    road[(road >= 50) & (road < 100)] = 4

    # distance to cable
    cable = gaussian_function(cable)  # changes

    for item in nc_data_path:
        list_of_tif_paths = list()
        print(f"Processing nc data [{item}]...")
        item_date = item.split('_')[-2:-1][0]
        datetime_item_date = datetime.datetime.strptime(item_date, CannotClassify.weather_filename_start_time_format)
        # print(list_of_tif_paths)  # test
        extract_path = None
        report_dir = Directory().get_landslide_report_directory(date)
        report_name = Name().get_landslide_report_name(datetime_item_date)
        global need_report
        has_something_todo = need_source_tif and need_render_data and need_other_data and need_png
        if need_report:
            if not os.path.exists(os.path.join(Directory.reference_directory, Name.landslide_report_template_filename)):
                print_error_text(f"cannot find report template file. report generate will be disabled.")
                need_report = False
            elif pathlib.Path(report_dir).joinpath(
                    report_name).exists() and not has_something_todo and not Switches.ignore_existing_report:
                print_info_text(f"report named [{report_name}] exists, ignore and process next dataset")
                continue
            else:
                extract_path = load_docx_template(
                    os.path.join(Directory.reference_directory, Name.landslide_report_template_filename),
                    'landslide_docx'
                )
        if not need_report and not has_something_todo:
            print_warning_text(f"Current processing settings will do nothing to dataset [{item}]. "
                               f"Check your config and try again.")
            exit(-1)
        list_of_tif_paths.extend(forge_landslide_rain_tiff(item, temporary_directory))
        counter = 0
        for tif in list_of_tif_paths:
            print(f"Processing tif data {tif}...")
            print('Resampling data...')
            path, name, extension, filename = get_file_path_name_extension(tif)
            resampled_filename = name + '_resampled.' + extension
            resampled_filepath = os.path.abspath(os.path.join(path, resampled_filename))
            code = arcpy_resample_helper(os.path.abspath(tif), 0.005, resampled_filepath)
            if code != 0 and not os.path.exists(resampled_filepath):
                print_error_text(f"resample execution failed, return code {code}")
                continue
            elif code != 0 and os.path.exists(resampled_filepath):
                print_warning_text("Resampled file found but process did not exit correctly. "
                                   "Incomplete/damaged output file is possible")
            print("Extracting data by mask...")
            _, name_r, extension_r, _ = get_file_path_name_extension(resampled_filename)
            extracted_filename = name_r + '_extracted.' + extension_r
            extracted_filepath = os.path.abspath(os.path.join(path, extracted_filename))
            code = arcpy_extract_by_mask_helper(resampled_filepath, mask_abs_path, extracted_filepath, mask_abs_path)
            if not code == 0 and not give_warning_of_existed_file(extracted_filepath):
                print_error_text(f"extract by mask execution failed, returned code {code}")
                continue
            elif not code == 0 and give_warning_of_existed_file(extracted_filepath):
                print_warning_text("extract bt mask file found but process did not exit correctly. "
                                   "Incomplete/damaged output file is possible")
            print("Computing landslide possibility...")
            rain = read_tif_data(os.path.join(path, extracted_filename))
            rain[rain == rain[0][0]] = np.nan
            rain[(rain >= 0.1) & (rain < 10)] = 2
            rain[(rain >= 10) & (rain < 25)] = 3
            rain[(rain >= 25) & (rain < 50)] = 4
            rain[rain >= 50] = 5
            rain[rain < 0.1] = 1
            landslide_risk = 0.09 * dem + 0.103 * slope + 0.091 * terrain + 0.102 * ndvi + 0.106 * rain + 0.074 * river + 0.109 * landslipe + 0.054 * town + 0.043 * road
            landslide_risk = landslide_risk * cable  # changes
            print("Saving result as tif data...")
            tif_output_filename = f'landslide_risk_{name}.{extension}'
            png_output_filename = f'landslide_risk_{name}.png'
            tif_output_filepath = temporary_directory
            save_tif(landslide_risk, tif_output_filepath, geo_transformation, geo_projection, tif_output_filename)
            if need_source_tif:
                copy(os.path.join(tif_output_filepath, tif_output_filename),
                     os.path.join(output_directory, tif_output_filename))
            print("Rendering result to png image....")
            code = arcpy_mxd_to_png_helper(
                mxd_absolute_path=os.path.join(Directory().get_landslide_mxd_template_directory(),
                                               Name.landslide_mxd_template),
                input_file_absolute_path=[os.path.abspath(os.path.join(tif_output_filepath, tif_output_filename))],
                image_output_absolute_path=os.path.abspath(os.path.join(tif_output_filepath, png_output_filename)),
                replace_index=[1],
                dpi=ReportProperties.image_output_dpi
            )
            if not code == 0 and not give_warning_of_existed_file(os.path.join(tif_output_filepath, png_output_filename)):
                print_error_text(f"png file export failed, process returned {code}")
                continue
            elif not code == 0 and give_warning_of_existed_file(os.path.join(tif_output_filepath, png_output_filename)):
                print_warning_text("PNG file found but process did not exit correctly. "
                                   "Incomplete/damaged file is possible")
            if need_png:
                copy(os.path.join(tif_output_filepath, png_output_filename),
                     os.path.join(output_directory, png_output_filename))
            if need_report and extract_path is not None:
                new_date = datetime_item_date + datetime.timedelta(days=counter)
                dayx_keyword = ReportProperties.KeyWords.day_x.replace(ReportProperties.KeyWords.day_x_keyword,
                                                                       str(counter))
                image_keyword = ReportProperties.KeyWords.image_day_x.replace(ReportProperties.KeyWords.day_x_keyword,
                                                                              str(counter + 2))
                report_image_replace(os.path.join(tif_output_filepath, png_output_filename),
                                     image_keyword.format(Name.png_file_ext),
                                     extract_path)
                report_text_replace(
                    dayx_keyword,
                    ReportProperties.Formats.date_year_month_day_hour_formats.format(
                        new_date.year, new_date.month, new_date.day, new_date.hour
                    ),
                    extract_path
                )
                counter += 1
            print_success_text(f"Tif file {tif} processing has complete")
        if need_report and extract_path is not None:
            new_date = datetime_item_date + datetime.timedelta(days=counter)
            dayx_keyword = ReportProperties.KeyWords.day_x.replace(ReportProperties.KeyWords.day_x_keyword,
                                                                   str(counter))
            report_text_replace(
                dayx_keyword,
                ReportProperties.Formats.date_year_month_day_hour_formats.format(
                    new_date.year, new_date.month, new_date.day, new_date.hour
                ),
                extract_path
            )
            print(f"Saving report file...")
            zip_docx_report(report_dir, report_name, extract_path)
            clean_temporary_folder(extract_path)
            print_success_text(f"Report saved as {Name().get_landslide_report_name(datetime_item_date)}")
            if need_pdf:
                docx_to_pdf(os.path.join(report_dir, report_name), try_other_method=True)
        print("Cleaning workplace...")
        clean_temporary_folder(temporary_directory)


def forge_landslide_rain_tiff(nc_file_full_path, directory_to_save):
    """
    Convert nc rain data into tif format for landslide use
    :param nc_file_full_path: relative/absolute full path to nc file
    :param directory_to_save: directory where tifs should be saved
    :return: list of full path to tif files
    """
    item = nc_file_full_path
    tiff_filename = []
    item_date = item.split('_')[-2:-1][0]
    datetime_item_date = datetime.datetime.strptime(item_date, CannotClassify.weather_filename_start_time_format)
    print(f"Start date of NC dataset is: {datetime_item_date}")
    nc_dataset = nC.Dataset(item)  # 读取nc数据集
    nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
    nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
    nc_time_array = np.array(nc_dataset.variables["time"][:])
    nc_lon_array_size = len(nc_lon_array)
    nc_lat_array_size = len(nc_lat_array)
    nc_time_step = 24
    nc_data_obj = nC.Dataset(item)
    nc_start_datetime = utc_timestamp_sec_to_datetime_since_1990(nc_time_array[0])

    nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size - 1], nc_lat_array[0]
    nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / (nc_lat_array_size - 1)
    nc_lat_need_flip = nc_lat_step > 0
    nc_lat_top = nc_lat_index_max if nc_lat_need_flip else nc_lat_index_min
    nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size - 1], nc_lon_array[0]
    nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / (nc_lon_array_size - 1)
    nc_lon_need_flip = nc_lon_step < 0
    nc_lon_left = nc_lon_index_min if not nc_lon_need_flip else nc_lon_index_max
    geo_transformation = (nc_lon_left, nc_lon_step if not nc_lon_need_flip else -nc_lon_step, 0, nc_lat_top, 0,
                          -nc_lat_step if nc_lat_need_flip else nc_lat_step)

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
    geo_projection = srs.ExportToWkt()

    rain_field_tag = DataProperty.Rain.NCFieldTag
    rain_friendly_name = DataProperty.Rain.FriendlyName
    # initialize variable
    j = 0
    while j + nc_time_step <= len(nc_time_array):
        # process rain data (every 1h)
        # print(f"data={nc_data_obj['PRE_1h'][j, :, :].T.shape}")
        # print(f"summary={rain_array.shape}")
        rain_array = np.zeros((nc_lat_array_size, nc_lon_array_size))
        for i in range(j, j + nc_time_step):
            rain_array = np.add(rain_array, nc_data_obj[rain_field_tag][i, :, :])
        filename = Name().get_nc_dataset_tif_name(nc_start_datetime,
                                                  f"{j}h",
                                                  f"{j + nc_time_step}h",
                                                  rain_friendly_name)
        path = directory_to_save
        if nc_lat_need_flip:
            rain_array = np.flip(rain_array, 0)
        if nc_lon_need_flip:
            rain_array = np.flip(rain_array, 1)
        if give_warning_of_existed_file(os.path.join(path, filename)):
            print_warning_text(f"{filename} exists, replacing file")
        save_tif(geo_transform_s=geo_transformation, geo_projection_s=geo_projection, path_s=path,
                 data_array_2d_s=rain_array, filename_s=filename)
        tiff_filename.append(os.path.join(path, filename))
        j += nc_time_step
    return tiff_filename


need_report = Switches.need_report
need_pdf = Switches.convert_pdf
need_source_tif = Switches.save_input_data
need_png = Switches.save_png_image
need_other_data = Switches.save_other_data
need_render_data = Switches.save_render_data


def main_process(date=datetime.datetime.now()):
    print("Landslide component starting...")
    get_weather_nc_data(date)
    process_landslide_data(date)
    print("Landslide component finishing...")


if __name__ == '__main__':
    print("Landslide component v0.1.1")
    time1 = time.time()
    main_process()
    time2 = time.time()
    print(f"Program executed for {time2 - time1} seconds")
