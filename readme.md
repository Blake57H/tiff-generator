# Weather Report Generator

some (old) release version are placed in [copies of release version]

develop betas or tests or every other mess are placed in [you guys can sleep now (archive or deprecated)]

I think i will put everything in root directory 

## before running it

Update: If installing packages and python interpretor is a hassle, then try the virtual environment. Suppose it will work in a portable way but it has not been tested. Check out in the "I have prepared a virtual python environment" section.

### library and python used

These scripts are written and tested with `Python 3.9.6 64-bit`.

Minimum Python version required is `3.7`. (a certain person has python 3.7 and his laptop can run my scripts so it has been tested)

This project's code has a lot of annotations, and after some research only python 3.7 or later can read those. 3.6 and eailer are unable to parse those annotations (like `def hello_world(list1:list[str], string1: str="hello world"):`) and will throw error if try to execute.

And I made a version checker and give it a try if you want...

part of the libiaries used are listed below:

```
netCDF4==1.5.7
numpy==1.21.1
Pillow==8.3.1
zip-files==0.4.1
GDAL==3.3.1
geopandas==0.9.0
...(too lazy to edit manually)
```

*library used in requirement format: `requirement.txt` in project root directory

#### In case `pip install [some package]` does not work

Let's say I want to install `GDAL` and I have a standalone python 3.8 installation, then my pip install command would be `pip install GDAL`.

However for some reason GDAL cannot be installed that way. Some error show up. Therefore, my GDAL installation is from a pre-compiled whl file (googled online), which make my pip install line looks like:

(command line) `pip install C:/path_to/GDAL-3.3.1-cp38-cp38-win_amd64.whl`

or

(in requrirement.txt) `GDAL @ file:///C:/path_to/GDAL-3.3.1-cp38-cp38-win_amd64.whl`

*when this readme was first written I was using python 3.8 and it will explain why `.whl` file has a "cp38" in its name. I upgraded to 3.9 later.

#### In case `pip install [some package]` does not work and you don't want to download .whl file

Let's say I want to install `GDAL` and I don't mind having another python installation.

Google says python package manager (is it the right term? is it a manager? but it sorts of act like that to me) like `conda` can take care of it.

Install conda and there will be a python installation in it. Then you can use command line `conda install GDAL` to, well, install GDAL.

### Required directories

`source data`*, `reference` in root directory, or progeam give error

Note that `source data` will be generated automatically by ftp download component if not present. So technically `source data` is optional if you plan on running downloader (which is executed automtically by running the main program) before doing data process.

`output` in root directory, but it's optional, cuz it will be generated if not present

### Some paths that needs to be configed

There is a class named `Paths` in `config_x_y_z.py`.

To run scripts in `/arc_toolbox`, ArcGIS Desktop must be installed and changed `arcgis_python_installation` to your Arcgis's python interpreter path. (ArcGIS Desktop version used on my machine is `10.8` at the time of writing) P.S not sure if it will run on ArcGIS Pro.

To convert docx reports to PDF format, Libre Office must be installed and change `libre_office_path` to your Libre Office's `soffice.exe`'s path. I used default installation path on Windows C: drive. (Libre Office version used on my machine is `7.2-something` at the time of writing)

### I have prepared a virtual python environment

`venv.7z` in "#hack" folder is a virtual python environment with all necessary packages pre-installed.

The environment is created under python 3.9.6 using Idea IDE. Perhaps it will only runs on Windows 10 (refer to python 3.9 release note).

## using this program

Run `[weather/thunder/landslide/icy].py` and everything will be settled

Generated report can be found in `output\[report_type]\[year]\[month&day]\[filename].docx`

Note: some `.cmd` files are for my laziness. One click and process data from the past few days(like batch process?) and pause when finished (so that I could know whether thing had gone wrong or not).
