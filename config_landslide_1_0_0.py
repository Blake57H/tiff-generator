from config_1_1_6 import *

class Name(Name):
    landslide_report_template_filename = 'landslide_report_template.docx'
    landslide_mxd_template = "LandSlipeRisk.mxd"

    # land slide part
    landslide_DEM = "NASADEM_500米_昭通_曲靖（WGS1984）.tif"
    landslide_slope = "NASA坡度_500米_昭通_曲靖（WGS1984）.tif"
    landslide_terrain = "NASADEM_500米_昭通_曲靖（WGS1984）地形起伏度.tif"
    landslide_NDVI = "2020年6月_500米昭通、曲靖NDVI.tif"
    landslide_river = "到河流距离_500米.tif"
    landslide_history_density = "昭通_曲靖滑坡点核密度_500米.tif"
    landslide_town = "到居民地距离_500米.tif"
    landslide_road = "到公路距离_500米.tif"
    landslide_cable = 'YunNan到电线距离.tif'

    def get_landslide_report_name(self, specific_time=None):
        pre_defined_name = '{0}年{1}月{2}日{3}时{4}分昭通曲靖滑坡.docx'
        if specific_time is None:
            time = datetime.datetime.now()
        elif not isinstance(specific_time, datetime.datetime):
            time = datetime.datetime.now()
            print(f"ERROR type of given time <{type(specific_time)}> is not <{type(datetime.datetime)}>, "
                  f"using current time instead")
        else:
            time = specific_time
        return pre_defined_name.format(time.year, time.month, time.day, time.hour, time.minute)

