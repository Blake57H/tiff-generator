import warnings
import numpy as np
import netCDF4 as nC
import datetime
from osgeo import gdal, osr, ogr
import os
import glob

warnings.filterwarnings("ignore")

path = "subjects/"
output_path = "output/"
filePrefix = "pre_"
fileExtension = ".tif"


def get_filename_by_time(time):
    utc_timestamp_sec = (time * 3600) + 631152000  # 631152000 seconds between Jan 1 1990 and Jan 1 1970
    date = datetime.datetime.utcfromtimestamp(utc_timestamp_sec)
    return date.strftime("%Y%m%d%H0000")


def save_tif(data_array_2d_s, filename_s, path_s, geo_transform_s, geo_projection_s, hour_s, data_field_name_s):
    """
    save 2d matrix to tif
    :param data_field_name_s: used in filename creation
    :param hour_s: time lapse of data
    :param geo_projection_s: geo projection
    :param geo_transform_s: get transform
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param data_array_2d_s: data
    """
    geoTiffDriver = gdal.GetDriverByName("GTiff")
    save_filename = f"{path_s}{filename_s}_{data_field_name_s}_{hour_s}h.tif"
    # print(data_array_2d_s.shape)
    print(f"Saving image {save_filename}: size = {data_array_2d_s.shape[1]}x{data_array_2d_s.shape[0]}")
    outDataset = geoTiffDriver.Create(save_filename, data_array_2d_s.shape[1], data_array_2d_s.shape[0], 1,
                                      gdal.GDT_Float32)
    outDataset.SetGeoTransform(geo_transform_s)
    outDataset.SetProjection(geo_projection_s)
    outDataset.GetRasterBand(1).WriteArray(data_array_2d_s)
    outDataset.FlushCache()
    outDataset = None


def judge_leap_year(year):
    """
    判断输入的年份是闰年
    :param year:输入年份
    :return:平年或者闰年一年总共有多少个小时
    """
    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                return 8783  # 整百年能被400整除的是闰年
            else:
                return 8759
        else:
            return 8783  # 非整百年能被4整除的为闰年
    else:
        return 8759


def NC_to_tiffs(data, Output_folder):
    """
    将nc数据批量转为tiff栅格数据
    :param data:输入nc数据路径
    :param Output_folder:输出文件路径
    :return:无返回值
    """
    nc_data_obj = nC.Dataset(data)  # 读取nc数据集
    data_list = list(nc_data_obj.variables.keys())  # 得到nc数据集里的所有要素名称
    Lon = nc_data_obj.variables["lon"][:]  # 经度
    Lat = nc_data_obj.variables["lat"][:]  # 维度
    # 影像的左上角和右下角坐标
    LonMin, LatMax, LonMax, LatMin = [Lon.min(), Lat.max(), Lon.max(), Lat.min()]
    # 遍历nc文件里的每一个要素
    # 分辨率计算
    N_Lat = len(Lat)
    N_Lon = len(Lon)
    Lon_Res = (LonMax - LonMin) / (float(N_Lon) - 1)
    Lat_Res = (LatMax - LatMin) / (float(N_Lat) - 1)
    print(f"Image size: {N_Lon}x{N_Lat}")
    # 创建.tif文件
    # 设置影像的显示范围,-Lat_Res一定要是-的
    geotransform = (LonMin, Lon_Res, 0, LatMax, 0, -Lat_Res)
    # 获取地理坐标系统信息，用于选取需要的地理坐标系统
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
    global output_path

    # 读取时间
    time_middle = nc_data_obj.variables['time'][:]
    # initialize variable
    rain_array = np.zeros((N_Lon, N_Lat))
    rain_filename = ""
    # 逐小时遍历时间
    for j in range(len(time_middle)):
        time = get_filename_by_time(time_middle[j])
        if len(rain_filename) == 0:
            rain_filename = time
        # process rain data (every 1h)
        # print(f"data={nc_data_obj['PRE_1h'][j, :, :].T.shape}")
        # print(f"summary={rain_array.shape}")
        rain_array = np.add(rain_array, nc_data_obj['PRE_1h'][j, :, :].T)

        # 数据写出
        if (j + 1) % 24 == 0:
            save_tif(geo_transform_s=geotransform, geo_projection_s=srs.ExportToWkt(), data_array_2d_s=rain_array.T,
                     data_field_name_s="rain", path_s=output_path, filename_s=rain_filename, hour_s=(j % 24) + 1)
            rain_array = np.zeros((N_Lon, N_Lat))
            rain_filename = ""

    # about wind and temp (fine max value in a 24h range)
    print("Mapping max value for wind and temperature:")
    windspeed_array = np.zeros((N_Lon, N_Lat))
    temp_array = np.zeros((N_Lon, N_Lat))
    temp_field = "TEM"
    windspeed_field = "WS"
    time_section = 0  # an hour offset indicating which part of data to extract
    time_step = 24  # every 24 hours
    while time_section < len(time_middle):
        for iLon in range(N_Lon):
            print(f"\r\t{time_section}h~{time_section+time_step}h,{iLon} of {N_Lon} complete", end="")
            for iLat in range(N_Lat):
                temp_range = np.array(nc_data_obj.variables[temp_field][time_section:time_section+time_step, iLat, iLon])
                temp_max = temp_range[0]
                windspeed_range = nc_data_obj.variables[windspeed_field][time_section:time_section+time_step, 0, iLat, iLon]  # 0 means height level of 10
                temp_array[iLon, iLat] = temp_range.max()
                windspeed_array[iLon, iLat] = windspeed_range.max()
                # print(f"{iLat+1} of {N_Lat}", end="\r")
        print("")
        time = get_filename_by_time(time_middle[time_section])
        save_tif(geo_transform_s=geotransform, geo_projection_s=srs.ExportToWkt(), data_array_2d_s=temp_array.T,
                 data_field_name_s=temp_field, path_s=output_path, filename_s=time, hour_s=time_step)
        save_tif(geo_transform_s=geotransform, geo_projection_s=srs.ExportToWkt(), data_array_2d_s=windspeed_array.T,
                 data_field_name_s=windspeed_field, path_s=output_path, filename_s=time, hour_s=time_step)
        time_section += time_step


if __name__ == "__main__":
    print("ready get set go")
    Input_folder = r".\source data"
    # 如果不存在此文件夹就创建
    if not os.path.exists(r".\subjects\tif"):
        os.mkdir(r".\subjects\tif")
    Output_folder = r".\subjects\tif"
    # 遍历文件夹
    nc_files = glob.glob(Input_folder + "/*.nc")
    # 遍历文件夹下的nc文件
    for nc_file in nc_files:
        NC_to_tiffs(nc_file, Output_folder)
    print("and stop")
