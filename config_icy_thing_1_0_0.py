import datetime
import os

from config_1_1_6 import *


class Switches(Switches):
    """
    Refer config file for more detailed explanation
    * unless this report needs a override configuration, use the same value as in config
    """
    need_report = Switches.need_report  # generate .docx report
    convert_pdf = Switches.convert_pdf  # a .docx report must be present, then it will be converted to PDF
    # save_png_image = True  # save png output
    # save_input_data = True  # formerly 'save_source_tif', save input data in a certain format (tif, shp, etc..)
    # save_render_data = True  # save data that are used to export png image
    # save_other_data = True  # save data that are not input/render and are generated during process
    # ignore_existing_report = True  # if set to [True], existing report will be over written


class Name(Name):
    """
    Refer config file for more detailed explanation
    """
    icy_dem_reference = 'ZT_QJ_500m_WGS1984.tif'
    icy_mxd_template = 'ZT_QJ_Ice_Predict(10.0).mxd'
    icy_docx_template = 'icy_report_template.docx'

    def get_icy_calculated_result_tif_filename(self, date: datetime.datetime, hour_offset: int,
                                               hour_length: int) -> str or None:
        """
        look at the function name~
        :param date: date data file representing
        :param hour_offset: datetime difference between data file and dataset
        :param hour_length: length of time a dataset is covered
        :return: tif file name string or None
        """
        if isinstance(date, datetime.datetime):
            name = 'ice_predict_{}_{}h-{}h.tif'.format(
                date.strftime(CannotClassify.weather_filename_start_time_format),
                hour_offset,
                hour_offset + hour_length
            )
            return name
        else:
            print(f"ERROR given date must be an instance of datetime\n"
                  f"\texpected {type(datetime.datetime)}, got {type(date)}")
            return None


class Directory(Directory):
    """
    Refer config file for more detailed explanation
    """
    icy_sub_docx_extract_path = 'icy_weather_docx'

    def get_icy_temporary_workplace(self) -> str:
        return os.path.join(Directory.temporary_directory, 'icy work folder')

    def get_icy_mxd_template_path(self) -> str:
        return os.path.join(Directory.reference_directory, 'mxds/ice_predict')

    def get_icy_output_path(self, date: datetime.datetime) -> str:
        sub_path = 'ice_predict_nc_report/{0}/{1}'.format(
            datetime.datetime.strftime(date, CannotClassify.year_format),
            datetime.datetime.strftime(date, CannotClassify.month_day_format)
        )
        path = os.path.join(Directory.output_directory, sub_path)
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def get_icy_dem_reference_full_path(self, reference_name=Name.icy_dem_reference) -> str:
        return os.path.join(Directory.reference_directory, reference_name)


class DataProperty(DataProperty):
    """
    Refer config file for more detailed explanation
    """

    class IcyThing:
        resample_size = 0.005
        extract_by_mask_reference = Name.icy_dem_reference
        mxd_time_textbox_name = 'time_str'
        mxd_data_replace_index = 1

        def get_mxd_time_text(self, time_start: datetime.datetime, time_length: datetime.timedelta):
            time_end = time_start + time_length
            mxd_time_string = '{}年{}月{}日{}时——{}年{}月{}日{}时'.format(
                time_start.year, time_start.month, time_start.day, time_start.hour,
                time_end.year, time_end.month, time_end.day, time_end.hour
            )
            return mxd_time_string


class ReportProperties(ReportProperties):
    report_image_index_offset = 2
