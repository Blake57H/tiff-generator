import datetime
import os
from datetime import time
from ftplib import FTP
import sys

from utility_0_1_1 import print_error_text, print_warning_text, print_info_text, print_success_text
from config_1_1_6 import CannotClassify

"""
some code are copied from FTP_download.py by [__author__ = "Maylon"]
"""


class FTPLoginInfo:
    def __init__(self, port=21, address=None, username=None, password=None):
        self.port = port
        self.address = address
        self.username = username
        self.password = password


class Logger(object):

    def __init__(self, stream=sys.stdout, *output_dir):
        if not os.path.exists(str(*output_dir)):
            os.makedirs(str(*output_dir))
        log_name = '{}.log'.format(time.strftime('%Y-%m-%d-%H-%M'))
        filename = os.path.join(str(*output_dir), log_name)

        self.terminal = stream
        self.log = open(filename, 'a+')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass


def size_formatter(size):
    suffix = ['B', 'KB', 'MB', 'GB', 'TB']
    index = 0
    while index < len(suffix) - 1 and size > 1024:
        size = round(size / 1024, ndigits=2)
        index += 1
    return '{0}{1}'.format(str(size), suffix[index])


# 判断远程文件和本地文件大小是否一致
def is_same_size(ftp, local_file, remote_file) -> bool:
    """
    参数:
        ftp：FTP()创建的对象
        local_file：本地文件
        remote_file：远程文件
    """
    # 获取远程文件的大小
    try:
        remote_file_size = ftp.size(remote_file)
    except Exception as e:
        print_error_text("while checking remote file size, reason %s" % e)
        remote_file_size = -1
    # 获取本地文件的大小
    try:
        local_file_size = os.path.getsize(local_file)
    except Exception as e:
        print_error_text("while checking local file size, reason %s" % e)
        local_file_size = -2
    # 比较文件大小，大小相等返回True，否则返回False
    if remote_file_size == local_file_size:
        result = True
    else:
        result = False
    return result


# ftp连接
def ftp_connect(ftp_address, port, username, password) -> FTP or None:
    """
    参数：
        ip_addr：ip地址
        port：端口号
        username：用户名
        password：密码
    """
    ftp = FTP()
    try:
        print(f"Connecting to {ftp_address}....", end="", flush=True)
        ftp.connect(ftp_address, port)  # 连接FTP服务器
        print_success_text("Connected")
        print(ftp.getwelcome())
        print(f"Logging in as {username}....", end="", flush=True)
        ftp.login(username, password)  # 登录
        print_success_text("Logged in")
        return ftp
    except Exception as e:
        print_error_text(f"Connect to {ftp_address} failed, reason: {e}")
        return None  # return NONE when connection failed


def is_directory(ftp, subject, last_directory='..') -> bool:
    try:
        ftp.cwd(subject)  # 判断是否为目录，若是目录则切换到目录下，否则出现异常
        ftp.cwd(last_directory)  # 切换进目录后需要返回上一级
        return True
    except Exception:
        return False


class WriteFile:
    def __init__(self, file, file_size, block_size):
        self.file_full_path = file
        self.file_size = file_size
        self.block_size = block_size

    def handle(self, data):
        print()


# 文件下载
def download_file(ftp, local_file_full_path, remote_file) -> bool:
    """
    参数：
        ftp：FTP()创建的对象
        local_file：本地文件
        remote_file：远程文件
    """
    # 判断远端文件是否存在
    if remote_file not in ftp.nlst():
        print_info_text(f"{remote_file} not exist in remote site, aborting action")  # 若远端文件不存在则打印错误信息
        return False
    buf_size = 8192
    # 判断本地文件是否存在
    if os.path.exists(local_file_full_path) and is_same_size(ftp, local_file_full_path, remote_file):
        print_info_text(f"file {remote_file} with same size exists in local directory, cancel download")
        return True
    else:
        remote_file_size_txt = size_formatter(ftp.size(remote_file))
        with open(local_file_full_path, 'wb') as local_file_stream:
            global progress
            progress = 0

            def write_data(data):
                global progress
                local_file_stream.write(data)
                progress += len(data)
                print(f"\rDownloading：{remote_file}...({size_formatter(progress)}/{remote_file_size_txt})", end="")

            ftp.retrbinary(f'RETR {remote_file}', write_data, buf_size)
        # 判断文件大小是否相等，不相等则重新下载
        if is_same_size(ftp, local_file_full_path, remote_file):
            print_success_text(f"\rDownloading：{remote_file}...downloaded")
            return True
        else:
            print()
            global retries_max, retries_count
            if retries_count < retries_max:
                retries_count += 1
                print_warning_text(f"File size not match, re-trying..({retries_count}/{retries_max})..")
                download_file(ftp, local_file_full_path, remote_file)
            else:
                print_error_text(f"Could not download file after {retries_max} attempts, aborting")
                retries_count = 0
                return False


# 下载整个目录下的文件
def download_dir(ftp, local_full_path, remote_path, include_parent_dir=False) -> bool:
    """
    参数：
        ftp：FTP()创建的对象
        local_dir：本地路径
        remote_path：远程目录
    """
    # get the folder subject to download
    global has_no_error
    if not os.path.exists(local_full_path):
        print(f"INFO local download path {local_full_path} not exists and will be created")
        os.makedirs(local_full_path)  # 可能是多级目录，需要用到makedirs , create local download folder before downloading
    try:
        ftp.cwd(remote_path)  # 切换到远程目录
    except Exception:
        print_error_text(f"remote path {remote_path} not found")
        return False
    if include_parent_dir:
        local_full_path = os.path.join(local_full_path, remote_path)
        os.mkdir(local_full_path)
    file_list = ftp.nlst()  # 获取下载文件列表
    for file_name in file_list:
        if is_directory(ftp, file_name):
            to_return = download_dir(ftp, local_full_path, file_name, include_parent_dir=True)  # 递归下载目录
            ftp.cwd("..")
        else:
            to_return = download_file(ftp, os.path.join(local_full_path, file_name), file_name)
        has_no_error = has_no_error and to_return
    return has_no_error


retries_count = 0
retries_max = CannotClassify.max_retries


def main_process(local_path_to_download, login_info_obj, remote_path_to_download) -> bool:
    print("ftp file fetcher starting")
    if not isinstance(login_info_obj, FTPLoginInfo):
        print_error_text(f"invalid login info, expected {type(FTPLoginInfo)} got {type(login_info_obj)}")
        return False
    print(f"FTP address: {login_info_obj.address}:{login_info_obj.port}\n"
          f"Login username: {login_info_obj.username}")
    ftp_object = ftp_connect(ftp_address=login_info_obj.address, port=login_info_obj.port,
                             username=login_info_obj.username, password=login_info_obj.password)
    to_return = False
    if ftp_object is not None:
        if is_directory(ftp_object, remote_path_to_download, ftp_object.pwd()):
            to_return = download_dir(ftp_object, local_path_to_download, remote_path_to_download)
        else:
            if remote_path_to_download.endswith('/'):
                remote_path_to_download = remote_path_to_download[:-1]
            if remote_path_to_download.endswith('\\'):
                remote_path_to_download = remote_path_to_download[:-1]
            file_name = remote_path_to_download.split('/')[-1:][0].split('\\')[-1:][0]
            if not os.path.exists(local_path_to_download):
                print_info_text(f"local download path {local_path_to_download} not exists and will be created")
                os.makedirs(local_path_to_download)  # create local download folder before downloading
            local_path_to_download = os.path.join(local_path_to_download, file_name)
            to_return = download_file(ftp_object, local_path_to_download, remote_path_to_download)
    print("ftp file fetcher stopping")
    global has_no_error
    has_no_error = has_no_error and to_return
    return has_no_error


has_no_error = True
progress = 0

if __name__ == "__main__":
    print("have a nice day in __main__ ;)")
