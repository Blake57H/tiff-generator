import pathlib

from config_1_1_6 import *


class Switches(Switches):
    """
    Refer config file for more detailed explanation
    * unless this report needs a override configuration, use the same value as in config
    """
    save_input_data = False
    save_other_data = False
    save_render_data = True
    test = False  # for test and debug purpose only


class Name(Name):
    def get_weather_station_data_file_name(self, date: datetime.datetime, hour: int = None) -> str:
        name = date.strftime(CannotClassify.year_month_day_format)
        if hour is None:
            ext = '.*'
        else:
            ext = '.0'
            if hour < 10:
                ext += '0'
            ext = f'{ext}{hour}'
        return f'{name}{ext}'

    def get_weather_station_plain_text_cache_file_base_name(
            self,
            start_date: datetime.datetime,
            time_offset: datetime.timedelta,
            time_range: datetime.timedelta = None
    ):
        string = f'weather_station_plain_text_{start_date.strftime(CannotClassify.year_month_day_hour_format)}'
        if time_range not in [None, datetime.timedelta]:
            string += f'_{int(time_offset.total_seconds() / 3600)}h-{int((time_offset + time_range).total_seconds() / 3600)}h'
        elif time_offset not in [None, datetime.timedelta]:
            string += f'_{int(time_offset.total_seconds() / 3600)}h'
        return string

    def get_wind_direction_sheet_name(self, start_date: datetime.datetime, time_offset: datetime.timedelta):
        string = f'weather-verify_nc-wind-direction_{start_date.strftime(CannotClassify.year_month_day_hour_format)}'
        if time_offset not in [None, datetime.timedelta]:
            string += f'_{int(time_offset.total_seconds() / 3600)}h'
        return string



class Directory(Directory):
    output_directory = 'beta-output'  # testing only, overrides definition in __init__

    def __init__(self, output: str = None, reference: str = None, temporary: str = None, source: str = None):
        if output is not None:
            self.output_directory = output
        if reference is not None:
            self.reference_directory = reference,
        if temporary is not None:
            assert not pathlib.Path(temporary).absolute().__str__() in pathlib.Path('').absolute().__str__(), \
                "use program's parent directory as temporary work folder is a dangerous act (｡ŏ_ŏ)"
            self.temporary_directory = temporary
        if source is not None:
            self.input_directory = source

    def get_weather_station_data_directory(self, date: datetime.datetime) -> str:
        root = pathlib.Path(Directory.input_directory)
        sub = 'weather_station_plain_text/{}/{}'.format(date.strftime(CannotClassify.year_format),
                                                        date.strftime(CannotClassify.month_day_format))
        return str(root.joinpath(sub))

    def get_weather_verify_output_directory(self, date: datetime.datetime) -> str:
        root = pathlib.Path(Directory.output_directory)
        sub = 'weather_verify/{}/{}'.format(date.strftime(CannotClassify.year_format),
                                            date.strftime(CannotClassify.month_day_format))
        return str(root.joinpath(sub))


class CannotClassify(CannotClassify):
    group_of = 4


class DataProperty(DataProperty):
    class WindDirection:
        NCFieldTag = "WD"
        FriendlyName = "wind-direction"
    
    class WeatherStationPlainText:
        class DataHeader:
            def __init__(self, sheet_header: list[str], pre_process_sheet_header: list[str] or None, data_size_header: str,
                         none_values: list[str]):
                self.sheet_header = sheet_header
                self.sheet_header_length: int = len(sheet_header)
                self.pre_processed_sheet_header = pre_process_sheet_header
                self.data_size_header = data_size_header
                self.none_values = none_values

            def header_check(self, subject: list[str]) -> bool:
                """
                check if header is the same as subject
                :param subject:
                :return:
                """
                return len(subject) == self.sheet_header_length and subject == self.sheet_header

            def get_sheet_column_length(self) -> int:
                return self.sheet_header_length

        class NecessaryColumn:
            id = 'id'  # usually longitude&&latitude is enough but I want an extra column to reduce search time
            max_temperature = 'max_temperature'
            # min_temperature = 'min_temperature'
            max_wind = 'max_wind'
            rain = 'rain'
            longitude = 'longitude'
            latitude = 'latitude'

            # pre defined data column that we should look for
            value_pair: dict[str, list[str]] = {
                id: ['站号', '序号'],
                max_temperature: ['最高气温', '温度(℃)'],
                # min_temperature: ['最低气温', '温度(℃)'],
                max_wind: ['风速'],
                rain: ['雨量', '雨量(mm)'],
                longitude: ['经度'],
                latitude: ['纬度']
            }

            def get_column_header(self):
                return [self.id, self.longitude, self.latitude, self.max_temperature,  # self.min_temperature,
                        self.max_wind, self.rain]

        class NcNecessaryColumns:
            max_temperature = 'nc_max_temperature'
            max_wind = 'nc_max_wind'
            rain = 'nc_rain'

        class ResultDeviationColumns:
            max_temperature = 'deviation_max_temperature'
            max_wind = 'deviation_max_wind'
            rain = 'deviation_rain'

        class ResultOutput:
            max_temperature_std = 'high temperature standard deviation'
            max_temperature_variance = 'high temperature variance'
            correct_max_temperature_forecast = 'correct high temperature forecast percentage'

            max_wind_std = 'high wind standard deviation'
            max_wind_variance = 'high wind variance'
            correct_max_wind_forecast = 'correct high wind forecast percentage'

            rain_std = 'rain standard deviation'
            rain_variance = 'rain variance'
            correct_no_rain_forecast = 'correct no rain forecast percentage'
            correct_little_rain_forecast = 'correct little rain forecast percentage'
            correct_heavy_rain_forecast = 'correct heavy rain forecast percentage'
            rain_ts = 'rain ts value'

        def __init__(self, data_header: list[DataHeader] = None):
            self.list_of_header = list()
            self.list_of_header.append(
                self.DataHeader(
                    ['站号', '站名', '经度', '纬度', '拔海高度', '雨量', '最高气温', '最低气温', '风速', '风向', '相对湿度', '本站气压', '最高气压', '最低气压'],
                    None,
                    '站数',
                    ['999999', '999017', '--', '------']
                )
            )
            self.list_of_header.append(
                self.DataHeader(
                    ['序号', '经度', '纬度', '雨量(mm)', '温度(℃)', '相对湿度(%)', 'U风', 'V风'],
                    ['序号', '经度', '纬度', '雨量(mm)', '温度(℃)', '相对湿度(%)', '风向(°)', '风速(m/s)'],
                    '格点数',
                    ['9999.00']
                )
            )
            if data_header is not None:
                for item in data_header:
                    self.list_of_header.append(item)
            self.selected_header = None

        merge_range = datetime.timedelta(days=1)

        def get_data_size_header_by_header(self, header: list[str]) -> str or None:
            ret = None
            for item in self.list_of_header:
                if item.header_check(header):
                    ret = item.data_size_header
                    break
            return ret

        def get_defined_header_by_header(self, header: list[str], select: bool = True) -> DataHeader:
            ret = None
            counter = 0
            for item in self.list_of_header:
                if item.header_check(header):
                    ret = item
                    self.selected_header = counter
                    break
                counter += 1
            return ret

        def get_column_index(self, column_header: str, header: list[str] = None) -> list[int]:
            ret = []
            if header is None:
                if self.selected_header is None:
                    return ret
                    # raise Exception("no column header defined")
                header = self.list_of_header.__getitem__(self.selected_header)
            for i in range(len(header)):
                if column_header in header[i]:
                    ret.append(i)
            return ret


'''
        # station_count_string_begin_with = '格点数：'
        # pre_defined_data_header = ['序号', '经度', '纬度', '雨量(mm)', '温度(℃)', '相对湿度(%)', 'U风', 'V风']
        # pre_processed_data_header = ['序号', '经度', '纬度', '雨量(mm)', '温度(℃)', '相对湿度(%)', '风向(°)', '风速(m/s)']
        # pre_defined_data_header = ['站号', '站名', '经度', '纬度', '拔海高度', '雨量', '最高气温', '最低气温', '风速', '风向', '相对湿度', '本站气压', '最高气压', '最低气压']
        pre_processed_data_header = None
        station_count_string_begin_with = '站数'
        column_size = len(pre_defined_data_header)'''
