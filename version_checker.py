import sys
import platform


class VersionChecker:
    # default minimum requirement: >= 3.7
    def __init__(self, major=3, minor=7, micro=None, suggest='3.9.6'):
        """
        Checking if current python version meets the minimum requirement
        :param major: sys.version_info.major, default 3 (required)
        :param minor: sys.version_info.minor, default 7 (required)
        :param micro: sys.version_info.micro, default None (optional)
        :param suggest: default 3.9.6 (my python version, optional)
        """
        self.major = major
        self.minor = minor
        self.micro = micro
        self.suggest = suggest

    def compare(self, version_info):
        """
        yes!
        :param version_info: sys.version_info
        :return: a boolean representing "is minimum requirement meet?"
        """
        assert isinstance(version_info, type(sys.version_info)), "Incorrect version info object"
        v1, v2, v3 = sys.version_info[0], sys.version_info[1], sys.version_info[2]
        expected_version_string = '{}.{}'.format(self.major, self.minor)
        if self.micro is not None:
            expected_version_string += '.{}'.format(self.micro)
        if v1 >= self.major and v2 >= self.minor and (self.micro is None or v3 >= self.micro):
            pass
        else:
            print("This script require Python {} or later, current {}".format(expected_version_string, platform.python_version()))
            return False
        if self.major < v1:
            print(
                "This script may not be compatible with Python {}. "
                "Suggest using version {} instead"
                    .format(expected_version_string, self.suggest)
            )
        return True


if __name__ == "__main__":
    print(sys.version)
    if VersionChecker().compare(sys.version_info):
        print("Your python should be able to run this project")
