from __future__ import annotations
import numpy as np
import netCDF4
from osgeo import osr, gdal
import osgeo
import datetime
import pathlib
import os
import pandas
import sys
import platform


class VersionChecker:
    # default minimum requirement: >= 3.7
    def __init__(self, major=3, minor=7, micro=None, suggest='3.9.6'):
        """
        Checking if current python version meets the minimum requirement
        :param major: sys.version_info.major, default 3 (required)
        :param minor: sys.version_info.minor, default 7 (required)
        :param micro: sys.version_info.micro, default None (optional)
        :param suggest: default 3.9.6 (my python version, optional)
        """
        self.major = major
        self.minor = minor
        self.micro = micro
        self.suggest = suggest

    def compare(self, version_info):
        """
        yes!
        :param version_info: sys.version_info
        :return: a boolean representing "is minimum requirement meet?"
        """
        assert isinstance(version_info, type(sys.version_info)), "Incorrect version info object"
        v1, v2, v3 = sys.version_info[0], sys.version_info[1], sys.version_info[2]
        expected_version_string = '{}.{}'.format(self.major, self.minor)
        if self.micro is not None:
            expected_version_string += '.{}'.format(self.micro)
        if v1 >= self.major and v2 >= self.minor and (self.micro is None or v3 >= self.micro):
            pass
        else:
            print("This script require Python {} or later, current {}".format(expected_version_string, platform.python_version()))
            return False
        if self.major < v1:
            print(
                "This script may not be compatible with Python {}. "
                "Suggest using version {} instead"
                    .format(expected_version_string, self.suggest)
            )
        return True


class Name:
    def get_weather_station_data_file_name(self, date: datetime.datetime, hour: int = None) -> str:
        name = date.strftime(CannotClassify.year_month_day_format)
        if hour is None:
            ext = '.*'
        else:
            ext = '.0'
            if hour < 10:
                ext += '0'
            ext = f'{ext}{hour}'
        return f'{name}{ext}'

    def get_weather_station_plain_text_cache_file_base_name(
            self,
            start_date: datetime.datetime,
            time_offset: datetime.timedelta,
            time_range: datetime.timedelta = None
    ):
        string = f'weather_station_plain_text_{start_date.strftime(CannotClassify.year_month_day_hour_format)}'
        if time_range not in [None, datetime.timedelta]:
            string += f'_{int(time_offset.total_seconds() / 3600)}h-{int((time_offset + time_range).total_seconds() / 3600)}h'
        elif time_offset not in [None, datetime.timedelta]:
            string += f'_{int(time_offset.total_seconds() / 3600)}h'
        return string

    def get_wind_direction_sheet_name(self, start_date: datetime.datetime, time_offset: datetime.timedelta):
        string = f'weather-verify_nc-wind-direction_{start_date.strftime(CannotClassify.year_month_day_hour_format)}'
        if time_offset not in [None, datetime.timedelta]:
            string += f'_{int(time_offset.total_seconds() / 3600)}h'
        return string

class CannotClassify:
    """
    I don't know where to put them so here they are
    """
    weather_filename_start_time_format = "%Y%m%d%H%M"
    thunder_data_datetime_format = "%Y%m%d%H%M"
    remote_directory_date_format = "%Y%m%d"
    year_format = "%Y"
    month_day_format = "%m%d"
    year_month_day_format = "%Y%m%d"
    year_month_day_hour_format = "%Y%m%d%H"
    publish_hour_offset = 2  # not using, defined in docx replace tool instead
    majority_low = 0.1
    majority_high = 0.9
    max_retries = 3
    
class Directory:
    output_directory = 'beta-output'  # testing only, overrides definition in __init__

    def __init__(self, output: str = None, reference: str = None, temporary: str = None, source: str = None):
        if output is not None:
            self.output_directory = output
        if reference is not None:
            self.reference_directory = reference,
        if temporary is not None:
            assert not pathlib.Path(temporary).absolute().__str__() in pathlib.Path('').absolute().__str__(), \
                "use program's parent directory as temporary work folder is a dangerous act (｡ŏ_ŏ)"
            self.temporary_directory = temporary
        if source is not None:
            self.input_directory = source

    def get_weather_station_data_directory(self, date: datetime.datetime) -> str:
        root = pathlib.Path(Directory.input_directory)
        sub = 'weather_station_plain_text/{}/{}'.format(date.strftime(CannotClassify.year_format), date.strftime(CannotClassify.month_day_format))
        return str(root.joinpath(sub))

    def get_weather_verify_output_directory(self, date: datetime.datetime) -> str:
        root = pathlib.Path(Directory.output_directory)
        sub = 'weather_verify/{}/{}'.format(date.strftime(CannotClassify.year_format),
                                            date.strftime(CannotClassify.month_day_format))
        return str(root.joinpath(sub))


class DataProperty:
    class WindDirection:
        NCFieldTag = "WD"
        FriendlyName = "wind-direction"


def save_tif(data_array_2d_s, path_s, geo_transform_s, geo_projection_s, filename_s: str,
             data_type_s: osgeo.gdalconst = gdal.GDT_Float32):
    """
    save 2d matrix to tif
    :param data_type_s: tif raster band data type, default [gdal.GDT_Float32]
    :param geo_projection_s: geo projection
    :param geo_transform_s: get transform
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param data_array_2d_s: data
    """
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    if not filename_s.endswith('.tif'):
        filename_s += '.tif'
    save_filename = os.path.join(path_s, filename_s)
    # print(data_array_2d_s.shape)
    print(f"Saving image {save_filename}: size = {data_array_2d_s.shape[1]}x{data_array_2d_s.shape[0]}")
    out_dataset = geo_tiff_driver.Create(save_filename, data_array_2d_s.shape[1], data_array_2d_s.shape[0], 1,
                                         data_type_s)
    if geo_transform_s is not None and geo_projection_s is not None:
        out_dataset.SetGeoTransform(geo_transform_s)
        out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(data_array_2d_s)
    out_dataset.FlushCache()


def save_data_sheet_to_xlsx(dataset_sheet: list[list[str]], header: list[str], path: str, name: str) -> bool:
    dataset = pandas.DataFrame(dataset_sheet, columns=header)
    full_path = pathlib.Path(path)
    if not full_path.exists():
        print(f"creating directory {path}")
        os.makedirs(path)
    if not name.endswith('.xlsx'):
        name += '.xlsx'
    full_path = full_path.joinpath(name)
    print(f"saving [{name}]...")
    try:
        writer = pandas.ExcelWriter(full_path)
        dataset.to_excel(writer, index=False)
        writer.save()
    except Exception as ex:
        print(f"failed to save [{name}], reason: {ex}")
        return False
    print(f"{name} saved")
    return True


def sub_process__extract_wind_direction():
    """
    Wind direction data extraction
    """
    nc_folder = "source data/weather_nc/2021/0901/"  # your nc files' parent folder, prefer relative path
    nc_files_full_path = pathlib.Path(nc_folder).glob('*.nc')
    wind_direction_data_output: list = list()
    for nc_file_full_path in nc_files_full_path:
        nc_filename = pathlib.Path(nc_file_full_path).name
        nc_start_time = nc_filename.split('_')[-2:-1][0]
        nc_start_time = datetime.datetime.strptime(
            nc_start_time, CannotClassify.weather_filename_start_time_format
        )
        output_directory = Directory().get_weather_verify_output_directory(nc_start_time)
        nc_dataset = netCDF4.Dataset(nc_file_full_path)
        nc_time_size = np.array(nc_dataset.variables["time"][:]).size
        wind_direction_data = nc_dataset[DataProperty.WindDirection.NCFieldTag][:, 0, :, :]
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size - 1], nc_lat_array[0]
        nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / (nc_lat_array_size - 1)
        nc_lat_need_flip = nc_lat_step > 0
        nc_lat_top = nc_lat_index_max if nc_lat_need_flip else nc_lat_index_min
        nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size - 1], nc_lon_array[0]
        nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / (nc_lon_array_size - 1)
        nc_lon_need_flip = nc_lon_step < 0
        nc_lon_left = nc_lon_index_min if not nc_lon_need_flip else nc_lon_index_max
        geo_transformation = (nc_lon_left, nc_lon_step if not nc_lon_need_flip else -nc_lon_step, 0, nc_lat_top, 0,
                              -nc_lat_step if nc_lat_need_flip else nc_lat_step)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
        geo_projection = srs.ExportToWkt()
        for i in range(nc_time_size):
            wind_direction_data_sheet = np.array(wind_direction_data[i, :, :])
            if nc_lat_need_flip:
                wind_direction_data_sheet = np.flip(wind_direction_data_sheet, 0)  # flip vertically
            if nc_lon_need_flip:
                wind_direction_data_sheet = np.flip(wind_direction_data_sheet, 1)  # flip horizontally
            filename = Name().get_wind_direction_sheet_name(nc_start_time, datetime.timedelta(hours=i))
            save_data_sheet_to_xlsx(wind_direction_data_sheet, None, output_directory, filename)
            save_tif(wind_direction_data_sheet, output_directory, geo_transformation, geo_projection, filename)
            formatted_wind_direction_data_sheet = wind_direction_data_sheet
            formatted_wind_direction_data_sheet[
                ((0 <= formatted_wind_direction_data_sheet) & (formatted_wind_direction_data_sheet <= 22.5)) | 
                ((360 >= formatted_wind_direction_data_sheet) & (formatted_wind_direction_data_sheet > 337.5))
                ] = 0  # 0/360 +- 22.5 (north)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 67.5) & (formatted_wind_direction_data_sheet > 22.5)
                ] = 1  # 45 +- 22.5 (north east)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 112.5) & (formatted_wind_direction_data_sheet > 67.5)
                ] = 2  # 90 +- 22.5 (east)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 157.5) & (formatted_wind_direction_data_sheet > 112.5)
                ] = 3  # 135 +- 22.5 (south east)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 202.5) & (formatted_wind_direction_data_sheet > 157.5)
                ] = 4  # 180 +- 22.5 (south)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 247.5) & (formatted_wind_direction_data_sheet > 202.5)
                ] = 5  # 225 +- 22.5 (south west)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 292.5) & (formatted_wind_direction_data_sheet > 247.5)
                ] = 6  # 270 +- 22.5 (west)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet <= 337.5) & (formatted_wind_direction_data_sheet > 292.5)
                ] = 7  # 315 +- 22.5 (north west)
            formatted_wind_direction_data_sheet[
                (formatted_wind_direction_data_sheet < 0) | (formatted_wind_direction_data_sheet > 7)
                ] = np.nan  # else (invalid)
            filename += '_formatted'
            save_data_sheet_to_xlsx(formatted_wind_direction_data_sheet, None, output_directory, filename)


if __name__.__eq__("__main__"):
    if VersionChecker().compare(sys.version_info):
        sub_process__extract_wind_direction()