import os
from shutil import copy

from osgeo import osr

from config_icy_thing_1_0_0 import *
from utility_0_1_1 import *
import netCDF4 as nC
import numpy as np
from ftp_file_fetcher_0_2_1 import FTPLoginInfo
from ftp_file_fetcher_0_2_1 import main_process as ftp_download_process


def get_weather_data(date: datetime.datetime):
    # this function is used to download today's weather data only (default)
    # subject 'today' is defined as 'at the date of running this script'
    # or can set to some specific date as wish
    print(f"Downloading weather data for date {date}")
    login_info = FTPLoginInfo(
        port=CannotClassify.ftp_port,
        address=CannotClassify.ftp_address,
        username=CannotClassify.ftp_username,
        password=CannotClassify.ftp_password
    )
    # date = date - datetime.timedelta(days=20)  # test only
    dir_set = Directory()
    remote_path = dir_set.get_expected_remote_weather_data(date)
    local_path = dir_set.get_weather_nc_directory(date)
    ftp_download_process(local_path, login_info, remote_path)


def forge_weather_2d_data(data_array_3d, friendly_field_name, time_step,
                          flip_horizontal, flip_vertical, time_array_len, lat_array_size, lon_array_size,
                          calculate_max=True):
    print(f"Extracting {friendly_field_name} data...")
    j = 0
    data = list()
    if friendly_field_name in [DataProperty.Rain.FriendlyName]:
        while j + time_step <= time_array_len:
            some_array = np.zeros((lat_array_size, lon_array_size))
            for i in range(j, j + time_step):
                some_array = np.add(some_array, data_array_3d[i, :, :])
            if flip_vertical:
                some_array = np.flip(some_array, 0)
            if flip_horizontal:
                some_array = np.flip(some_array, 1)
            data.append(some_array)
            j += time_step
        if time_array_len - j > 0:
            print_info_text(
                f"ditching remaining {time_array_len - j} {'sets' if time_array_len - j > 1 else 'set'} of data "
                f"for quantity less than {time_step}")
    elif friendly_field_name in [DataProperty.Temperature.FriendlyName, DataProperty.WindSpeed.FriendlyName]:
        while j + time_step <= time_array_len:
            some_array = np.zeros((lat_array_size, lon_array_size))
            for i in range(lat_array_size):
                for k in range(lon_array_size):
                    if calculate_max:
                        some_array[i, k] = np.max(data_array_3d[j:j + time_step - 1, i, k])
                    else:
                        some_array[i, k] = np.min(data_array_3d[j:j + time_step - 1, i, k])
            if flip_vertical:
                some_array = np.flip(some_array, 0)
            if flip_horizontal:
                some_array = np.flip(some_array, 1)
            data.append(some_array)
            j += time_step
        if time_array_len - j > 0:
            print_info_text(
                f"ditching remaining {time_array_len - j} {'sets' if time_array_len - j > 1 else 'set'} of data "
                f"for quantity less than {time_step}")
    else:
        print_error_text(f"cannot generate weather data, unknown field name {friendly_field_name}")
        return None
    print_success_text(f"Extracted {len(data)} {'sets' if len(data) > 1 else 'set'} of {friendly_field_name} data")
    return data


def weather_data_post_process(
        data_input_2d,
        data_name,
        temporary_work_place,
        output_folder,
        reference_folder,
        geo_transform=None,
        geo_projection=None
) -> str or None:
    _, name, ext, _ = get_file_path_name_extension(data_name)
    save_tif(data_input_2d, temporary_work_place, geo_transform, geo_projection, data_name)
    src_data_full_path = os.path.join(temporary_work_place, data_name)
    src_data_abs_path = os.path.abspath(src_data_full_path)
    if need_source_data:
        copy(src_data_full_path, os.path.join(output_folder, data_name))
        print_success_text(f"source tif data saved as [{os.path.join(output_folder, data_name)}]")

    is_success = True
    return_path = None
    total_retries = max(max_retries, 1)
    for tries in range(total_retries):  # avoid invalid max_retries settings, run this loop at least once
        if tries > 0:
            print_warning_text(f"retrying{tries}/{total_retries}")

        resampled_name = name + '_resampled'
        resampled_full_path = os.path.join(temporary_work_place, f"{resampled_name}.{ext}")
        resampled_data_abs_path = os.path.abspath(resampled_full_path)
        code = arcpy_resample_helper(input_absolute_path=src_data_abs_path,
                                     cell_size=DataProperty.IcyThing.resample_size,
                                     output_absolute_path=resampled_data_abs_path)
        if code != 0 and not give_warning_of_existed_file(resampled_full_path):
            print_error_text(f"resample execution failed, return code {code}")
            is_success = False
            continue
        elif code != 0 and give_warning_of_existed_file(resampled_full_path):
            print_warning_text("Resampled file found but process did not exit correctly. "
                               "Incomplete/damaged output file is possible")
        if need_other_data:
            copy(resampled_full_path, os.path.join(output_folder, f"{resampled_name}.{ext}"))
            print_success_text(f"[{os.path.join(output_folder, f'{resampled_name}.{ext}')}] saved")

        mask_reference_full_path = os.path.join(reference_folder, DataProperty.IcyThing.extract_by_mask_reference)
        mask_reference_abs_path = os.path.abspath(mask_reference_full_path)
        extracted_name = resampled_name + '_extracted-by-mask'
        extracted_full_path = os.path.join(temporary_work_place, f"{extracted_name}.{ext}")
        extracted_abs_path = os.path.abspath(extracted_full_path)
        code = arcpy_extract_by_mask_helper(input_absolute_path=resampled_data_abs_path,
                                            mask_absolute_path=mask_reference_abs_path,
                                            output_absolute_path=extracted_abs_path,
                                            ref_mask_abs_path=mask_reference_abs_path)
        if code != 0 and not give_warning_of_existed_file(extracted_full_path):
            print_error_text(f"extract bt mask execution failed, return code {code}")
            is_success = False
            continue
        elif code != 0 and give_warning_of_existed_file(extracted_full_path):
            print_warning_text("extract bt mask file found but process did not exit correctly. "
                               "Incomplete/damaged output file is possible")
        if need_other_data:
            copy(resampled_full_path, os.path.join(output_folder, f"{extracted_name}.{ext}"))
            print_success_text(f"[{os.path.join(output_folder, f'{extracted_name}.{ext}')}] saved")

        is_success = True
        return_path = extracted_full_path
        break
    if not is_success:
        print_error_text(f"max retries reached")
    return return_path


def ice_predict_calculate_result(temperature_min_path: str,
                                 wind_max_path: str,
                                 dem_ref_path: str,
                                 result_filename: str,
                                 temporary_work_place: str,
                                 output_folder: str,
                                 humidity_max_path: str = None,
                                 geo_transform=None,
                                 geo_projection=None
                                 ):
    """
    Calculate ice prediction result
    :param temperature_min_path:
    :param wind_max_path:
    :param dem_ref_path:
    :param result_filename:
    :param temporary_work_place:
    :param output_folder:
    :param humidity_max_path:
    :param geo_transform:
    :param geo_projection:
    :return: result file's relative path
    """
    if not give_warning_of_non_exist_directory(temporary_work_place):
        print_info_text("temporary work folder does not exist and will be created")
        os.makedirs(temporary_work_place)

    if not give_warning_of_existed_file(temperature_min_path):
        print_error_text(f"temperature data [{temperature_min_path}] not found, abort calculation")
        return
    temperature_min = read_tif_data(temperature_min_path)
    # 日最低气温
    temperature_min[temperature_min == temperature_min[0][0]] = np.nan
    temperature_min[(temperature_min > 0) | (temperature_min <= -20)] = -100
    temperature_min[(temperature_min > -3) & (temperature_min <= 0)] = 5
    temperature_min[(temperature_min > -9) & (temperature_min <= -3)] = 10
    temperature_min[(temperature_min > -20) & (temperature_min <= -9)] = 15

    if not give_warning_of_existed_file(wind_max_path):
        print_error_text(f"wind speed data [{wind_max_path}] not found, abort calculation")
        return
    wind_speed_max = read_tif_data(wind_max_path)
    # 最大风速
    wind_speed_max[wind_speed_max == wind_speed_max[0][0]] = np.nan
    wind_speed_max[wind_speed_max < 1] = -100
    wind_speed_max[(wind_speed_max >= 1) & (wind_speed_max < 6)] = 5
    wind_speed_max[(wind_speed_max >= 6) & (wind_speed_max < 10)] = 10
    wind_speed_max[wind_speed_max > 10] = 15

    if not give_warning_of_existed_file(dem_ref_path):
        print_error_text(f"temperature data [{dem_ref_path}] not found, abort calculation")
        return
    DEM = read_tif_data(dem_ref_path)
    # 高程
    DEM[DEM == DEM[0][0]] = np.nan
    DEM[DEM <= 500] = -100
    DEM[(DEM > 500) & (DEM <= 1000)] = 5
    DEM[(DEM > 1000) & (DEM <= 1400)] = 10
    DEM[DEM > 1400] = 15

    ice_predict = temperature_min + wind_speed_max + DEM
    if humidity_max_path is not None:
        # 平均相对湿度
        if not give_warning_of_existed_file(humidity_max_path):
            print_error_text(f"humidity data [{humidity_max_path}] not found")
        humidity_max = read_tif_data(humidity_max_path)  # data unavailable rn
        humidity_max[humidity_max == humidity_max[0][0]] = np.nan
        humidity_max[humidity_max < 85] = -100
        humidity_max[(humidity_max >= 85) & (humidity_max < 90)] = 5
        humidity_max[(humidity_max >= 90) & (humidity_max < 95)] = 10
        humidity_max[humidity_max >= 95] = 15
        ice_predict += humidity_max
    ice_predict[ice_predict < 0] = 0
    print_success_text("icy prediction calculated")
    return ice_predict

    calculated_result_full_path = os.path.join(temporary_work_place, result_filename)
    geoTiffDriver = gdal.GetDriverByName("GTiff")
    outDataset = geoTiffDriver.Create(calculated_result_full_path, ice_predict.shape[1], ice_predict.shape[0], 1,
                                      gdal.GDT_Float32)
    outDataset.GetRasterBand(1).WriteArray(ice_predict)

    if geo_transform is not None and geo_projection is not None:
        outDataset.SetGeoTransform(geo_transform)
        outDataset.SetProjection(geo_projection)
    if Switches.save_render_data:
        if not give_warning_of_non_exist_directory(output_folder):
            print_info_text("output folder does not exist and will be created")
            os.makedirs(output_folder)
        render_data_save_full_path = os.path.join(output_folder, result_filename)
        copy(calculated_result_full_path, render_data_save_full_path)
        print_success_text(f"render data saved as [{render_data_save_full_path}]")
    return calculated_result_full_path


def calculate_process_main(date: datetime.datetime):
    output_directory = Directory().get_icy_output_path(date)
    input_directory = Directory().get_weather_nc_directory(date)
    reference_directory = Directory.reference_directory
    temporary_directory = Directory().get_icy_temporary_workplace()
    if not give_warning_of_non_exist_directory(output_directory):
        print_info_text("output folder is being created")
        os.makedirs(output_directory)
    if not give_warning_of_non_exist_directory(temporary_directory):
        print_info_text("creating working directory")
        os.makedirs(temporary_directory)
    elif len(os.listdir(temporary_directory)) > 0:
        print_info_text("cleaning working directory")
        clean_temporary_folder(temporary_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print_error_text("input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print_error_text("reference folder not found")
        exit(2)
    nc_data_path = glob.glob(os.path.join(input_directory, Name.weather_data_file))
    for item in nc_data_path:
        print(f"Processing nc dataset {item}")
        item_date = item.split('_')[-2:-1][0]
        datetime_item_date = datetime.datetime.strptime(item_date, CannotClassify.weather_filename_start_time_format)
        report_file_name = Name().get_weather_nc_report_name(datetime_item_date)
        report_file_path = output_directory
        '''
        if not Switches.ignore_existing_report and need_report:
            report_file_full_path = os.path.join(report_file_path, report_file_name)
            if give_warning_of_existed_file(report_file_full_path, True):
                print(f"INFO {report_file_name} exists, skipping {item}")
                continue
        '''
        extracted_path = None
        global need_report
        has_things_to_do = need_source_data or need_render_data or need_png
        if not has_things_to_do and not need_report:
            print_warning_text(f"Current processing settings will do nothing to dataset [{item}]. "
                               f"Check your config and try again.")
            exit(-1)
        if not has_things_to_do and need_report:
            if give_warning_of_existed_file(
                    os.path.join(report_file_path, report_file_name)) and not over_write_existing_report:
                print_info_text(f"report named [{report_file_name}] exists, ignore and process next dataset")
                continue
        if need_report:
            if not os.path.exists(os.path.join(Directory.reference_directory, Name.icy_docx_template)):
                print_error_text(f"cannot find report template file. report generate will be disabled.")
                need_report = False
            else:
                extracted_path = load_docx_template(
                    os.path.join(reference_directory, Name.icy_docx_template),
                    docx_sub_path=Directory.icy_sub_docx_extract_path
                )
                print("Report template loaded")

        print(f"Start date of NC dataset is: {datetime_item_date}")
        nc_dataset = nC.Dataset(item)  # 读取nc数据集
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_time_array = np.array(nc_dataset.variables["time"][:])
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_time_step = 24
        nc_time_unit = 'h'
        nc_start_datetime = utc_timestamp_sec_to_datetime_since_1990(nc_time_array[0])
        nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size - 1], nc_lat_array[0]
        nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / (nc_lat_array_size - 1)
        nc_lat_need_flip = nc_lat_step > 0
        nc_lat_top = nc_lat_index_max if nc_lat_need_flip else nc_lat_index_min
        nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size - 1], nc_lon_array[0]
        nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / (nc_lon_array_size - 1)
        nc_lon_need_flip = nc_lon_step < 0
        nc_lon_left = nc_lon_index_min if not nc_lon_need_flip else nc_lon_index_max
        geo_transformation = (nc_lon_left, nc_lon_step if not nc_lon_need_flip else -nc_lon_step, 0, nc_lat_top, 0,
                              -nc_lat_step if nc_lat_need_flip else nc_lat_step)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
        geo_projection = srs.ExportToWkt()
        timestamp = get_timestamp_filename(utc_timestamp_sec_to_datetime_since_1990(nc_time_array[0]))

        temperature_data = forge_weather_2d_data(data_array_3d=nc_dataset[DataProperty.Temperature.NCFieldTag][:, :, :],
                                                 friendly_field_name=DataProperty.Temperature.FriendlyName,
                                                 time_step=nc_time_step, flip_horizontal=nc_lon_need_flip,
                                                 flip_vertical=nc_lat_need_flip, time_array_len=len(nc_time_array),
                                                 lat_array_size=nc_lat_array_size, lon_array_size=nc_lon_array_size,
                                                 calculate_max=False)
        wind_speed_data = forge_weather_2d_data(data_array_3d=nc_dataset[DataProperty.WindSpeed.NCFieldTag][:, 0, :, :],
                                                friendly_field_name=DataProperty.WindSpeed.FriendlyName,
                                                time_step=nc_time_step, flip_horizontal=nc_lon_need_flip,
                                                flip_vertical=nc_lat_need_flip, time_array_len=len(nc_time_array),
                                                lat_array_size=nc_lat_array_size, lon_array_size=nc_lon_array_size)
        sets_of_data = np.min([len(temperature_data), len(wind_speed_data)])
        if sets_of_data <= 0:
            raise Exception(f"Unexpected numbers of dataset: expected 1 or more, got {sets_of_data}")
        print_info_text(f"weather contains {sets_of_data} set(s) of data")
        time_hour_offset = 0
        for index in range(sets_of_data):
            temperature_data_set = weather_data_post_process(
                data_input_2d=temperature_data[index],
                data_name=Name().get_nc_dataset_tif_name(data_time=datetime_item_date,
                                                         start_time=f"{time_hour_offset}{nc_time_unit}",
                                                         end_time=f"{time_hour_offset + nc_time_step}{nc_time_unit}",
                                                         data_field=DataProperty.Temperature.FriendlyName),
                temporary_work_place=temporary_directory,
                output_folder=output_directory,
                reference_folder=reference_directory,
                geo_transform=geo_transformation,
                geo_projection=geo_projection
            )
            wind_speed_data_set = weather_data_post_process(
                data_input_2d=wind_speed_data[index],
                data_name=Name().get_nc_dataset_tif_name(data_time=datetime_item_date,
                                                         start_time=f"{time_hour_offset}{nc_time_unit}",
                                                         end_time=f"{time_hour_offset + nc_time_step}{nc_time_unit}",
                                                         data_field=DataProperty.WindSpeed.FriendlyName),
                temporary_work_place=temporary_directory,
                output_folder=output_directory,
                reference_folder=reference_directory,
                geo_transform=geo_transformation,
                geo_projection=geo_projection
            )
            calculated_result_filename = Name().get_icy_calculated_result_tif_filename(datetime_item_date,
                                                                                       time_hour_offset, nc_time_step)
            calculated_geo_transform = return_dataset(temperature_data_set).GetGeoTransform()
            calculated_geo_projection = return_dataset(temperature_data_set).GetProjection()
            calculated_result_full_path = os.path.join(temporary_directory, calculated_result_filename)
            calculated_data = ice_predict_calculate_result(
                temperature_min_path=temperature_data_set,
                wind_max_path=wind_speed_data_set,
                dem_ref_path=Directory().get_icy_dem_reference_full_path(),
                result_filename=calculated_result_filename,
                temporary_work_place=temporary_directory,
                output_folder=output_directory,
                geo_transform=calculated_geo_transform,
                geo_projection=calculated_geo_projection
            )
            save_tif(calculated_data, temporary_directory, calculated_geo_transform, calculated_geo_projection,
                     calculated_result_filename)
            if Switches.save_render_data:
                render_data_save_full_path = os.path.join(output_directory, calculated_result_filename)
                copy(calculated_result_full_path, render_data_save_full_path)
                print_success_text(f"render data saved as [{render_data_save_full_path}]")

            total_tries = max(max_retries, 1)
            is_success = True
            for tries in range(total_tries):
                if tries > 0:
                    print_warning_text(f"Retrying {tries}/{total_tries}")

                _, name, _, _ = get_file_path_name_extension(calculated_result_filename)
                png_filename = f"{name}.png"
                png_full_path = os.path.join(temporary_directory, png_filename)
                png_abs_path = os.path.abspath(png_full_path)
                input_list = [os.path.abspath(calculated_result_full_path)]
                input_index_list = [DataProperty.IcyThing.mxd_data_replace_index]
                text_content_list = [DataProperty.IcyThing().get_mxd_time_text(
                    datetime_item_date + datetime.timedelta(hours=time_hour_offset),
                    datetime.timedelta(hours=nc_time_step)
                )]
                text_name_list = [DataProperty.IcyThing.mxd_time_textbox_name]
                code = arcpy_mxd_to_png_helper(
                    mxd_absolute_path=os.path.abspath(
                        os.path.join(Directory().get_icy_mxd_template_path(), Name.icy_mxd_template)),
                    input_file_absolute_path=input_list,
                    image_output_absolute_path=png_abs_path,
                    replace_index=input_index_list,
                    text_content=text_content_list,
                    text_name=text_name_list,
                    dpi=ReportProperties.image_output_dpi
                )
                if code != 0 and not give_warning_of_existed_file(png_full_path):
                    is_success = False
                    print_error_text(f"png file export failed, process returned {code}")
                    continue
                elif code != 0 and give_warning_of_existed_file(png_full_path):
                    print_warning_text("PNG file found but process did not exit correctly. "
                                       "Incomplete/damaged file is possible")
                if need_png:
                    copy(png_full_path, os.path.join(output_directory, png_filename))
                    print_success_text(f"png image saved as [{os.path.join(output_directory, png_filename)}]")
                if need_report:
                    day_x_keyword = ReportProperties.KeyWords.day_x_keyword
                    day_x = ReportProperties.KeyWords.day_x
                    this_day = day_x.replace(day_x_keyword, str(index))
                    this_day_datetime = datetime_item_date + datetime.timedelta(days=index)
                    this_day_str = ReportProperties.Formats().string_year_month_day_hour_format(this_day_datetime)
                    report_text_replace(this_day, this_day_str, extracted_path)
                    report_image_replace(png_full_path,
                                         f'image{ReportProperties.report_image_index_offset + index}.png',
                                         extracted_path)
                is_success = True
                break
            if not is_success:
                print_error_text("max retries reached")
            time_hour_offset += nc_time_step
        if need_report:
            day_x_keyword = ReportProperties.KeyWords.day_x_keyword
            day_x = ReportProperties.KeyWords.day_x
            this_day = day_x.replace(day_x_keyword, str(sets_of_data))
            this_day_datetime = datetime_item_date + datetime.timedelta(days=float(sets_of_data))
            this_day_str = ReportProperties.Formats().string_year_month_day_hour_format(this_day_datetime)
            report_text_replace(this_day, this_day_str, extracted_path)

            zip_docx_report(report_file_path, report_file_name, extracted_path)
            clean_temporary_folder(extracted_path)
            print_success_text(f"report saved as [{report_file_name}]")
            if need_pdf:
                code = docx_to_pdf(input_docx_full_path=os.path.join(report_file_path, report_file_name),
                                   try_other_method=True)
                if code == 0:
                    print_success_text("report converted to pdf")
    clean_temporary_folder(temporary_directory)


need_source_data = Switches.save_input_data
need_render_data = Switches.save_render_data
need_other_data = Switches.save_other_data
need_png = Switches.save_png_image
need_report = Switches.need_report
need_pdf = Switches.convert_pdf
over_write_existing_report = Switches.ignore_existing_report
max_retries = CannotClassify.max_retries


def main(date: datetime.datetime = datetime.datetime.now()) -> None:
    """
    the main process,  auto download then calculate
    :return:
    """
    print("Icy Thing (ice predict) starting...")
    get_weather_data(date)
    calculate_process_main(date)
    print("Icy Thing (ice predict) finishing...")


if __name__ == '__main__':
    main()
