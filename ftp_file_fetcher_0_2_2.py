import datetime
import ftplib
import os.path
import pathlib
import dateutil.parser
from datetime import time
from ftplib import FTP
import sys

from utility_0_1_1 import print_error_text, print_warning_text, print_info_text, print_success_text, annotations
from config_1_1_6 import CannotClassify

"""
some code are copied from FTP_download.py by [__author__ = "Maylon"]
"""


class FTPLoginInfo:
    def __init__(self, address: str, port: int = 21, username: str = 'anonymous', password: str = ''):
        self.port = port
        self.address = address
        self.username = username
        self.password = password


class Logger(object):

    def __init__(self, stream=sys.stdout, *output_dir):
        if not os.path.exists(str(*output_dir)):
            os.makedirs(str(*output_dir))
        log_name = '{}.log'.format(time.strftime('%Y-%m-%d-%H-%M'))
        filename = os.path.join(str(*output_dir), log_name)

        self.terminal = stream
        self.log = open(filename, 'a+')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass


def size_formatter(size):
    suffix = ['B', 'KB', 'MB', 'GB', 'TB']
    index = 0
    while index < len(suffix) - 1 and size > 1024:
        size = round(size / 1024, ndigits=2)
        index += 1
    return '{0}{1}'.format(str(size), suffix[index])


# 判断远程文件和本地文件大小是否一致
def is_same_size(ftp, local_file, remote_file) -> bool:
    """
    参数:
        ftp：FTP()创建的对象
        local_file：本地文件
        remote_file：远程文件
    """
    # 获取远程文件的大小
    try:
        remote_file_size = ftp.size(remote_file)
    except Exception as e:
        print_error_text("while checking remote file size, reason %s" % e)
        remote_file_size = -1
    # 获取本地文件的大小
    try:
        local_file_size = os.path.getsize(local_file)
    except Exception as e:
        print_error_text("while checking local file size, reason %s" % e)
        local_file_size = -2
    # 比较文件大小，大小相等返回True，否则返回False
    if remote_file_size == local_file_size:
        result = True
    else:
        result = False
    return result


# ftp连接
def ftp_connect(ftp_address, port, username, password, timeout=60) -> FTP or None:
    """
    参数：
        ip_addr：ip地址
        port：端口号
        username：用户名
        password：密码
        timeout: seconds to wait before aborting action (avoid process hang)
    """
    ftp = FTP(timeout=timeout)
    try:
        print(f"Connecting to {ftp_address}....", end="", flush=True)
        ftp.connect(ftp_address, port)  # 连接FTP服务器
        print_success_text("Connected")
        print(ftp.getwelcome())
        print(f"Logging in as {username}....", end="", flush=True)
        ftp.login(username, password)  # 登录
        print_success_text("Logged in")
        return ftp
    except Exception as e:
        print_error_text(f"Connect to {ftp_address} failed, reason: {e}")
        return None  # return NONE when connection failed


def is_directory(ftp, subject, last_directory='..') -> bool:
    try:
        ftp.cwd(subject)  # 判断是否为目录，若是目录则切换到目录下，否则出现异常
        ftp.cwd(last_directory)  # 切换进目录后需要返回上一级
        return True
    except Exception:
        return False


class WriteFile:
    def __init__(self, file, file_size, block_size):
        self.file_full_path = file
        self.file_size = file_size
        self.block_size = block_size

    def handle(self, data):
        print()


# 文件下载
def download_file(ftp, local_file_full_path, remote_file) -> bool:
    """
    参数：
        ftp：FTP()创建的对象
        local_file：本地文件
        remote_file：远程文件
    """
    # 判断远端文件是否存在
    if remote_file not in ftp.nlst():
        print_info_text(f"{remote_file} not exist in remote site, aborting action")  # 若远端文件不存在则打印错误信息
        return False
    buf_size = 8192
    # 判断本地文件是否存在
    if os.path.exists(local_file_full_path) and is_same_size(ftp, local_file_full_path, remote_file):
        print_info_text(f"file {remote_file} with same size exists in local directory, cancel download")
        return True
    else:
        remote_file_size_txt = size_formatter(ftp.size(remote_file))
        with open(local_file_full_path, 'wb') as local_file_stream:
            global progress
            progress = 0

            def write_data(data):
                global progress
                local_file_stream.write(data)
                progress += len(data)
                print(f"\rDownloading：{remote_file}...({size_formatter(progress)}/{remote_file_size_txt})", end="")

            ftp.retrbinary(f'RETR {remote_file}', write_data, buf_size)
        # 判断文件大小是否相等，不相等则重新下载
        if is_same_size(ftp, local_file_full_path, remote_file):
            print_success_text(f"\rDownloading：{remote_file}...downloaded")
            return True
        else:
            print()
            global retries_max, retries_count
            if retries_count < retries_max:
                retries_count += 1
                print_warning_text(f"File size not match, re-trying..({retries_count}/{retries_max})..")
                download_file(ftp, local_file_full_path, remote_file)
            else:
                print_error_text(f"Could not download file after {retries_max} attempts, aborting")
                retries_count = 0
                return False


# 下载整个目录下的文件
def download_dir(ftp, local_full_path, remote_path, include_parent_dir=False) -> bool:
    """
    参数：
        ftp：FTP()创建的对象
        local_dir：本地路径
        remote_path：远程目录
    """
    # get the folder subject to download
    global has_no_error
    if not os.path.exists(local_full_path):
        print(f"INFO local download path {local_full_path} not exists and will be created")
        os.makedirs(local_full_path)  # 可能是多级目录，需要用到makedirs , create local download folder before downloading
    try:
        ftp.cwd(remote_path)  # 切换到远程目录
    except Exception:
        print_error_text(f"remote path {remote_path} not found")
        return False
    if include_parent_dir:
        local_full_path = os.path.join(local_full_path, remote_path)
        os.mkdir(local_full_path)
    file_list = ftp.nlst()  # 获取下载文件列表
    for file_name in file_list:
        if is_directory(ftp, file_name):
            to_return = download_dir(ftp, local_full_path, file_name, include_parent_dir=True)  # 递归下载目录
            ftp.cwd("..")
        else:
            to_return = download_file(ftp, os.path.join(local_full_path, file_name), file_name)
        if not to_return:
            print_error_text(f"An error occured while downloading {file_name}")
        has_no_error = has_no_error and to_return
        # print({has_no_error})
    return has_no_error


retries_count = 0
retries_max = CannotClassify.max_retries


def old_way(local_path_to_download, login_info_obj, remote_path_to_download) -> bool:
    global has_no_error
    has_no_error = True
    print("ftp file fetcher starting")
    if not isinstance(login_info_obj, FTPLoginInfo):
        print_error_text(f"invalid login info, expected {type(FTPLoginInfo)} got {type(login_info_obj)}")
        return False
    print(f"FTP address: {login_info_obj.address}:{login_info_obj.port}\n"
          f"Login username: {login_info_obj.username}")
    ftp_object = ftp_connect(ftp_address=login_info_obj.address, port=login_info_obj.port,
                             username=login_info_obj.username, password=login_info_obj.password)
    to_return = True
    if ftp_object is not None:
        if is_directory(ftp_object, remote_path_to_download, ftp_object.pwd()):
            to_return = download_dir(ftp_object, local_path_to_download, remote_path_to_download)
        else:
            if remote_path_to_download.endswith('/'):
                remote_path_to_download = remote_path_to_download[:-1]
            if remote_path_to_download.endswith('\\'):
                remote_path_to_download = remote_path_to_download[:-1]
            file_name = remote_path_to_download.split('/')[-1:][0].split('\\')[-1:][0]
            if not os.path.exists(local_path_to_download):
                print_info_text(f"local download path {local_path_to_download} not exists and will be created")
                os.makedirs(local_path_to_download)  # create local download folder before downloading
            local_path_to_download = os.path.join(local_path_to_download, file_name)
            to_return = download_file(ftp_object, local_path_to_download, remote_path_to_download)
            if not to_return:
                print_error_text(f"An error occured while downloading {file_name}")
        has_no_error = has_no_error and to_return
    else:
        print_error_text("An error occurred while communicating with the server")
        has_no_error = False
    print(f"ftp file fetcher stopping ({has_no_error})")
    return has_no_error


# above are old codes copied from one of my senpai...
# ===================SPLITER===================
# below are new codes by me (use MLSD to reduce time in size check)

class FtpRemoteFileInfo:
    class Constant:
        file_type = 'type'
        file_size = 'size'
        file_modify = 'modify'
        file_type_directory = 'dir'
        file_type_file = 'file'

    def __init__(self, info: tuple[str, dict[str, str]]):
        self.filename: str = info[0]
        self.file_type: str = info[1][self.Constant.file_type]
        if self.Constant.file_type_directory == self.file_type:
            pass
        elif self.Constant.file_type_file == self.file_type:
            self.file_size = info[1][self.Constant.file_size]
        else:
            raise print_error_text(f"unknown object type: {self.file_type}")
        try:
            self.file_modify: datetime.datetime = dateutil.parser.parse(info[1][self.Constant.file_modify])
        except Exception as ex:
            raise print_error_text(f"Cannot set file/directory modify, reason: {ex}")

    def get_name(self) -> str:
        return self.filename

    def get_modify(self) -> datetime.datetime:
        return self.file_modify

    def get_type(self) -> str:
        return self.file_type

    def get_size(self) -> str or None:
        """
        return ftp remote file size, return None if given 'file' is a directory
        :return: size string or None
        """
        return self.file_size

    def check_if_same_modify(self, modify: datetime.datetime) -> bool:
        """
        check if given file/directory object has the same modify time
        difference less than 1 second is considered 'same'
        :param modify: given file/directory
        :return: is same?
        """
        return abs((self.file_modify - modify).total_seconds()) < 1


def size_formatter_new(size: str) -> str:
    suffix = ['B', 'KB', 'MB', 'GB', 'TB']
    index = 0
    if not (str.isdigit(size)):
        raise RuntimeError(f"invalid file size type, expect {type(str)}, got {type(size)}")
    size = int(size)
    while index < len(suffix) - 1 and size > 1024:
        size = round(size / 1024, ndigits=2)
        index += 1
    return '{0}{1}'.format(str(size), suffix[index])


def check_file_is_same(remote_file_info: FtpRemoteFileInfo, local_file_full_path: str) -> bool:
    """
    check remote file and local file's property and see if there is any difference in modify time and size as well as file existence
    :param remote_file_info: an item of ftplib.mlst results
    :param local_file_full_path:
    :return: True if modify and size is the same, or otherwise
    """
    local_file_stat = pathlib.Path(local_file_full_path).stat()
    if not pathlib.Path(local_file_full_path).exists():
        return False
    local_file_size = str(local_file_stat.st_size)
    local_file_modify = datetime.datetime.fromtimestamp(local_file_stat.st_mtime)
    # print(f"local: size={local_file_size}, modify={local_file_modify}, sec={local_file_modify.second}")
    remote_file_size = remote_file_info.file_size
    remote_file_modify = remote_file_info.file_modify
    # print(f"remote: size={remote_file_size}, modify={remote_file_modify}, sec={remote_file_modify.second}")
    if remote_file_info.file_type == FtpRemoteFileInfo.Constant.file_type_directory or not pathlib.Path(
            local_file_full_path).is_file():
        print_error_text("remote path or local path is not a file")
        return False
    # print((remote_file_modify - local_file_modify).total_seconds())
    is_modify_same = remote_file_info.check_if_same_modify(local_file_modify)
    return remote_file_size == local_file_size and is_modify_same


def download_file_new(remote_info: FtpRemoteFileInfo, remote_full_path: str, local_full_path: str,
                      ftp_object: ftplib.FTP) -> bool:
    remote_file_size_txt = size_formatter_new(remote_info.file_size)
    remote_file_name = remote_info.filename
    with open(local_full_path, 'wb') as local_file_stream:
        global progress
        progress = 0

        def write_data(data):
            global progress
            local_file_stream.write(data)
            progress += len(data)
            print(
                f"\rDownloading：{remote_file_name}...({size_formatter(progress)}/{remote_file_size_txt})",
                end=""
            )

        ftp_object.retrbinary(
            f'RETR {remote_full_path}',
            write_data,
            CannotClassify.ftp_download_buff_size)
    os.utime(local_full_path, (remote_info.file_modify.timestamp(), remote_info.file_modify.timestamp()))
    if check_file_is_same(remote_info, local_full_path):
        print_success_text(f"\rDownloading：{remote_info.filename}...downloaded")
        return True
    else:
        print_error_text(f"\rDownloading：{remote_info.filename}...file check failed")
        return False


def new_way(local_path_to_download: str, login_info_obj: FTPLoginInfo, remote_path_to_download: str,
            include_parent_dir: bool = False) -> bool:
    print("ftp file fetcher starting")
    if not isinstance(login_info_obj, FTPLoginInfo):
        print_error_text(f"invalid login info, expected {type(FTPLoginInfo)} got {type(login_info_obj)}")
        return False
    print(f"FTP address: {login_info_obj.address}:{login_info_obj.port}\n"
          f"Login username: {login_info_obj.username}")
    max_retries_loop = max(retries_max, 1)
    ftp_object = None
    for i in range(max_retries_loop):
        if i > 0:
            print_warning_text(f"Retrying ({i}/{max_retries_loop-1})....")
        ftp_object = ftp_connect(ftp_address=login_info_obj.address, port=login_info_obj.port,
                                 username=login_info_obj.username, password=login_info_obj.password)
        if ftp_object is not None:
            break
    global has_no_error

    mlsd_yes = False
    if ftp_object is not None:
        features: list[str] = list()

        try:
            feats = ftp_object.sendcmd('feat')
            feats = feats.split('\n')
            for feat in feats:
                feat = feat.strip()
                if feat[:3].__eq__('211'):
                    continue
                features.append(feat)
                if 'MLSD' in feat.upper():
                    mlsd_yes = True
        except Exception as ex:
            print_error_text(f"fatal error: {ex}")
            exit(-1)

        print(f"Server features: {features}")
        if not mlsd_yes:
            print_warning_text("This server does not support 'MLSD' command, switching to backup download process")
            return old_way(local_path_to_download, login_info_obj, remote_path_to_download)

        if include_parent_dir:
            parent, child = os.path.split(remote_path_to_download)
            if child != '':
                child = child.strip('/').strip('\\')
                parent = parent.strip('/').strip('\\')
                is_success = True
                for tries in range(max(retries_max, 1)):
                    try:
                        temp = ftp_object.mlsd(parent)
                        for item in temp:
                            temp = FtpRemoteFileInfo(info=item)
                            if temp.filename == child:
                                if temp.file_type == FtpRemoteFileInfo.Constant.file_type_file:
                                    local_path_to_download = os.path.join(local_path_to_download, parent)
                                else:
                                    local_path_to_download = os.path.join(local_path_to_download, child)
                        if not os.path.exists(local_path_to_download):
                            print_info_text(f"Creating download directory [{local_path_to_download}]...")
                            os.makedirs(local_path_to_download)
                        is_success = True
                        break
                    except ftplib.error_perm as er:
                        print_error_text(f"An error occurred: {er}")
                        is_success = False
                if not is_success:
                    print_error_text("max retries reached")
                    return False
        download_list = list()
        download_list.append(remote_path_to_download)
        while len(download_list) > 0:
            object_to_download = download_list.pop()
            to_return = True
            for tries in range(max_retries_loop):
                if tries > 0:
                    print_warning_text(f"Retrying.... ({tries}/{max_retries_loop})")
                try:
                    name_list = ftp_object.nlst(object_to_download)
                    better_name_list = ftp_object.mlsd(object_to_download)
                except ftplib.error_perm as resp:
                    if "550" in str(resp):
                        print_warning_text(f"given file or directory not found: {object_to_download}")
                        has_no_error = False
                        break
                    else:
                        print_error_text(f"Ftp fatal error, reason: {resp}")
                        to_return = False
                        continue
                except Exception as ex:
                    print_error_text(f"Ftp fatal error, reason: {ex}")
                    to_return = False
                    continue
                relative_path = object_to_download.replace(remote_path_to_download, '').strip('/').strip('\\')
                file_count = len(name_list)
                if file_count == 0:
                    continue
                for item in better_name_list:
                    remote_file_info = FtpRemoteFileInfo(item)
                    the_name = remote_file_info.filename
                    the_file_type = remote_file_info.file_type
                    if the_file_type == FtpRemoteFileInfo.Constant.file_type_directory:
                        path = os.path.join(remote_path_to_download, relative_path, the_name)
                        if not os.path.exists(path):
                            os.makedirs(path)
                        download_list.append(path)
                    elif the_file_type == FtpRemoteFileInfo.Constant.file_type_file:
                        remote_file = os.path.join(remote_path_to_download, relative_path, the_name)
                        local_file = os.path.join(local_path_to_download, relative_path, the_name)
                        if check_file_is_same(remote_file_info, local_file):
                            print_info_text("An existing file already in local directory, skipped download")
                            continue
                        is_success = False
                        for retry in range(max_retries_loop):
                            if retry > 0:
                                print_warning_text(f"retrying... {retry}/{max_retries_loop}")
                            if download_file_new(
                                remote_info=remote_file_info,
                                remote_full_path=remote_file,
                                local_full_path=local_file,
                                ftp_object=ftp_object
                            ):
                                is_success = True
                                break
                        if not is_success:
                            to_return = False
                            print_error_text(f"max retries reached")
                if not to_return:
                    print_warning_text("some error occurred during download")
                else:
                    break
            has_no_error = has_no_error and to_return
            if not to_return:
                print_error_text("max retries reached")
    else:
        print_error_text("An error occurred while communicating with the server")
        has_no_error = False
    print("ftp file fetcher stopping")
    return has_no_error


def main_process(local_path_to_download, login_info_obj, remote_path_to_download,
                 include_parent_directory=False) -> bool:
    return new_way(local_path_to_download, login_info_obj, remote_path_to_download, include_parent_directory)
    # return old_way(local_path_to_download, login_info_obj, remote_path_to_download)


has_no_error = True
progress = 0

if __name__ == "__main__":
    print("i donno what to download so have a nice day ;)")
