from datetime import datetime, timedelta
import pathlib

import converter_0_0_1 as convert


def main():
    # test_objects = ['input/pre_20210715080000.tif']
    test_objects = pathlib.Path('input/').glob('*.tif')
    test_output_directory = pathlib.Path('output')
    for test_object in test_objects:
        filepath = pathlib.Path(test_object)
        title = filepath.name.replace(filepath.suffix.__str__(), '')
        time_str = title.split('_')[1]
        time: datetime = datetime.strptime(time_str, '%Y%m%d%H%M%S')
        effective: timedelta = timedelta(hours=1)

        # don't mind this, its for debug purpose
        '''
        print(
            f"{filepath.parent}\n"
            f"{filepath.name}\n"
            f"{filepath.suffix}\n"
            f"{title}\n"
            f"{time.__str__()}\n"
        )
        '''

        header1 = convert.MicapsType4AdditionalHeader(title, time, effective)
        type4_data = convert.Tif2MicapsType4(test_object, header1)
        type4_data.save_text_as_micaps_data_type_4(test_output_directory.joinpath(f'{title}_micapsType4.txt'), overwrite_if_exist=True)


if __name__ == "__main__":
    print("ready get set go")
    main()
    print("and stop")
