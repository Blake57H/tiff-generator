import glob
import os
from datetime import datetime, timedelta
import tifffile
import pathlib


def loop_print(subject):
    for item in subject:
        print(item)


from numpy import array

tif_file = pathlib.Path("input\pre_20210715080000.tif")

tif = tifffile.TiffFile(tif_file.__str__())
for page in tif.pages:
    for tag in page.tags:
        tag_name, tag_value = tag.name, tag.value
        # print(f"name={tag_name}, value={tag_value}")
    print("================")
    print(
        # f"{page.tags['CellWidth']} \n"
        # f"{page.tags['CellLength']} \n"
        f"{page.tags.__str__()} \n"
        # f"{page.tags['ResolutionUnit']} \n"
    )
    print("================")

exit(0)

dir = pathlib.Path('input/')
loop_print(dir.glob('*.tif'))
loop_print(glob.glob('input/*.tif'))
exit(0)

print(len(tif.pages))


class BClass:
    def __init__(self):
        pass


b = BClass()

a = "abdjd"
print(type(b))
assert isinstance(b, BClass)

image = tif.pages[0]

data_body: array = image.asarray()

print(data_body)
print(len(data_body.shape))

print(image.shape)

print(image.dtype)

with open('./output/size test.txt') as result:
    text = result.readlines()
    lines = len(text)
    word = 0
    for line in text:
        word += len(line.split())
    print(f"{lines}, {word}")
