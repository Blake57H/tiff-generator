# general/built-in package import (site-packages import later)
import os
import typing
from datetime import datetime, timedelta
import sys
import platform
import pathlib

if __name__ == "__main__":
    print("I cannot run on my own. Use a launcher to call my functions instead.")
    exit(0)
else:
    # python version check
    # This script is tested with python 3.9.6.
    # Anything equal to or higher than 3.7 should work, but not guaranteed.
    ver_info = sys.version_info
    list_ver = f"\t Minimum: 3.7\n" \
               f"\t Recommended: 3.9.6\n" \
               f"\t You have: {platform.python_version()}"
    if ver_info[0] == 3:
        if ver_info[1] >= 9:
            pass
        elif ver_info[1] >= 7:
            # this import line has not been tested
            exec('from __future__ import annotations')
        else:
            raise OSError(f"Incompatible python interpreter.\n{list_ver}")
    else:
        print(f"This script is for Python 3, and may not be compatible with your python instance.\n{list_ver}")

    # site-package import
    import tifffile
    from numpy import array


def assertion_type_check(name: str, subject: object, expected_type: type) -> typing.Any:
    """
    returns the subject as is after passing the check
    :param name: parameter name (maybe), to display error message
    :param subject: subject you wanna check
    :param expected_type: type you wanna check against
    :return: the subject
    """
    assert isinstance(subject, expected_type), f"{name} expects {expected_type}, got {type(subject)}"
    return subject


# headers data which are NOT included in TIFF
class MicapsType4AdditionalHeader:
    def __init__(self, title: str, date: datetime, effective_time: timedelta,
                 level: float = 999.0, contour_interval: int = 0, contour_initial_value: int = 0,
                 contour_termination_value: int = 0, smoothing_factor: int = 1, bold_line_value: int = 0) -> None:
        """
        headers data which are NOT included in TIFF itself, hence need to be added manually
        *1 There are values that I have no idea of what it means. Change as you wish.
        :param title: 'diamond 4' will be added if not present at the beginning of the string
        :param date: start date of your dataset
        :param effective_time: effect time of your dataset
        :param level: *1
        :param contour_interval: *1
        :param contour_initial_value: *1
        :param contour_termination_value: *1
        :param smoothing_factor: *1
        :param bold_line_value: *1
        """
        assertion_type_check("title", title, str)
        if not title.startswith('diamond 4 '):
            title = f"diamond 4 {title}"
        self.title: str = title
        self.date: datetime = assertion_type_check("date", date, datetime)
        self.effective_time: timedelta = assertion_type_check("effective_time", effective_time, timedelta)
        self.level: float = assertion_type_check("level", level, float)
        # self.longitude_spacing: int
        # self.latitude_spacing: int
        self.contour_interval: int = assertion_type_check("contour_interval", contour_interval, int)
        self.contour_initial_value: int = assertion_type_check("contour_initial_value", contour_initial_value, int)
        self.contour_termination_value: int = \
            assertion_type_check("contour_termination_value", contour_termination_value, int)
        self.smoothing_factor: int = assertion_type_check("smoothing_factor", smoothing_factor, int)
        self.bold_line_value: int = assertion_type_check("bold_line_value", bold_line_value, int)

    def display(self) -> str:
        string = f"** Additional Header Data **\n" \
                 f"\t Title: {self.title}\n" \
                 f"\t Start date: {self.date.__str__()}\n" \
                 f"\t Effective time: {self.effective_time.total_seconds()} second(s)\n" \
                 f"\t Level: {self.level}\n" \
                 f"\t Contour interval: {self.contour_interval}\n" \
                 f"\t Contour initial value: {self.contour_initial_value}\n" \
                 f"\t Contour termination value: {self.contour_termination_value}\n" \
                 f"\t Smoothing factor: {self.smoothing_factor}\n" \
                 f"\t Bold line value: {self.bold_line_value}\n" \
                 f"** END **"
        return string

    # useless?
    def __str__(self) -> str:
        return f"{self.title}\n" \
               f"{self.date.year} {self.date.month} {self.date.day} {self.date.hour} " \
               f"{self.effective_time.total_seconds() / 3600}"


class Tif2MicapsType4:
    class MicapsType4GeoInfoHeader:
        def __init__(self, tiff_tags: tifffile.TiffTags, zonal_grid_points: int = None, through_grid_points: int = None,
                     longitude_grid_spacing: float = None, latitude_grid_spacing: float = None,
                     start_of_longitude: float = None, start_of_latitude: float = None):
            """
            According to the tif file I was given, the following is present in the file, and I, for convenience, will
            hardcode to read them. BTW, I would assume the tiff file is "north up", the same as the file I was given.
            Note: any parameter received after tiff_tage will overwrite tiff_tags's value. If all parameters are pres-
            ent, tiff_tags will be ignored.
            :param tiff_tags: a list of tags in form of dictionary, provided by geotiff image itself
            """

            # width, horizontal, the longitude axis
            if zonal_grid_points is None:
                zonal_grid_points = tiff_tags['ImageWidth'].value
            self.zonal_grid_points: int = zonal_grid_points

            # length, vertical, the latitude axis
            if through_grid_points is None:
                through_grid_points = tiff_tags['ImageLength'].value
            self.through_grid_points: int = through_grid_points

            # cell size (positive), pass it as is
            if longitude_grid_spacing is None:
                longitude_grid_spacing = tiff_tags['ModelPixelScaleTag'].value[0]
            self.longitude_grid_spacing: float = longitude_grid_spacing

            # cell size (negative), need to invert before passing
            if latitude_grid_spacing is None:
                latitude_grid_spacing = -tiff_tags['ModelPixelScaleTag'].value[1]
            self.latitude_grid_spacing: float = latitude_grid_spacing

            if start_of_longitude is None:
                start_of_longitude = tiff_tags['ModelTiepointTag'].value[3]
            self.start_of_longitude: float = start_of_longitude

            if start_of_latitude is None:
                start_of_latitude = tiff_tags['ModelTiepointTag'].value[4]
            self.start_of_latitude: float = start_of_latitude

            self.termination_of_longitude: float = \
                self.start_of_longitude + self.longitude_grid_spacing * (self.zonal_grid_points - 1)

            self.termination_of_latitude: float = \
                self.start_of_latitude + self.latitude_grid_spacing * (self.through_grid_points - 1)

    def __init__(self, data_file_path: str, data_header: MicapsType4AdditionalHeader,
                 geo_header: MicapsType4GeoInfoHeader = None) -> None:
        # pre-process tif file (read the data)
        pathlib_filepath = pathlib.Path(data_file_path)
        assert pathlib_filepath.is_file() and pathlib_filepath.exists(), f"[{pathlib_filepath.__str__()}] not exist"
        file_stream = tifffile.TiffFile(pathlib_filepath.__str__())
        # for my laziness, only read the first page of tif image. Rest will be ignored
        tags = file_stream.pages[0].tags
        data: array = file_stream.pages[0].asarray()
        assert len(data.shape) == 2, f"Expecting a 2d raster data, looks like it is {len(data.shape)}d"
        if geo_header is None:
            try:
                geo_header = Tif2MicapsType4.MicapsType4GeoInfoHeader(tags)
            except Exception:
                raise ValueError("Cannot parse geo info from tiff file. "
                                 "Consider passing these information in geo_header. ")

        # initializing variable
        # it contains header info required by type 4 data but not presented in tiff
        self.data_header: MicapsType4AdditionalHeader = data_header
        # it only contains header info required by type 4 data
        self.tif_header: Tif2MicapsType4.MicapsType4GeoInfoHeader = geo_header
        # it contains the raster data from tif image
        self.tif_data: array = data

    def get_header(self, no_new_line_in_the_end: bool = False, verbose: bool = True) -> str:
        # constructing Micaps data type 4 header. Variables are named according to Micaps3.1 user manual.
        string = f"{self.data_header.title}\n" \
                 f"{self.data_header.date.year} {self.data_header.date.month} {self.data_header.date.day} " \
                 f"{self.data_header.date.hour} {self.data_header.effective_time.total_seconds() / 3600} " \
                 f"{self.data_header.level} {self.tif_header.longitude_grid_spacing} " \
                 f"{self.tif_header.latitude_grid_spacing} {self.tif_header.start_of_longitude} " \
                 f"{self.tif_header.termination_of_longitude} {self.tif_header.start_of_latitude} " \
                 f"{self.tif_header.termination_of_latitude} {self.tif_header.zonal_grid_points} " \
                 f"{self.tif_header.through_grid_points} {self.data_header.contour_interval} " \
                 f"{self.data_header.contour_initial_value} {self.data_header.contour_termination_value} " \
                 f"{self.data_header.smoothing_factor} {self.data_header.bold_line_value}"
        if verbose:
            print(f"Header: {string}")
        if not no_new_line_in_the_end:
            string += '\n'
            # pass
        return string

    def get_data(self, verbose: bool = True) -> str:
        string = ''
        if verbose:
            size = self.tif_header.through_grid_points * self.tif_header.zonal_grid_points
            count = 0
        for rows in self.tif_data:
            for row_item in rows:
                content = str(round(row_item, ndigits=2))
                ndigit = len(content.split('.')[1])
                if ndigit == 1:
                    content += '0'
                string += f"{content} "
                if verbose:
                    count += 1
                    if count % 50000 == 0:
                        print(f"\rReading data ({count}/{size})", end='')
            string += '\n'
        if verbose:
            print(f"\rRead completed: {size} in total")
        return string

    def get_micaps_data_type_4_string(self, verbose: bool = True) -> str:
        return self.get_header(no_new_line_in_the_end=False, verbose=verbose) + self.get_data(verbose=verbose)

    def save_text_as_micaps_data_type_4(self, filepath: pathlib.Path, overwrite_if_exist: bool = False,
                                        verbose: bool = True) -> bool:
        if filepath.exists():
            if not overwrite_if_exist:
                print("ERROR: A file with the same name exist, unable to continue\n"
                      "Consider deleting the file or enable 'overwrite_if_exist'")
                return False
        if not filepath.parent.exists():
            if verbose:
                print(f"INFO creating directory {filepath.parent.__str__()}")
            os.makedirs(filepath.parent.__str__())
        if not filepath.name.endswith('.txt'):
            filepath.name += '.txt'
            if verbose:
                print(f"INFO renaming output filename to {filepath.name}")
        filepath.write_text(
            data=self.get_micaps_data_type_4_string(verbose)
        )
        if verbose:
            print(f"File {filepath.__str__()} saved")
        return True
