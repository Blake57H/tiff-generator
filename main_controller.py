from weather_0_2_4 import main_process
from ftp_file_fetcher_0_1_0 import get_weather_data

if __name__ == "__main__":
    get_weather_data()
    main_process()
    exit(0)

print("main_controller.py, version 1.0, have a nice day")
