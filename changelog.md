# change log (time in this file are UTC+8 unless specified)

## 2021-09-30 18:10 - major ftp downloader update

* tools in thunder/weather imports are updated to the latest version

* upgraded ftp downloader (0.2.3) - ditched the old download method and implemented 'list' command along side of 'mlsd'. The only one thing is 'list' may not provide an accuate modify time as 'mlsd' does, explictly for files/folders older than 1 year. 60 seconds timeout is inherited from v0.2.2 btw.

* added return value for weather/thunder data process function (0=done, 1=already done before)

* added timeout to ftp downloader (0.2.2), default 60 seconds

## 2021-09-15 17:42:32 - minor update

* ftp file fetcher 0.2.2: fixed return value in old_way

* utiltity 1.3:

 1. auto add `.tif` extension when saving tif image
 1. added a `ten_second_download` to cooldown

## 2021-09-08 13:56:44 - a push before someone submit a "author right" thing

* added author info in weather_0.3.2 and utility_0.1.3

* weather verify: working on wind direction data extraction

* ftp download: uses `feat` command to check server `mlsd` support instead of a try-catch block

## 2021-08-30 13:29:36 - start writing changelog

* weather/landslide/thunder/icy fully working (no issue most of the time)

* Ftp download supports `mlsd` so as to check existing file faster (but it seems that the f**king server does not support it).

* weather verify in beta (i donno what they want next, thus making a half working version with limited functionalities)
