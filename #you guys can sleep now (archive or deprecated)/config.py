class Directory:
    output_directory = "./beta-output/"
    input_directory = "./source data/"
    reference_directory = "./reference"  # background image should be placed here


class ProcessType:
    find_max_value = 0
    calculate_sum = 1


class ColorDataType:
    rain = 1
    temperature = 2
    wind_speed = 3


class NCFileFieldTag:
    rain = "PRE_1h"
