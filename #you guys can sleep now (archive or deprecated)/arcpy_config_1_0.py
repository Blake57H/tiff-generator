import os


class Directories:
    def get_absolute_working_directory(self):
        """
        an absolute directory for the pain of arcpy in python 2.7

        for no fucking reason running arcpy get os.curdir change to the [.mxd]'s place
        so hopefully this may ease the pain for putting .mxd file in a sub folder in working directory
        a must before executing any arcpy code, or U get the path where mxd file is stored
        better make it your first line of code, u no want problem (you don't want to trouble yourself, right?)
        either put every path absolute on every code when arcpy is involved, or put .mxd on root directory
        i think i will choose the former one
        yes, fuck arcpy, screw that shit
        :return: an absolute directory where your working directory is supposed to be
        """
        return os.path.abspath(os.curdir)


class Helps:
    mxd_to_image_description = "Export image with given shape file as data while using a mxd file as template\n" \
                               "Note that all paths/directories used as input should be absolute path"
    shp_input = "Shape file for data display (file used in template)"
    img_output = "Directory that exported image file should be in"
    mxd_file = "the template .mxd file"
    replace_index = "zero based index of layer in .mxd file that should be replaced with shp input"
    cell_size_value = "length of cell size in decimal degree"
    cell_size_reference = "use cell size from another raster data as reference"
    resample_method = "arcgis resampling type, must be one of these: NEAREST, BILINEAR, CUBIC, MAJORITY\n" \
                      "default is BILINEAR, for more information " \
                      "refer https://desktop.arcgis.com/en/arcmap/10.3/tools/data-management-toolbox/resample.htm"
    extract_by_mask_mask = "Input mask data defining areas to extract. It can be a raster or feature dataset."
    extract_by_mask_env_mask  = "A reference mask used in extent, snap raster raster analysis mask in environment " \
                                "settings. It can be the same as input mask."
