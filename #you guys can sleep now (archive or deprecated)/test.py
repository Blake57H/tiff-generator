import os.path
import subprocess

import sys
from datetime import datetime, timedelta

import numpy as np
import netCDF4 as nC
from PIL import Image

import config_1_1_2
import ftp_file_fetcher_0_2_0
import utility_0_1_1beta
import weather_0_3_0BETA
from sleeping import colorBETA
import glob

import statisticsBETA
from statisticsBETA import ValueWithGeoPosition, Statistics
import docx_replace_tool_0_1_2 as drt
from docx_replace_tool_0_1_2 import unzip_template, zip_directory, DocxKeyWords, date_formats, DefaultDirectories
from config_1_1_3beta import Directory, Name, CannotClassify, Paths
import json

import ftplib

print(utility_0_1_1beta.get_file_path_name_extension("temp/202107310800_rain_0h-24h.tif"))
exit(0)

arcpy_path = config_1_1_2.Paths.arcgis_python_installation
ebm = Paths.arc_tool_extract_by_mask
input_file = r"D:\Download\Dtaa\School Projects\weather idk\Programming\weather\temp\202107310800_rain_0h-24h_resampled.tif"
output_file = r"D:\Download\Dtaa\School Projects\weather idk\Programming\weather\temp\202107310800_rain_0h-24h_resampled_extracted.tif"
ref = r"D:\Download\Dtaa\School Projects\weather idk\Programming\weather\temp\inref.tif"
ref1 = 0.005
run = f"\"{arcpy_path}\" " \
      f"\"{ebm}\" " \
      f"-input \"{input_file}\" " \
      f"-output \"{output_file}\" " \
      f"-mask \"{ref}\" " \
      f"-ref \"{ref}\" "
print(run)
with subprocess.Popen(run) as proc:
    if proc.communicate()[0] is not None:
        print(proc.communicate()[0], flush=True)
    if proc.communicate()[1] is not None:
        print(proc.communicate()[1], flush=True)
    if proc.returncode != 0:
        print("error test")
    elif proc.returncode == 0:
        print("run success")
exit(0)


arcpy_path = config_1_1_2.Paths.arcgis_python_installation
resample = Paths.arc_tool_resample
input_file = r"D:\Download\Dtaa\School Projects\weather idk\Programming\weather\temp\202107310800_rain_0h-24h.tif"
output_file = r"D:\Download\Dtaa\School Projects\weather idk\Programming\weather\temp\202107310800_rain_0h-24h_resampled.tif"
ref = r"D:\Download\Dtaa\School Projects\weather idk\Programming\weather\temp\inref.tif"
ref1 = 0.005
run = f"\"{arcpy_path}\" " \
      f"\"{resample}\" " \
      f"-input \"{input_file}\" " \
      f"-output \"{output_file}\" " \
      f"-size \"{ref1}\" "
print(run)
with subprocess.Popen(run) as proc:
    if proc.communicate()[0] is not None:
        print(proc.communicate()[0], flush=True)
    if proc.communicate()[1] is not None:
        print(proc.communicate()[1], flush=True)
    if proc.returncode != 0:
        print("error test")
    elif proc.returncode == 0:
        print("run success")
exit(0)


ftp = ftplib.FTP()
ftp.connect('localhost', 21)
ftp.login('work', 'work')
print(ftp.welcome)
print(ftp.nlst())
exit(0)

arcpy_path = Paths.arcgis_python_installation
mxd2png = Paths.arc_tool_mxd_to_png
shps = glob.glob(os.path.join(Directory.temporary_directory, 'shp', '*.shp'))
abs_mxd = os.path.abspath(Directory.thunder_mxd_template_file_path)
mxd2png_test = r'D:\Download\Dtaa\School Projects\weather idk\Programming\_theirs\thunder to report with arcpy\test4.py'
shp_dir = r'D:\Download\Dtaa\School Projects\weather idk\Programming\_theirs\thunder to report with arcpy\a\shp\JT7J2UKR_lightning_yb_utc8_202107280845.shp'
png_dir = r'D:\Download\Dtaa\School Projects\weather idk\Programming\_theirs\thunder to report with arcpy\a\png\fu.png'
mxd_dir = r'D:\Download\Dtaa\School Projects\weather idk\Programming\_theirs\thunder to report with arcpy\a\thunder.mxd'
shp_dir = r'D:\Download\Dtaa\School Projects\weather idk\Programming\_theirs\thunder to report with arcpy\a\shp\JT7J2UKR_lightning_yb_utc8_202107280845.shp'
png_dir = r'D:\Download\Dtaa\School Projects\weather idk\Programming\weather\beta-output\fu.png'
mxd_dir = r'D:\Download\Dtaa\School Projects\weather idk\Programming\weather\reference\mxds\thunder\thunder.mxd'
# run = f'{arcpy_path} \"{test_subject}\"'
# run = f'{arcpy_path} \"D:/Download/Dtaa/School Projects/weather idk/Programming/_theirs/thunder to report with arcpy/test4.py\"'
# run = f'\"{arcpy_path}\" \"{mxd2png_test}\" -input \"{shp_dir}\" -output \"{png_dir}\" -template \"{mxd_dir}\" -index {1}'
run = f"\"{arcpy_path}\" " \
      f"\"{mxd2png}\" " \
      f"-input \"{shp_dir}\" " \
      f"-output \"{png_dir}\" " \
      f"-template \"{mxd_dir}\" " \
      f"-index {1}"
print(run)
with subprocess.Popen(run) as proc:
    if proc.communicate()[0] is not None:
        print(proc.communicate()[0], flush=True)
    if proc.communicate()[1] is not None:
        print(proc.communicate()[1], flush=True)
    if proc.returncode != 0:
        print("error test")
    elif proc.returncode == 0:
        print("run success")
exit(0)

for item in shps:
    abs_shp = os.path.abspath(item)
    abs_png = os.path.abspath(os.path.join(Directory.output_directory, 'a.png'))
    run = f'{arcpy_path} \"{mxd2png}\" -input \"{abs_shp}\" -output \"{abs_png}\" -template \"{abs_mxd}\" -index {1}'
    print(f"executing {run}")
    # print(subprocess.check_output(run), flush=True)
    # subprocess.Popen(run)
    subprocess.run(run, capture_output=True, encoding='utf8')


d = os.path.join(d, config_1_1_2.Directory.input_directory)
print(d)
exit(0)

ftp_file_fetcher_0_2_0.get_thunder_data()
exit(0)



def chdir(path):
    path+="/lll"
    print(path)

dire = r'./temp\a\b/c'
chdir(dire)
print(dire)
remote_path = dire.split('/')[-1:][0].split('\\')[-1:][0]
print(remote_path)
a = 5
print(a)
a.__add__(4)
print(a)
exit(0)

dire = glob.glob(os.path.join(config_1_1_2.Directory.input_directory, "*"))
print(glob.glob(config_1_1_2.Directory.input_directory))
print(dire)
dire = os.listdir(config_1_1_2.Directory.input_directory)
print(dire)
exit(0)

nc_data_path = glob.glob(os.path.join(Directory.input_directory, Name.weather_data_file))
now = datetime.now()
strTime = nc_data_path[0].split("_")[-2:-1][0]
print(strTime)
date = datetime.strptime(strTime, CannotClassify.weather_filename_start_time_format)
print(date.strftime("%Y%m%d%H%M"))
print(f"y={date.year},m={date.month},d={date.day}")
exit(0)

str1 = 'nana{day0}hah'
print(str1.replace("{day0}", "77"))
str1 = str1.replace("{day0}", "77")
print(str1)
today = datetime.now()
for days in range(0, 4):
    date = today + timedelta(days=days)
    date_str = date_formats.format(date.month, date.day)
    date_keyword = DocxKeyWords.day_x.replace(DocxKeyWords.day_x_keyword, str(days))
    print(date_keyword)
    with open("beta-output/document.xml", mode='r', encoding='utf8') as docXml:
        doc_xml_str = docXml.read()
    doc_xml_str = doc_xml_str.replace(date_keyword, date_str)
    print(doc_xml_str)
    # print(doc_xml_str)
    with open("beta-output/document.xml", mode='w', encoding='utf8') as docXml:
        docXml.write(doc_xml_str)
exit(0)

now = datetime.now()
today = now
for days in range(0, 4):
    date = today + timedelta(days=days)
    date_str = date_formats.format(date.month, date.day)
    print(date_str)
    date_keyword = DocxKeyWords.day_x.replace(DocxKeyWords.day_x_keyword, str(days))
    print(date_keyword)
exit(0)

a = "one "
b = None
e = ""
c = "and"
d = " two"
print(f"{a}{b if b is not None else e}{c}{d}")
exit(0)

a = np.arange(9)
print(a)
b = np.sum(a)
print(b)
c = np.divide(a, b)
print(c)
exit(0)

unzip_template(Directory.temporary_directory, os.path.join(Directory.reference_directory, "docLockedTemplate.docx"))
zip_directory(Directory.temporary_directory, Directory.output_directory, "1.docx")
config_1_1_2.clean_temporary_folder()
exit(0)

obj = ValueWithGeoPosition(1, 10, 10)
data = {}
data['key'] = obj.to_dict()
data['avg'] =16
data['arr'] = ['12', '23', '45']
data['arr1'] = [['12', 2, 3], ['112', 2, 3], ['122', 2, 3]]
print(data)
json_data = json.dumps(data)
print(json_data)
with open("./beta-output/thing.json", mode='w') as fileout:
    print("file created")
    fileout.write(json_data)
print([ValueWithGeoPosition(1,2,3).to_dict(),ValueWithGeoPosition(3,4,5).to_dict()])
exit(0)

print(f"{type(obj)}, {type(statisticsBETA.ValueWithGeoPosition.__class__)}")
print(isinstance(obj, ValueWithGeoPosition))
stat = Statistics()
stat.add_data(obj)
exit(0)

input_file = "./reference/Screenshot 2021-07-13 140756.png"
im = Image.open(input_file)
pix = im.load()
x, y = im.size
print(im)
print(im.size[1])
print(f"{x}x{y}")  # Get the width and hight of the image for iterating over
print(pix[2, 2][0:3])  # Get the RGBA Value of the a pixel of an image
pix[2, 2] = colorBETA.REPLACE_TARGET  # Set the RGBA Value of the image (tuple)
print(pix[2, 2])
print(pix[50, 30][0])
im.save('./beta-output/alive_parrot.png')  # Save the modified pixels as .png
exit(0)

a = np.arange(20)
a = a.reshape((4, 5))
print(a)
b = np.arange(2)
b = b.reshape((1, 2))
print(b)
a[-2:-1, -3:-1] = b
print(a)
exit(0)

Input_folder = r"./source data"
nc_files = glob.glob(Input_folder + "/*.nc")
nc_data_obj = nC.Dataset(nc_files[0])
print(nc_data_obj.variables["PRE_1h"][0, 0, 0])
print("------------------")
exit(0)


def add(d):
    d += 10
    return d


a = 10
print(f"{a}, {add(a)}")
for item in range(10, 20):
    print(item, end=", ")
r = g = b = 255
print()
print((r, g, b) == colorBETA.WHITE)
a, b, c = colorBETA.BLACK
print(f"{a}, {b}, {c}")
print("------------------")

print(np.where([[True, False], [True, True]],
               [[1, 2], [3, 4]],
               [[9, 8], [7, 6]]))
c = (155, 155, 155)
a = (1, 4, 6)
print(a[0])
print(a[1])
print(a[2])
a = np.arange(20)
print(a)
print(a[15:20])
print(a[15:50])
a = a.reshape((4, 5))
print(a)
print("------------------")
print(a.shape)
print(len(a.shape))
