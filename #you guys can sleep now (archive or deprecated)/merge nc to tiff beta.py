import os.path
import datetime

import numpy as np
import netCDF4 as nC
from osgeo import gdal
import glob

import colorBETA
import config
from colorBETA import Rain, Temperature, WindSpeed

"""
This python script is developed using python version 3.8.6 64bit
"""

DEBUG = False
DEBUG1 = False


def get_timestamp_filename(time):
    utc_timestamp_sec = (time * 3600) + 631152000  # 631152000 seconds between Jan 1 1990 and Jan 1 1970
    date = datetime.datetime.utcfromtimestamp(utc_timestamp_sec)
    return date.strftime("%Y%m%d%H0000")


def check_file_existence(path):
    return os.path.exists(path)


def lonlat_to_index(x1, x2, step):
    return round((x1 - x2) / step, ndigits=None)


def lonlat_to_index_v2(reference, target, step):
    result = (target - reference) / step
    result = round(result, ndigits=None)
    if result < 0:
        print(f"ERROR there could be something wrong: {result}=round(({target}-{reference})/{step})<0")
    return result


def index_to_lonlat_v2(index, reference, step):
    """
    Convert data's geo longitude/latitude value based on given index
    :param index: input index
    :param reference: array's upper-left element's corresponding longitude/latitude
    :param step: must be non-negative number, will automatically convert if is negative
    :return:
    """
    result = reference + index * step
    if result <= 0:
        print(f"ERROR there could be something wrong: {result}={reference}+{index}*{step})<0")
        print(
            "INFO If you live in a place where longitude/latitude is a negative value, "
            "delete this statement to get rid of this message. I live in the east so "
            "negative value does not make sense to me.")
    return result


def index_to_lonlat(index, reference, step, is_addition):
    """
    Convert tif image's array index to geo longitude/latitude value
    :param index: input index
    :param reference: array's upper-left element's corresponding longitude/latitude
    :param step: must be non-negative number, will automatically convert if is negative
    :param is_addition: is step negative (False) of positive (True)?
    :return:
    """
    if is_addition is None:
        is_addition = step > 0
    step = abs(step)
    if is_addition is None or is_addition is True:
        return reference + index * step
    else:
        return reference - index * step


def save_tif(rgb_array, path_s, geo_transform_s, geo_projection_s, filename_s):
    """
    save 2d matrix to tif
    :param geo_projection_s: geo projection
    :param geo_transform_s: get transform
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param rgb_array: rgb array, must be [r_matrix, g_matrix, b_matrix] or U R MESSING WITH ME
    """
    if len(rgb_array) != 3:
        print("U R MESSING WITH ME")
        os.system("pause")
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    save_filename = os.path.join(path_s, filename_s)
    image_height = rgb_array[0].shape[0]
    image_width = rgb_array[0].shape[1]
    print(f"Saving image {save_filename}: size = {image_width}x{image_height}")
    out_dataset = geo_tiff_driver.Create(save_filename, image_width, image_height, 3, gdal.GDT_Byte)
    out_dataset.SetGeoTransform(geo_transform_s)
    out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(rgb_array[0])
    out_dataset.GetRasterBand(2).WriteArray(rgb_array[1])
    out_dataset.GetRasterBand(3).WriteArray(rgb_array[2])
    out_dataset.FlushCache()


def source_index_to_target_index(src_index, src_ref_lonlat, src_step, src_is_add, target_ref_lonlat, target_step,
                                 target_is_step_increase):
    src_lonlat_offset = src_step * src_index
    # src_lonlat = np.where(src_is_add is True, src_ref_lonlat + src_lonlat_offset, src_ref_lonlat - src_lonlat_offset)
    src_lonlat = src_ref_lonlat + src_lonlat_offset
    return np.where(target_is_step_increase is True,
                    lonlat_to_index(src_lonlat, target_ref_lonlat, target_step),
                    lonlat_to_index(target_ref_lonlat, src_lonlat, target_step))


def determine_color(data_type, value):
    """
    check a given pixel's color by it's value. return white (255, 255, 255) if data_type does not match
    :param data_type: 1 = rain, 2 = temperature, 3 = wind speed, and nothing else (refer ColorDataType in config.py)
    :param value: value to check color ramp
    :return: RGB value (R, G, B)
    """
    if data_type == config.ColorDataType.rain:
        break_values = Rain.Split
        color_ramp = Rain.Color
    elif data_type == config.ColorDataType.temperature:
        break_values = Temperature.Split
        color_ramp = Temperature.Color
    elif data_type == config.ColorDataType.wind_speed:
        break_values = WindSpeed.Split
        color_ramp = WindSpeed.Color
    else:
        return colorBETA.WHITE  # return white if data_type mismatch
    break_point = 0
    break_values_len = len(break_values)
    while break_point <= break_values_len:
        if value < break_values[break_point]:
            return color_ramp[break_point]  # return color_ramp if value match
        break_point += 1
        if break_point == break_values_len:
            return color_ramp[break_point]  # return last color_ramp if value is larger than the biggest
    print("break_point loop error")
    return colorBETA.WHITE


def get_tif_value_by_lon_lat(data_array_2d, x_target, y_target, x_step, y_step, x_left_extent, y_bottom_extent):
    """
    get value from 2d array by x and y position
    :param data_array_2d: data
    :param x_target: longitude of the target point
    :param y_target: latitude of the target point
    :param x_step: step of x
    :param y_step: step of y
    :param x_left_extent: x_left_extent
    :param y_bottom_extent: y_bottom_extent
    :return: a certain value in data_array_2d, or None if exception occurred
    """
    np_data_array_2d = np.array(data_array_2d)
    if len(np_data_array_2d.shape) != 2:
        print(f"ERROR expect data array has 2 dimensions but it actually has {len(np_data_array_2d.shape)}")
        return None
    y_size, x_size = np_data_array_2d.shape
    if x_target < x_left_extent or x_target > x_left_extent + x_step * x_size:
        print(f"ERROR x target out of extent, {x_left_extent}<{x_target}<{x_left_extent + x_step * x_size}")
        return None
    if y_target < y_bottom_extent or y_target > y_bottom_extent + y_step * y_size:
        print(f"ERROR y target out of extent, {y_bottom_extent}<{y_target}<{y_bottom_extent + y_step * y_size}")
        return None
    x_index = round((x_target - x_left_extent) / x_step, None)
    y_index = round((y_target - y_bottom_extent) / y_step, None)
    return data_array_2d[y_index, x_index]


def get_nc_value_by_lon_lat(nc_3d_data_array, lon_tag, lat_tag, lon_value, lat_value,
                            nc_lon_array, nc_lat_array, nc_lon_step, nc_lat_step,
                            field_tag, value_type, time_range):
    """

    :param nc_3d_data_array:
    :param lon_tag:
    :param lat_tag:
    :param lon_value:
    :param lat_value:
    :param nc_lon_array:
    :param nc_lat_array:
    :param nc_lon_step:
    :param nc_lat_step:
    :param field_tag:
    :param value_type:
    :param time_range:
    :return:
    """
    value = 0
    counter = time_range[0]
    target_lon_index = round((lon_value - nc_lon_array[0]) / nc_lon_step, ndigits=None)
    target_lat_index = round((lat_value - nc_lat_array[0]) / nc_lat_step, ndigits=None)
    if value_type == config.ProcessType.calculate_sum:
        while counter < time_range[1]:
            value = np.add(x1=value, x2=nc_3d_data_array[counter, target_lat_index, target_lon_index])
            counter += 1
        return value
    elif value_type == config.ProcessType.find_max_value:
        arr = np.array(nc_3d_data_array[time_range[0]:time_range[1], target_lat_index, target_lon_index])
        return arr.max()
    else:
        print(f"ERROR: invalid value process type: {value_type}")
        return None


def rain_process_v2(bg_r, bg_g, bg_b, bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                    data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                    data_lon_step, data_lat_step):
    current_time_index = 0
    start_time_value = get_timestamp_filename(data_time_array[0])
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]
    nc_lat_step = data_lat_step
    nc_lon_step = data_lon_step
    # tif file's current processing index -- smaller the lat index, bigger the lat value
    nc_lat_max, nc_lat_min = np.where(data_overlay_ldru[3] > data_overlay_ldru[1],
                                      (data_overlay_ldru[3], data_overlay_ldru[1]),
                                      (data_overlay_ldru[1], data_overlay_ldru[3]))
    lat_index_min = round((bg_extent_ldru[3] - nc_lat_max) / abs(bg_lat_step), ndigits=None)
    lat_index_min = np.where(lat_index_min < 0, 0, lat_index_min)
    lat_index_max = round((bg_extent_ldru[3] - nc_lat_min) / abs(bg_lat_step), ndigits=None)
    lat_index_max = np.where(lat_index_max > bg_lat_size, bg_lat_size, lat_index_max)
    nc_lon_max, nc_lon_min = np.where(data_overlay_ldru[2] > data_overlay_ldru[0],
                                      (data_overlay_ldru[2], data_overlay_ldru[0]),
                                      (data_overlay_ldru[0], data_overlay_ldru[2]))
    lon_index_min = round((nc_lon_min - bg_extent_ldru[0]) / bg_lon_step, ndigits=None)
    lon_index_min = np.where(lon_index_min < 0, 0, lon_index_min)
    lon_index_max = round((nc_lon_max - bg_extent_ldru[0]) / bg_lon_step, ndigits=None)
    lon_index_max = np.where(lon_index_max > bg_lon_size, bg_lon_size, lon_index_max)
    if DEBUG1:
        print(f"lat index min max = {lat_index_min}, {lat_index_max}")
        print(f"lon index min max = {lon_index_min}, {lon_index_max}")
    while current_time_index + data_time_index_step < len(data_time_array):
        rain_value = 0
        for lat_index in range(lat_index_min, lat_index_max):
            for lon_index in range(lon_index_min, lon_index_max):
                pixel_color = (bg_r[lat_index, lon_index], bg_g[lat_index, lon_index], bg_b[lat_index, lon_index])
                if pixel_color != colorBETA.REPLACE_TARGET:
                    continue
                if DEBUG1:
                    print("calculate color")
                lat = index_to_lonlat_v2(lat_index, bg_extent_ldru[3], bg_lat_step)
                lon = index_to_lonlat_v2(lon_index, bg_extent_ldru[0], bg_lon_step)
                nc_lat_index = lonlat_to_index_v2(data_overlay_ldru[3], lat, nc_lat_step)
                nc_lon_index = lonlat_to_index_v2(data_overlay_ldru[0], lon, nc_lon_step)
                for time in range(current_time_index, current_time_index + data_time_index_step):
                    rain_value += data_3d_array[time, nc_lat_index, nc_lon_index]
                bg_r[lat_index, lon_index], bg_g[lat_index, lon_index], bg_b[lat_index, lon_index] = determine_color(
                    config.ColorDataType.rain, rain_value)
                if DEBUG1:
                    print(f"value={rain_value}, calculated color={determine_color(config.ColorDataType.rain, rain_value)}")
                rain_value = 0
            if lat_index % 100 == 0:
                print(f"{lat_index - lat_index_min} of {lat_index_max - lat_index_min} rows completed")
        output_rgb_image_array.append((bg_r, bg_g, bg_b))
        current_time_index += data_time_index_step
        break
    print()
    remain = len(data_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignore remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def rain_process_old(bg_r, bg_g, bg_b, bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                     data_overlay_extent_ldru,
                     time_index_step, nc_data_3d_array, nc_time_array, nc_lon_array, nc_lat_array):
    current_time_index = 0
    start_time_value = get_timestamp_filename(nc_time_array[0])
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]
    nc_lat_step = (nc_lat_array[len(nc_lat_array) - 1] - nc_lat_array[0]) / (len(nc_lat_array) - 1)
    nc_lat_step_is_increase = nc_lat_step > 0
    nc_lat_step = np.abs(nc_lat_step)
    nc_lon_step = (nc_lon_array[len(nc_lon_array) - 1] - nc_lon_array[0]) / (len(nc_lon_array) - 1)
    nc_lon_step_is_increase = nc_lon_step > 0
    nc_lon_step = abs(nc_lon_step)
    # tif file's current processing index -- smaller the lat index, bigger the lat value
    lat_index_min = round((bg_extent_ldru[3] - data_overlay_extent_ldru[3]) / abs(bg_lat_step), ndigits=None)
    lat_index_min = np.where(lat_index_min < 0, 0, lat_index_min)
    lat_index_max = round((bg_extent_ldru[3] - data_overlay_extent_ldru[1]) / abs(bg_lat_step), ndigits=None)
    if DEBUG:
        print(f"lat index equation = round(({bg_extent_ldru[3]} - {data_overlay_extent_ldru[1]}) / {bg_lat_step})")
        print(f"lat index max after calculate = {lat_index_max}")
    lat_index_max = np.where(lat_index_max > bg_lat_size, bg_lat_size, lat_index_max)
    lon_index_min = round((data_overlay_extent_ldru[0] - bg_extent_ldru[0]) / bg_lon_step, ndigits=None)
    lon_index_min = np.where(lon_index_min < 0, 0, lon_index_min)
    lon_index_max = round((data_overlay_extent_ldru[2] - bg_extent_ldru[0]) / bg_lon_step, ndigits=None)
    lon_index_max = np.where(lon_index_max > bg_lon_size, bg_lon_size, lon_index_max)
    if DEBUG:
        print(f"bg ldru = {bg_extent_ldru}")
        print(f"data ldru = {data_overlay_extent_ldru}")
    while current_time_index + time_index_step < len(nc_time_array):
        if DEBUG:
            print(f"{current_time_index + time_index_step}<{len(nc_time_array)}")  # DEBUG
            print(f"lat index min max = {lat_index_min}, {lat_index_max}")
            print(f"lon index min max = {lon_index_min}, {lon_index_max}")
        rain_value = 0
        for lat_index in range(lat_index_min, lat_index_max):
            for lon_index in range(lon_index_min, lon_index_max):
                pixel_color = (bg_r[lat_index, lon_index], bg_g[lat_index, lon_index], bg_b[lat_index, lon_index])
                if pixel_color != colorBETA.REPLACE_TARGET:
                    continue
                nc_lat_index = source_index_to_target_index(src_index=lat_index, src_ref_lonlat=bg_extent_ldru[3],
                                                            src_step=bg_lat_step, src_is_add=False,
                                                            target_step=nc_lat_step, target_ref_lonlat=nc_lat_array[0],
                                                            target_is_step_increase=nc_lat_step_is_increase)
                nc_lon_index = source_index_to_target_index(src_index=lon_index, src_ref_lonlat=bg_extent_ldru[0],
                                                            src_step=bg_lon_step, src_is_add=True,
                                                            target_step=nc_lon_step, target_ref_lonlat=nc_lon_array[0],
                                                            target_is_step_increase=nc_lon_step_is_increase)
                lat = index_to_lonlat(lat_index, bg_extent_ldru[3], )
                for time in range(current_time_index, current_time_index + time_index_step):
                    rain_value += nc_data_3d_array[time, nc_lat_index, nc_lon_index]
                bg_r[lat_index, lon_index], bg_g[lat_index, lon_index], bg_b[lat_index, lon_index] = determine_color(
                    config.ColorDataType.rain, rain_value)
                rain_value = 0
            if lat_index % 100 == 0:
                print(f"{lat_index - lat_index_min} of {lat_index_max - lat_index_min} rows completed")
        output_rgb_image_array.append((bg_r, bg_b, bg_g))
        current_time_index += time_index_step
        break
    print()
    remain = len(nc_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignore remaining {remain} datasets for quantity less than {time_index_step}")
    return output_rgb_image_array


def main_process():
    precision = 2  # x digits after decimal
    output_directory = config.Directory.output_directory
    input_directory = config.Directory.input_directory
    reference_directory = config.Directory.reference_directory  # background image should be placed here
    input_file_extension = "*.tif"  # specify input data (things to overlay)
    background_files = "weather grey content.tif"  # background image file
    if not check_file_existence(output_directory):
        print("INFO output folder is being created")
        os.mkdir(output_directory)
    if not check_file_existence(input_directory):
        print("ERROR input folder not found")
        exit(1)
    if not check_file_existence(reference_directory):
        print("ERROR reference folder not found")
        exit(2)
    # read background image
    bg_dataset = gdal.Open(os.path.join(reference_directory, background_files))
    bg_geo_transform = bg_dataset.GetGeoTransform()
    bg_geo_projection = bg_dataset.GetProjection()
    bg_red = bg_dataset.GetRasterBand(1).ReadAsArray()
    bg_green = bg_dataset.GetRasterBand(2).ReadAsArray()
    bg_blue = bg_dataset.GetRasterBand(3).ReadAsArray()
    bg_width = bg_dataset.RasterXSize
    bg_height = bg_dataset.RasterYSize
    bg_minx = bg_geo_transform[0]
    bg_maxy = bg_geo_transform[3]
    bg_x_step = bg_geo_transform[1]
    bg_y_step = bg_geo_transform[5]
    bg_maxx = bg_minx + bg_x_step * bg_dataset.RasterXSize
    bg_miny = bg_maxy + bg_y_step * bg_dataset.RasterYSize
    bg_extent_ldru = [bg_minx, bg_miny, bg_maxx,
                      bg_maxy]  # background image extent, [left, down, right, up], correspond
    print(f"Background image size: {bg_width}x{bg_height}")
    print(f"Background image extent: left={bg_minx}, right={bg_maxx}, top={bg_maxy}, bottom={bg_miny}")
    input_files = glob.glob(os.path.join(input_directory, input_file_extension))
    nc_data_path = glob.glob(os.path.join(input_directory, "*.nc"))
    nc_dataset = nC.Dataset(nc_data_path[0])  # 读取nc数据集
    nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
    nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
    nc_time_array = np.array(nc_dataset.variables["time"][:])
    nc_lon_array_size = len(nc_lon_array)
    nc_lat_array_size = len(nc_lat_array)
    nc_time_step = 24
    nc_overlay_extent_ldru = [nc_lon_array.min(), nc_lat_array.min(), nc_lon_array.max(), nc_lat_array.max()]
    nc_lat_step = (nc_overlay_extent_ldru[3] - nc_overlay_extent_ldru[1]) / nc_lat_array_size
    nc_lon_step = (nc_overlay_extent_ldru[2] - nc_overlay_extent_ldru[0]) / nc_lon_array_size
    nc_overlay_array_ldru = [nc_lon_array[0], nc_lat_array[len(nc_lat_array) - 1],
                             nc_lon_array[len(nc_lon_array) - 1], nc_lat_array[0]]
    if DEBUG:
        print(f"bg ldru (in main_process) = {bg_extent_ldru}")
        print(f"data ldru (in main_process) = {nc_overlay_extent_ldru}")
        os.system("pause")
    images = rain_process_v2(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                             bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width, bg_lat_size=bg_height,
                             data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                             data_3d_array=nc_dataset.variables[config.NCFileFieldTag.rain][:],
                             data_lat_step=nc_lat_step, data_lon_step=nc_lon_step, data_time_array=nc_time_array)
    time_index = 0
    filename = get_timestamp_filename(nc_time_array[0])
    for image_rgb in images:
        filename_to_save = f"{filename}_rain_{time_index}-{time_index + nc_time_step}h.tif"
        save_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
        time_index += nc_time_step


if __name__ == "__main__":
    print("ready get set go")
    main_process()
    print("and stop")
