import datetime
import os
import zipfile

file_extension = ".docx"
date_formats = "{0}月{1}日"
publish_hour_offset = 2


class DocxKeyWords:
    image_day_x = "image_x.tif"
    rain_image_0 = "image2.tif"
    rain_image_1 = "image5.tif"
    rain_image_2 = "image8.tif"
    temperature_image_0 = "image3.tif"
    temperature_image_1 = "image6.tif"
    temperature_image_2 = "image9.tif"
    wind_speed_image_0 = "image4.tif"
    wind_speed_image_1 = "image7.tif"
    wind_speed_image_2 = "image10.tif"
    day0 = '{day0}'
    day1 = '{day1}'
    day2 = '{day2}'
    day_x = '{day_x}'
    data_hour = '{data_hh}'
    publish_hour = '{pub_hh}'
    day_x_keyword = '_x'
    day_x_rain = "{day_xrain}"
    day_x_rain_addition = '{day_xrain_add}'
    day0rain = '{day0rain}'
    day1rain = '{day1rain}'
    day2rain = '{day2rain}'
    day_x_temperature = '{day_xtemp}'
    day_x_temperature_addition = '{day_xtemp_add}'
    day0temperature = '{day0temp}'
    day1temperature = '{day1temp}'
    day2temperature = '{day2temp}'
    day_x_wind = '{day_xwind}'
    day_x_wind_addition = '{day_xwind_add}'
    day0wind = '{day0wind}'
    day1wind = '{day1wind}'
    day2wind = '{day2wind}'


class DefaultDirectories:
    image_path = "word/media"
    document_file_path = "word/document.xml"


def unzip_template(extract_path, file_path):
    with zipfile.ZipFile(file_path, 'r') as zip_ref:
        zip_ref.extractall(extract_path)


def zip_directory(folder_path, zip_path, zip_filename):
    with zipfile.ZipFile(os.path.join(zip_path, zip_filename), mode='w', compression=zipfile.ZIP_DEFLATED) as zipf:
        len_dir_path = len(folder_path)
        for root, _, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                zipf.write(file_path, file_path[len_dir_path:])


def replace_text_content(keyword, replace, extract_path):
    document_file = os.path.join(extract_path, DefaultDirectories.document_file_path)
    if os.path.exists(document_file):
        with open(document_file, mode='r', encoding='utf8') as docXml:
            doc_xml_str = docXml.read()
        doc_xml_str = doc_xml_str.replace(keyword, replace)
        with open(document_file, mode='w', encoding='utf8') as docXml:
            docXml.write(doc_xml_str)
    else:
        print("ERROR cannot find extracted word document")


def update_day(extract_path, given_date):
    if given_date is None or not isinstance(given_date, datetime.datetime):
        today = datetime.datetime.now()
    else:
        today = given_date
    pub_date = today + datetime.timedelta(hours=publish_hour_offset)
    replace_text_content(keyword=DocxKeyWords.data_hour, replace=str(today.hour), extract_path=extract_path)
    replace_text_content(keyword=DocxKeyWords.publish_hour,
                         replace=str(pub_date.hour),
                         extract_path=extract_path)
    for days in range(0, 4):
        date = today + datetime.timedelta(days=days)
        date_str = date_formats.format(date.month, date.day)
        date_keyword = DocxKeyWords.day_x.replace(DocxKeyWords.day_x_keyword, str(days))
        replace_text_content(keyword=date_keyword, replace=date_str, extract_path=extract_path)
