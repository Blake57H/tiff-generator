WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
REPLACE_TARGET = (130, 130, 130)
DEFINED_CONTENT = BLACK
BG_FOREGROUND = WHITE
DATA_BACKGROUND = WHITE


class Rain:
    """
    Contains break value (Split) and color value (Color) for rain legend
    """
    Color = [WHITE, (164, 255, 115), (56, 168, 0), (115, 178, 255), (0, 92, 230), (230, 0, 168), (168, 0, 6)]
    Split = [0.1, 10, 25, 50, 100, 250]


class Temperature:
    """
    Contains break value (Split) and color value (Color) for temperature legend
    """
    Color = [(164, 255, 115), (181, 247, 82), (205, 240, 48), (255, 170, 0), (207, 90, 0),
             (158, 32, 0), (255, 0, 195), (204, 0, 153), (156, 0, 111), (115, 0, 77)]
    Split = [15, 20, 25, 30, 35, 37, 39, 41, 43]


class WindSpeed:
    """
    Contains break value (Split) and color value (Color) for temperature legend
    """
    Color = [WHITE, (194, 255, 161), (125, 255, 69), (255, 234, 0), (255, 200, 0),
             (255, 170, 0), (255, 119, 0), (255, 60, 0), (224, 0, 0), (168, 0, 0), (115, 0, 0)]
    Split = [1.5, 3.3, 5.4, 7.9, 10.7, 13.8, 17.1, 18, 20.7, 24.4]
