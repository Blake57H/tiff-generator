import os.path
import datetime

import numpy as np
import netCDF4 as nC
from osgeo import gdal
from PIL import Image
import glob

import config_1_0
from config_1_0 import DataProperty
from config_1_0 import Switches

"""
This python script is developed using python version 3.8.6 64bit
"""


def get_timestamp_filename(time):
    """
    yes
    :param time: yes
    :return: %Y%m%d%H0000
    """
    utc_timestamp_sec = (time * 3600) + 631152000  # 631152000 seconds between Jan 1 1990 and Jan 1 1970
    date = datetime.datetime.utcfromtimestamp(utc_timestamp_sec)
    return date.strftime("%Y%m%d%H0000")


def give_warning_of_existed_file(path, no_print):
    """
    as it says
    :param no_print: set True to ignore print command and return existence as boolean
    :param path: yes
    :return: boolean
    """
    result = os.path.exists(path)
    if result:
        if os.path.isdir(path) and not no_print:
            print(f"ERROR [{path}] exists as a directory and is not a file")
        elif not no_print:
            print(f"WARNING File [{path}] exists. File will be over-written")
    return result


def give_warning_of_non_exist_directory(path):
    """
    as it says (if given path exists but is not a directory, it will print a message)
    :param path: yes
    :return: boolean
    """
    is_exist = os.path.exists(path)
    if is_exist and not os.path.isdir(path):
        print(f"ERROR [{path}] exists not as a directory")
    return is_exist


def lonlat_to_index_v2(reference, target, step):
    """
    calculate index by given index[0]'s longitude/latitude and data's geo resolution
    :param reference: index[0]'s longitude/latitude
    :param target: the longitude/latitude need to be convert to index
    :param step: longitude/latitude resolution?
    :return: index
    """
    result = (target - reference) / step
    result = round(result, ndigits=None)
    if result < 0:
        print(f"ERROR there could be something wrong: {result}=round(({target}-{reference})/{step})<0")
    return result


def index_to_lonlat_v2(index, reference, step):
    """
    Convert data's geo longitude/latitude value based on given index
    :param index: input index
    :param reference: array's upper-left element's corresponding longitude/latitude
    :param step: must be non-negative number, will automatically convert if is negative
    :return:
    """
    result = reference + index * step
    if result <= 0:
        print(f"ERROR there could be something wrong: {result}={reference}+{index}*{step})<0")
        print(
            "INFO If you expect your longitude/latitude to be a negative value, "
            "delete this statement to get rid of this message. I live in the east so "
            "negative value does not make sense to me.")
    return result


# for now it only map corresponding pixel by width/height index, no resample is applied
# resample feature will apply in the future
# todo looks like some bug here is making image out of position
# todo: data image is one pixel above its supposed position
# not using this until it is fixed
def resize_data_array(resize_width, resize_height, data_2d_array):
    print("...resizing", end="")
    data_height, data_width = data_2d_array.shape
    resize_2d_array = np.zeros((resize_height, resize_width))
    height_scale = data_height / resize_height
    width_scale = data_width / resize_width
    if Switches.INFO:
        print(f"\nwidth scale = {width_scale}, height scale = {height_scale}")
    for height_index in range(resize_height):
        # print(f"Resizing {height_index + 1} of {resize_height} rows")
        for width_index in range(resize_width):
            data_h_index = round(height_index * data_height / resize_height, ndigits=None)
            if data_h_index >= data_height:
                data_h_index = data_height - 1
            data_w_index = round(width_index * data_width / resize_width, ndigits=None)
            if data_w_index >= data_width:
                data_w_index = data_w_index - 1
            resize_2d_array[height_index, width_index] = data_2d_array[data_h_index, data_w_index]
    print("...done", end="")
    return resize_2d_array


def save_tif(data_array_2d_s, path_s, geo_transform_s, geo_projection_s, filename_s):
    """
    save 2d matrix to tif
    :param geo_projection_s: geo projection
    :param geo_transform_s: get transform
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param data_array_2d_s: data
    """
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    save_filename = f"{path_s}{filename_s}"
    # print(data_array_2d_s.shape)
    print(f"Saving image {save_filename}: size = {data_array_2d_s.shape[1]}x{data_array_2d_s.shape[0]}")
    out_dataset = geo_tiff_driver.Create(save_filename, data_array_2d_s.shape[1], data_array_2d_s.shape[0], 1,
                                         gdal.GDT_Float32)
    if geo_transform_s is not None and geo_projection_s is not None:
        out_dataset.SetGeoTransform(geo_transform_s)
        out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(data_array_2d_s)
    out_dataset.FlushCache()


def save_rgb_tif(rgb_array, path_s, geo_transform_s, geo_projection_s, filename_s):
    """
    save rgb bands to tif
    :param geo_projection_s: geo projection (can be None)
    :param geo_transform_s: get transform (can be None)
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param rgb_array: rgb array, must be [r_matrix, g_matrix, b_matrix] or U R MESSING WITH ME
    """
    if len(rgb_array) != 3:
        print(f"ERROR array length mismatch, expect 3, got {len(rgb_array)}")
        os.system("pause")
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    save_filename = os.path.join(path_s, filename_s)
    give_warning_of_existed_file(save_filename, False)
    image_height = rgb_array[0].shape[0]
    image_width = rgb_array[0].shape[1]
    print(f"Saving image {save_filename}: size = {image_width}x{image_height}")
    out_dataset = geo_tiff_driver.Create(save_filename, image_width, image_height, 3, gdal.GDT_Byte)
    if geo_transform_s is not None and geo_projection_s is not None:
        out_dataset.SetGeoTransform(geo_transform_s)
        out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(rgb_array[0])
    out_dataset.GetRasterBand(2).WriteArray(rgb_array[1])
    out_dataset.GetRasterBand(3).WriteArray(rgb_array[2])
    out_dataset.FlushCache()


# not sure if it will be useful
def source_index_to_target_index(src_index, src_ref_lonlat, src_step, target_ref_lonlat, target_step):
    """
    find target data array's y/x index by source data array's y/x index with upper left position's longitude/latitude
    :param src_index: source data's x/y index
    :param src_ref_lonlat: source data
    :param src_step:
    :param target_ref_lonlat:
    :param target_step:
    :return:
    """
    the_middle_thing = index_to_lonlat_v2(src_index, src_ref_lonlat, src_step)
    the_thing = lonlat_to_index_v2(target_ref_lonlat, the_middle_thing, target_step)
    return the_thing


def coloring_bg_image_old_school_way(bg_geo_extent_ldru, bg_union_index_ldru, bg_r, bg_g, bg_b, bg_lat_step,
                                     bg_lon_step, data_geo_extent_ldru, data_lat_step, data_lon_step, data_2d_array,
                                     color_break_points, color_ramp, legend_path, legend_offset):
    """
    coloring background image pixels (the way that I do nc to data tiffs)
    :param bg_geo_extent_ldru: background's geo longitude/latitude extent [left, down, right, up]
    :param bg_union_index_ldru: index extent that background shares with data image
    :param bg_r: background image's red band
    :param bg_g: ...green band
    :param bg_b: ...blue band
    :param bg_lat_step: background's latitude per one background image's pixel
    :param bg_lon_step: ...longtitude per that pixel
    :param data_geo_extent_ldru: data's geo longitude/latitude extent [left, down, right, up]
    :param data_lat_step: yes, the latitude size per data's height element
    :param data_lon_step: ...the width element
    :param data_2d_array: content of data
    :param color_break_points: refer to DataProperty.(SomeData).Split in config.py
    :param color_ramp: refer to DataProperty.(SomeData).Color in config.py
    :param legend_path: yes (it can be None though)
    :param legend_offset: position offset in pixel
    :return: colored rgb band: [r, g, b]
    """
    print("coloring image..", end="")
    lon_index_min, lat_index_max, lon_index_max, lat_index_min = bg_union_index_ldru
    out_bg_r = bg_r
    out_bg_g = bg_g
    out_bg_b = bg_b
    for lat_index in range(lat_index_min, lat_index_max):
        for lon_index in range(lon_index_min, lon_index_max):
            pixel_color = (
                out_bg_r[lat_index, lon_index], out_bg_g[lat_index, lon_index], out_bg_b[lat_index, lon_index])
            if pixel_color != config_1_0.DefaultColor.REPLACE_TARGET:
                continue
            lat = index_to_lonlat_v2(lat_index, bg_geo_extent_ldru[3], bg_lat_step)
            lon = index_to_lonlat_v2(lon_index, bg_geo_extent_ldru[0], bg_lon_step)
            nc_lat_index = lonlat_to_index_v2(data_geo_extent_ldru[3], lat, data_lat_step)
            nc_lon_index = lonlat_to_index_v2(data_geo_extent_ldru[0], lon, data_lon_step)
            break_point = 0
            rain_value = data_2d_array[nc_lat_index, nc_lon_index]
            break_values_len = len(color_break_points)
            while break_point <= break_values_len:
                if rain_value < color_break_points[break_point]:
                    out_bg_r[lat_index, lon_index], out_bg_g[lat_index, lon_index], out_bg_b[lat_index, lon_index] = \
                        color_ramp[
                            break_point]  # return color_ramp if value match
                    break
                break_point += 1
                if break_point == break_values_len:
                    out_bg_r[lat_index, lon_index], out_bg_g[lat_index, lon_index], out_bg_b[lat_index, lon_index] = \
                        color_ramp[
                            break_point]  # return last color_ramp if value is larger than the biggest
                    break
    y, x = bg_g.shape
    if legend_path is None or not give_warning_of_existed_file(legend_path, True):
        print(f"\n\tINFO: legend image not found {legend_path}...")
    elif os.path.isdir(legend_path):
        print(f"\n\tINFO: legend image path is a directory {legend_path}...")
    else:
        im = Image.open(legend_path)
        pix = im.load()
        for y_index in range(0, im.size[1]):
            for x_index in range(0, im.size[0]):
                # bg_x_index = x_index + legend_position_offset
                # bg_y_index = bg_height - im.size[1] - legend_position_offset
                bg_x_index = x_index
                bg_y_index = y - im.size[1] - legend_offset + y_index
                out_bg_r[bg_y_index, bg_x_index], out_bg_g[bg_y_index, bg_x_index], out_bg_b[bg_y_index, bg_x_index] = \
                    pix[x_index, y_index][0:3]
            # print(f"legend {y_index} pf {im.size[1]} completed")
    y = y - 1
    x = x - 1
    out_bg_r[y, x], out_bg_g[y, x], out_bg_b[y, x] = (35, 106, 158)  # easter egg
    return out_bg_r, out_bg_g, out_bg_b


def coloring_bg_image(focusing_index_extent_ldru, bg_r, bg_g, bg_b, data_content, data_color_break_points,
                      data_color_ramp,
                      legend_path, legend_position_offset, bg_height, bg_width):
    """
    coloring background image pixels
    :param focusing_index_extent_ldru: union part's index extent of background array and data array
    :param bg_r: background image's red band
    :param bg_g: ...green band
    :param bg_b: ...blue band
    :param data_content: data array (size must match with focusing_index_extent_ldru, direction must match with background)
    :param data_color_break_points: refer to config's break point
    :param data_color_ramp: refer to config's color ramp
    :param legend_path: yes
    :param legend_position_offset: not using rn
    :param bg_height: background image's height
    :param bg_width: ...width
    :return: colored rgb band: [r, g, b]
    """
    im = Image.open(legend_path)
    pix = im.load()
    last_color_index = len(data_color_break_points)
    for y_index in range(focusing_index_extent_ldru[3], focusing_index_extent_ldru[1]):
        for x_index in range(focusing_index_extent_ldru[0], focusing_index_extent_ldru[2]):
            if (bg_r[y_index, x_index], bg_g[y_index, x_index],
                bg_b[y_index, x_index]) != config_1_0.DefaultColor.REPLACE_TARGET:
                continue
            for color_index in range(last_color_index):
                if data_content[y_index - focusing_index_extent_ldru[3], x_index - focusing_index_extent_ldru[0]] < \
                        data_color_break_points[color_index]:
                    bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index] = \
                        data_color_ramp[color_index]
                    break
                if color_index + 1 == last_color_index:
                    bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index] = \
                        data_color_ramp[last_color_index]
    for y_index in range(0, im.size[1]):
        for x_index in range(0, im.size[0]):
            # bg_x_index = x_index + legend_position_offset
            # bg_y_index = bg_height - im.size[1] - legend_position_offset
            bg_x_index = x_index
            bg_y_index = y_index
            bg_r[bg_y_index, bg_x_index], bg_g[bg_y_index, bg_x_index], bg_b[bg_y_index, bg_x_index] = \
                pix[x_index, y_index][0:3]
        # print(f"legend {y_index} pf {im.size[1]} completed")
    return bg_r, bg_g, bg_b


def union_index_calculation(array1_geo_ldru, array2_geo_ldru, array1_lon_step, array1_lat_step, array2_lon_step,
                            array2_lat_step, array1_lon_size, array1_lat_size):
    """
    Calculate background array and data array's union part, and return corresponding index range of both array
    *ldru means [left, down, right, up]
    :param array1_geo_ldru: background array longitude and latitude extent
    :param array2_geo_ldru: data array longitude and latitude extent
    :param array1_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param array1_lat_step: size of latitude equivalent to one background image's width pixel (lon resolution?)
    :param array2_lon_step: size of longitude equivalent to one data image's width pixel (lon resolution?)
    :param array2_lat_step: size of latitude equivalent to one data image's width pixel (lon resolution?)
    :param array1_lon_size: background image's width
    :param array1_lat_size: background image's height
    :return: union part's index extent of both array, [[background's index ldru], [data's index ldru]]
    """
    # calculate array1's union index
    array2_lat_max, array2_lat_min = np.where(array2_geo_ldru[3] > array2_geo_ldru[1],
                                              (array2_geo_ldru[3], array2_geo_ldru[1]),
                                              (array2_geo_ldru[1], array2_geo_ldru[3]))
    array1_lat_index_min = round((array1_geo_ldru[3] - array2_lat_max) / abs(array1_lat_step), ndigits=None)
    array1_lat_index_min = np.where(array1_lat_index_min <= 0, 0, array1_lat_index_min - 1)
    array1_lat_index_max = round((array1_geo_ldru[3] - array2_lat_min) / abs(array1_lat_step), ndigits=None)
    array1_lat_index_max = np.where(array1_lat_index_max > array1_lat_size, array1_lat_size, array1_lat_index_max)
    array2_lon_max, array2_lon_min = np.where(array2_geo_ldru[2] > array2_geo_ldru[0],
                                              (array2_geo_ldru[2], array2_geo_ldru[0]),
                                              (array2_geo_ldru[0], array2_geo_ldru[2]))
    array1_lon_index_min = round((array2_lon_min - array1_geo_ldru[0]) / array1_lon_step, ndigits=None)
    array1_lon_index_min = np.where(array1_lon_index_min < 0, 0, array1_lon_index_min)
    array1_lon_index_max = round((array2_lon_max - array1_geo_ldru[0]) / array1_lon_step, ndigits=None)
    array1_lon_index_max = np.where(array1_lon_index_max > array1_lon_size, array1_lon_size, array1_lon_index_max)
    # array2's union index
    array2_lat_index_min = source_index_to_target_index(array1_lat_index_min, array1_geo_ldru[3], array1_lat_step,
                                                        array2_geo_ldru[3], array2_lat_step)
    array2_lat_index_max = source_index_to_target_index(array1_lat_index_max, array1_geo_ldru[3], array1_lat_step,
                                                        array2_geo_ldru[3], array2_lat_step)
    array2_lat_index_min, array2_lat_index_max = np.where(array2_lat_index_min < array2_lat_index_max,
                                                          (array2_lat_index_min, array2_lat_index_max),
                                                          (array2_lat_index_max, array2_lat_index_min))
    array2_lon_index_min = source_index_to_target_index(array1_lon_index_min, array1_geo_ldru[0], array1_lon_step,
                                                        array2_geo_ldru[0], array2_lon_step)
    array2_lon_index_max = source_index_to_target_index(array1_lon_index_max, array1_geo_ldru[0], array1_lon_step,
                                                        array2_geo_ldru[0], array2_lon_step)
    array2_lon_index_min, array2_lon_index_max = np.where(array2_lat_index_min < array2_lat_index_max,
                                                          (array2_lon_index_min, array2_lon_index_max),
                                                          (array2_lon_index_max, array2_lon_index_min))
    return [[array1_lon_index_min, array1_lat_index_max, array1_lon_index_max, array1_lat_index_min],
            [array2_lon_index_min, array2_lat_index_max, array2_lon_index_max, array2_lat_index_min]]


def rain_process_v2(bg_r, bg_g, bg_b,
                    bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                    data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                    data_lon_step, data_lat_step):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent RELATIVE TO it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    print("Processing rain")
    current_time_index = 0
    rain_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    bg_union_index_ldru, nc_union_index_ldru = union_index_calculation(array1_geo_ldru=bg_extent_ldru,
                                                                       array2_geo_ldru=data_overlay_ldru,
                                                                       array1_lon_step=bg_lon_step,
                                                                       array1_lat_step=bg_lat_step,
                                                                       array2_lon_step=data_lon_step,
                                                                       array2_lat_step=data_lat_step,
                                                                       array1_lon_size=bg_lon_size,
                                                                       array1_lat_size=bg_lat_size)
    while current_time_index + data_time_index_step < len(data_time_array):
        print(f"\r\tcalculating rain {current_time_index}h~{current_time_index + data_time_index_step}h..", end="")
        rain_array = np.zeros(rain_array_shape)
        for time in range(current_time_index, current_time_index + data_time_index_step):
            rain_array = np.add(rain_array, data_3d_array[time, :, :])

        out_bg_r, out_bg_g, out_bg_b = coloring_bg_image_old_school_way(bg_geo_extent_ldru=bg_extent_ldru,
                                                                        bg_union_index_ldru=bg_union_index_ldru,
                                                                        bg_r=np.array(bg_r), bg_g=np.array(bg_g),
                                                                        bg_b=np.array(bg_b),
                                                                        bg_lat_step=bg_lat_step,
                                                                        bg_lon_step=bg_lon_step,
                                                                        data_geo_extent_ldru=data_overlay_ldru,
                                                                        data_lat_step=data_lat_step,
                                                                        data_lon_step=data_lon_step,
                                                                        data_2d_array=rain_array,
                                                                        color_break_points=DataProperty.Rain.Split,
                                                                        color_ramp=DataProperty.Rain.Color,
                                                                        legend_path=DataProperty.Rain.LegendPath,
                                                                        legend_offset=5)
        output_rgb_image_array.append((out_bg_r, out_bg_g, out_bg_b))
        current_time_index += data_time_index_step
        print("done")
        # break
    print()
    remain = len(data_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignoring remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def temperature_process(bg_r, bg_g, bg_b,
                        bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                        data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                        data_lon_step, data_lat_step):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent RELATIVE TO it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    print("Processing temperature")
    current_time_index = 0
    temp_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    bg_union_index_ldru, nc_union_index_ldru = union_index_calculation(array1_geo_ldru=bg_extent_ldru,
                                                                       array2_geo_ldru=data_overlay_ldru,
                                                                       array1_lon_step=bg_lon_step,
                                                                       array1_lat_step=bg_lat_step,
                                                                       array2_lon_step=data_lon_step,
                                                                       array2_lat_step=data_lat_step,
                                                                       array1_lon_size=bg_lon_size,
                                                                       array1_lat_size=bg_lat_size)

    time_section = 0  # an hour offset indicating which part of data to extract
    while time_section + data_time_index_step < len(data_time_array):
        time_section_end = time_section + data_time_index_step
        temp_array = np.zeros(temp_array_shape)
        for iLon in range(temp_array_shape[1]):
            print(f"\r\t{time_section}h~{time_section_end}h, {iLon} of {temp_array_shape[1]} complete..", end="")
            for iLat in range(temp_array_shape[0]):
                temp_range = np.array(data_3d_array[time_section:time_section + data_time_index_step, iLat, iLon])
                temp_array[iLat, iLon] = temp_range.max()
            # print(f"{iLat+1} of {N_Lat}", end="\r")
        out_bg_r, out_bg_g, out_bg_b = coloring_bg_image_old_school_way(bg_geo_extent_ldru=bg_extent_ldru,
                                                                        bg_union_index_ldru=bg_union_index_ldru,
                                                                        bg_r=np.array(bg_r), bg_g=np.array(bg_g),
                                                                        bg_b=np.array(bg_b),
                                                                        bg_lat_step=bg_lat_step,
                                                                        bg_lon_step=bg_lon_step,
                                                                        data_geo_extent_ldru=data_overlay_ldru,
                                                                        data_lat_step=data_lat_step,
                                                                        data_lon_step=data_lon_step,
                                                                        data_2d_array=temp_array,
                                                                        color_break_points=DataProperty.Temperature.Split,
                                                                        color_ramp=DataProperty.Temperature.Color,
                                                                        legend_path=DataProperty.Temperature.LegendPath,
                                                                        legend_offset=5)
        output_rgb_image_array.append((out_bg_r, out_bg_g, out_bg_b))
        time_section += data_time_index_step
        print("done")
    remain = len(data_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignoring remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def windspeed_process(bg_r, bg_g, bg_b,
                      bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                      data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                      data_lon_step, data_lat_step):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent RELATIVE TO it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    print("Processing wind speed")
    current_time_index = 0
    data_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    bg_union_index_ldru, nc_union_index_ldru = union_index_calculation(array1_geo_ldru=bg_extent_ldru,
                                                                       array2_geo_ldru=data_overlay_ldru,
                                                                       array1_lon_step=bg_lon_step,
                                                                       array1_lat_step=bg_lat_step,
                                                                       array2_lon_step=data_lon_step,
                                                                       array2_lat_step=data_lat_step,
                                                                       array1_lon_size=bg_lon_size,
                                                                       array1_lat_size=bg_lat_size)

    time_section = 0  # an hour offset indicating which part of data to extract
    while time_section + data_time_index_step < len(data_time_array):
        time_section_end = time_section + data_time_index_step
        windspeed_array = np.zeros(data_array_shape)
        for iLon in range(data_array_shape[1]):
            print(f"\r\t{time_section}h~{time_section_end}h, {iLon} of {data_array_shape[1]} complete..", end="")
            for iLat in range(data_array_shape[0]):
                temp_range = np.array(data_3d_array[time_section:time_section + data_time_index_step, iLat, iLon])
                windspeed_array[iLat, iLon] = temp_range.max()
            # print(f"{iLat+1} of {N_Lat}", end="\r")
        out_bg_r, out_bg_g, out_bg_b = coloring_bg_image_old_school_way(bg_geo_extent_ldru=bg_extent_ldru,
                                                                        bg_union_index_ldru=bg_union_index_ldru,
                                                                        bg_r=np.array(bg_r), bg_g=np.array(bg_g),
                                                                        bg_b=np.array(bg_b),
                                                                        bg_lat_step=bg_lat_step,
                                                                        bg_lon_step=bg_lon_step,
                                                                        data_geo_extent_ldru=data_overlay_ldru,
                                                                        data_lat_step=data_lat_step,
                                                                        data_lon_step=data_lon_step,
                                                                        data_2d_array=windspeed_array,
                                                                        color_break_points=DataProperty.WindSpeed.Split,
                                                                        color_ramp=DataProperty.WindSpeed.Color,
                                                                        legend_path=DataProperty.WindSpeed.LegendPath,
                                                                        legend_offset=5)
        output_rgb_image_array.append((out_bg_r, out_bg_g, out_bg_b))
        time_section += data_time_index_step
        print("done")
    remain = len(data_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignoring remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def main_process():
    precision = 2  # x digits after decimal
    output_directory = config_1_0.Directory.output_directory
    input_directory = config_1_0.Directory.input_directory
    reference_directory = config_1_0.Directory.reference_directory  # background image should be placed here
    input_file_extension = "*.tif"  # specify input data (things to overlay)
    background_files = "weather grey content.tif"  # background image file
    if not give_warning_of_non_exist_directory(output_directory):
        print("INFO output folder is being created")
        os.mkdir(output_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print("ERROR input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print("ERROR reference folder not found")
        exit(2)
    # read background image
    bg_dataset = gdal.Open(os.path.join(reference_directory, background_files))
    bg_geo_transform = bg_dataset.GetGeoTransform()
    bg_geo_projection = bg_dataset.GetProjection()
    # test
    global peoj, tras
    peoj = bg_geo_projection
    tras = bg_geo_transform
    # end of test
    bg_red = bg_dataset.GetRasterBand(1).ReadAsArray()
    bg_green = bg_dataset.GetRasterBand(2).ReadAsArray()
    bg_blue = bg_dataset.GetRasterBand(3).ReadAsArray()
    bg_width = bg_dataset.RasterXSize
    bg_height = bg_dataset.RasterYSize
    bg_minx = bg_geo_transform[0]
    bg_maxy = bg_geo_transform[3]
    bg_x_step = bg_geo_transform[1]
    bg_y_step = bg_geo_transform[5]
    bg_maxx = bg_minx + bg_x_step * bg_dataset.RasterXSize
    bg_miny = bg_maxy + bg_y_step * bg_dataset.RasterYSize
    bg_extent_ldru = [bg_minx, bg_miny, bg_maxx,
                      bg_maxy]  # background image extent, [left, down, right, up], correspond
    print(f"Background image size: {bg_width}x{bg_height}")
    print(f"Background image extent: left={bg_minx}, right={bg_maxx}, top={bg_maxy}, bottom={bg_miny}")

    # NC input file section
    nc_data_path = glob.glob(os.path.join(input_directory, "*.nc"))
    for item in nc_data_path:
        nc_dataset = nC.Dataset(item)  # 读取nc数据集
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_time_array = np.array(nc_dataset.variables["time"][:])
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_time_step = 24
        nc_overlay_extent_ldru = [nc_lon_array.min(), nc_lat_array.min(), nc_lon_array.max(), nc_lat_array.max()]
        nc_lat_step = (nc_overlay_extent_ldru[3] - nc_overlay_extent_ldru[1]) / nc_lat_array_size
        nc_lon_step = (nc_overlay_extent_ldru[2] - nc_overlay_extent_ldru[0]) / nc_lon_array_size
        nc_overlay_array_ldru = [nc_lon_array[0], nc_lat_array[len(nc_lat_array) - 1],
                                 nc_lon_array[len(nc_lon_array) - 1], nc_lat_array[0]]
        # rain section
        images = rain_process_v2(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                 bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                 bg_lat_size=bg_height,
                                 data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                 data_3d_array=nc_dataset.variables[config_1_0.DataProperty.Rain.NCFieldTag][:],
                                 data_lat_step=nc_lat_step, data_lon_step=nc_lon_step, data_time_array=nc_time_array)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            filename_to_save = f"{filename}_rain_{time_index}-{time_index + nc_time_step}h.tif"
            save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step

        # temperature section
        images = temperature_process(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                     bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                     bg_lat_size=bg_height,
                                     data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                     data_3d_array=nc_dataset.variables[DataProperty.Temperature.NCFieldTag][:],
                                     data_lat_step=nc_lat_step, data_lon_step=nc_lon_step,
                                     data_time_array=nc_time_array)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            filename_to_save = f"{filename}_{DataProperty.Temperature.FriendlyName}_{time_index}-{time_index + nc_time_step}h.tif"
            save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step

        # wind speed section
        wind_speed_3d_array = nc_dataset.variables[DataProperty.WindSpeed.NCFieldTag][:, 0, :, :]
        images = windspeed_process(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                   bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                   bg_lat_size=bg_height,
                                   data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                   data_3d_array=wind_speed_3d_array,
                                   data_lat_step=nc_lat_step, data_lon_step=nc_lon_step, data_time_array=nc_time_array)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            filename_to_save = f"{filename}_{DataProperty.WindSpeed.FriendlyName}_{time_index}-{time_index + nc_time_step}h.tif"
            save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step
    if len(nc_data_path) == 0:
        print("INFO no input .nc file found")


if __name__ == "__main__":
    print("ready get set go")
    main_process()
    print("and stop")
