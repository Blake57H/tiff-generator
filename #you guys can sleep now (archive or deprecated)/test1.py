import datetime
import os
import sys

import config_1_1_2
import landslide_0_1_0
import thunder_0_1_0


a = 2
July31 = datetime.datetime.strptime("20210731", config_1_1_2.CannotClassify.remote_directory_date_format)
July30 = datetime.datetime.strptime("20210730", config_1_1_2.CannotClassify.remote_directory_date_format)
July28 = datetime.datetime.strptime("20210728", config_1_1_2.CannotClassify.remote_directory_date_format)

if a == 2:
    landslide_0_1_0.main_process(July31)
elif a == 1:
    thunder_0_1_0.main_process(July28)
