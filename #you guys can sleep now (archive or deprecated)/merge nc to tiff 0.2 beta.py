import os.path
import datetime

import numpy as np
import netCDF4 as nC
from osgeo import gdal
from PIL import Image
import glob

import configBETA

"""
This python script is developed using python version 3.8.6 64bit
"""

DEBUG = False
DEBUG1 = False


def get_timestamp_filename(time):
    utc_timestamp_sec = (time * 3600) + 631152000  # 631152000 seconds between Jan 1 1990 and Jan 1 1970
    date = datetime.datetime.utcfromtimestamp(utc_timestamp_sec)
    return date.strftime("%Y%m%d%H0000")


def give_warning_of_existed_file(path):
    result = os.path.exists(path)
    if result:
        if os.path.isdir(path):
            print(f"ERROR [{path}] exists as a directory and is not a file")
        else:
            print(f"WARNING File [{path}] exists. File will be over-written")
    return result


def give_warning_of_non_exist_directory(path):
    is_exist = os.path.exists(path)
    if is_exist and not os.path.isdir(path):
        print(f"ERROR [{path}] exists not as a directory")
    return is_exist


def lonlat_to_index_v2(reference, target, step):
    result = (target - reference) / step
    result = round(result, ndigits=None)
    if result < 0:
        print(f"ERROR there could be something wrong: {result}=round(({target}-{reference})/{step})<0")
    return result


def index_to_lonlat_v2(index, reference, step):
    """
    Convert data's geo longitude/latitude value based on given index
    :param index: input index
    :param reference: array's upper-left element's corresponding longitude/latitude
    :param step: must be non-negative number, will automatically convert if is negative
    :return:
    """
    result = reference + index * step
    if result <= 0:
        print(f"ERROR there could be something wrong: {result}={reference}+{index}*{step})<0")
        print(
            "INFO If you expect your longitude/latitude to be a negative value, "
            "delete this statement to get rid of this message. I live in the east so "
            "negative value does not make sense to me.")
    return result


# for now it only map corresponding pixel by width/height index, no resample is applied
# resample feature will apply in the future
def resize_data_array(resize_width, resize_height, data_2d_array):
    data_height, data_width = data_2d_array.shape
    resize_2d_array = np.zeros((resize_height, resize_width))
    height_scale = data_height / resize_height
    width_scale = data_width / resize_width
    for height_index in range(resize_height):
        print(f"Resizing {height_index + 1} of {resize_height} rows")
        for width_index in range(resize_width):
            data_h_index = round(height_index * height_scale, ndigits=None)
            if data_h_index >= data_height:
                data_h_index = data_height - 1
            data_w_index = round(width_index * width_scale, ndigits=None)
            if data_w_index >= data_width:
                data_w_index = data_w_index - 1
            resize_2d_array[height_index, width_index] = data_2d_array[data_h_index, data_w_index]
    return resize_2d_array


def save_rgb_tif(rgb_array, path_s, geo_transform_s, geo_projection_s, filename_s):
    """
    save 2d matrix to tif
    :param geo_projection_s: geo projection (can be None)
    :param geo_transform_s: get transform (can be None)
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param rgb_array: rgb array, must be [r_matrix, g_matrix, b_matrix] or U R MESSING WITH ME
    """
    if len(rgb_array) != 3:
        print("U R MESSING WITH ME")
        os.system("pause")
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    save_filename = os.path.join(path_s, filename_s)
    image_height = rgb_array[0].shape[0]
    image_width = rgb_array[0].shape[1]
    print(f"Saving image {save_filename}: size = {image_width}x{image_height}")
    out_dataset = geo_tiff_driver.Create(save_filename, image_width, image_height, 3, gdal.GDT_Byte)
    if geo_transform_s is not None and geo_projection_s is not None:
        out_dataset.SetGeoTransform(geo_transform_s)
        out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(rgb_array[0])
    out_dataset.GetRasterBand(2).WriteArray(rgb_array[1])
    out_dataset.GetRasterBand(3).WriteArray(rgb_array[2])
    out_dataset.FlushCache()


# not sure if it will be useful
def source_index_to_target_index(src_index, src_ref_lonlat, src_step, target_ref_lonlat, target_step):
    the_middle_thing = index_to_lonlat_v2(src_index, src_ref_lonlat, src_step)
    the_thing = lonlat_to_index_v2(target_ref_lonlat, the_middle_thing, target_step)
    return the_thing


def coloring_bg_image(focusing_index_extent_ldru, bg_r, bg_g, bg_b, data_content, data_color_break_points, data_color_ramp,
                      legend_rgb, legend_position_offset, bg_height, bg_width):
    # test
    im = Image.open("./reference/0a.png")
    pix = im.load()
    # end of test
    y_legend_start_index = bg_height - im.size[1]
    last_color_index = len(data_color_break_points)
    for y_index in range(focusing_index_extent_ldru[3], focusing_index_extent_ldru[1]):
        for x_index in range(focusing_index_extent_ldru[0], focusing_index_extent_ldru[2]):
            if (bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index]) != configBETA.DefaultColor.REPLACE_TARGET:
                continue
            for color_index in range(last_color_index):
                if data_content[y_index, x_index] < data_color_break_points[color_index]:
                    bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index] = \
                        data_color_ramp[color_index]
                    break
                if color_index + 1 == last_color_index:
                    bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index] = \
                        data_color_ramp[last_color_index]
    for y_index in range(0, im.size[1]):
        for x_index in range(0, im.size[0]):
            bg_x_index = x_index + legend_position_offset
            bg_y_index = bg_height - im.size[1] - legend_position_offset
            bg_r[bg_y_index, bg_x_index], bg_g[bg_y_index, bg_x_index], bg_b[bg_y_index, bg_x_index] = \
                pix[x_index, y_index][0:3]
    return bg_r, bg_g, bg_b


def rain_process_v2(bg_r, bg_g, bg_b,
                    bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                    data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                    data_lon_step, data_lat_step):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent relative to it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    current_time_index = 0
    rain_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    nc_lat_max, nc_lat_min = np.where(data_overlay_ldru[3] > data_overlay_ldru[1],
                                      (data_overlay_ldru[3], data_overlay_ldru[1]),
                                      (data_overlay_ldru[1], data_overlay_ldru[3]))
    lat_index_min = round((bg_extent_ldru[3] - nc_lat_max) / abs(bg_lat_step), ndigits=None)
    lat_index_min = np.where(lat_index_min < 0, 0, lat_index_min)
    lat_index_max = round((bg_extent_ldru[3] - nc_lat_min) / abs(bg_lat_step), ndigits=None)
    lat_index_max = np.where(lat_index_max > bg_lat_size, bg_lat_size, lat_index_max)
    nc_lon_max, nc_lon_min = np.where(data_overlay_ldru[2] > data_overlay_ldru[0],
                                      (data_overlay_ldru[2], data_overlay_ldru[0]),
                                      (data_overlay_ldru[0], data_overlay_ldru[2]))
    lon_index_min = round((nc_lon_min - bg_extent_ldru[0]) / bg_lon_step, ndigits=None)
    lon_index_min = np.where(lon_index_min < 0, 0, lon_index_min)
    lon_index_max = round((nc_lon_max - bg_extent_ldru[0]) / bg_lon_step, ndigits=None)
    lon_index_max = np.where(lon_index_max > bg_lon_size, bg_lon_size, lon_index_max)
    data_union_lat_index_min = source_index_to_target_index(lat_index_min, bg_extent_ldru[3], bg_lat_step, data_overlay_ldru[3], data_lat_step)
    data_union_lat_index_max = source_index_to_target_index(lat_index_max, bg_extent_ldru[3], bg_lat_step, data_overlay_ldru[1], data_lat_step)
    data_union_lon_index_min = source_index_to_target_index(lon_index_min, bg_extent_ldru[0], bg_lon_step, data_overlay_ldru[3], data_lon_step)
    data_union_lat_index_max = source_index_to_target_index(lat_index_max, bg_extent_ldru[0], bg_lon_step, data_overlay_ldru[3], data_lon_step)
    if DEBUG1:
        print(f"lat index min max = {lat_index_min}, {lat_index_max}")
        print(f"lon index min max = {lon_index_min}, {lon_index_max}")
    while current_time_index + data_time_index_step < len(data_time_array):
        rain_array = np.zeros(rain_array_shape)
        for time in range(current_time_index, current_time_index + data_time_index_step):
            rain_array = np.add(rain_array, data_3d_array[time, :, :])
        if data_lat_step > 0:  # must flip its latitude data upside down cos lower the index higher the latitude
            rain_array = np.flip(rain_array, 0)  # higher the index, lower the latitude, and its step should be negative
        data_content = resize_data_array(resize_height=bg_lat_size, resize_width=bg_lon_size, data_2d_array=rain_array)
        bg_r, bg_g, bg_b = coloring_bg_image([lon_index_min, lat_index_max, lon_index_max, lat_index_min],
                                             bg_r, bg_g, bg_b, data_content, configBETA.DataProperty.Rain.Split,
                                             configBETA.DataProperty.Rain.Color, None, 5, bg_lat_size, bg_lon_size)
        output_rgb_image_array.append((bg_r, bg_g, bg_b))
        current_time_index += data_time_index_step
        break
    print()
    remain = len(data_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignore remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def main_process():
    precision = 2  # x digits after decimal
    output_directory = configBETA.Directory.output_directory
    input_directory = configBETA.Directory.input_directory
    reference_directory = configBETA.Directory.reference_directory  # background image should be placed here
    input_file_extension = "*.tif"  # specify input data (things to overlay)
    background_files = "weather grey content.tif"  # background image file
    if not give_warning_of_non_exist_directory(output_directory):
        print("INFO output folder is being created")
        os.mkdir(output_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print("ERROR input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print("ERROR reference folder not found")
        exit(2)
    # read background image
    bg_dataset = gdal.Open(os.path.join(reference_directory, background_files))
    bg_geo_transform = bg_dataset.GetGeoTransform()
    bg_geo_projection = bg_dataset.GetProjection()
    bg_red = bg_dataset.GetRasterBand(1).ReadAsArray()
    bg_green = bg_dataset.GetRasterBand(2).ReadAsArray()
    bg_blue = bg_dataset.GetRasterBand(3).ReadAsArray()
    bg_width = bg_dataset.RasterXSize
    bg_height = bg_dataset.RasterYSize
    bg_minx = bg_geo_transform[0]
    bg_maxy = bg_geo_transform[3]
    bg_x_step = bg_geo_transform[1]
    bg_y_step = bg_geo_transform[5]
    bg_maxx = bg_minx + bg_x_step * bg_dataset.RasterXSize
    bg_miny = bg_maxy + bg_y_step * bg_dataset.RasterYSize
    bg_extent_ldru = [bg_minx, bg_miny, bg_maxx,
                      bg_maxy]  # background image extent, [left, down, right, up], correspond
    print(f"Background image size: {bg_width}x{bg_height}")
    print(f"Background image extent: left={bg_minx}, right={bg_maxx}, top={bg_maxy}, bottom={bg_miny}")

    # NC input file section
    nc_data_path = glob.glob(os.path.join(input_directory, "*.nc"))
    for item in nc_data_path:
        nc_dataset = nC.Dataset(item)  # 读取nc数据集
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_time_array = np.array(nc_dataset.variables["time"][:])
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_time_step = 24
        nc_overlay_extent_ldru = [nc_lon_array.min(), nc_lat_array.min(), nc_lon_array.max(), nc_lat_array.max()]
        nc_lat_step = (nc_overlay_extent_ldru[3] - nc_overlay_extent_ldru[1]) / nc_lat_array_size
        nc_lon_step = (nc_overlay_extent_ldru[2] - nc_overlay_extent_ldru[0]) / nc_lon_array_size
        nc_overlay_array_ldru = [nc_lon_array[0], nc_lat_array[len(nc_lat_array) - 1],
                                 nc_lon_array[len(nc_lon_array) - 1], nc_lat_array[0]]
        # rain section
        images = rain_process_v2(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                 bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                 bg_lat_size=bg_height,
                                 data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                 data_3d_array=nc_dataset.variables[configBETA.DataProperty.Rain.NCFieldTag][:],
                                 data_lat_step=nc_lat_step, data_lon_step=nc_lon_step, data_time_array=nc_time_array)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            filename_to_save = f"{filename}_rain_{time_index}-{time_index + nc_time_step}h.tif"
            save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step
    if len(nc_data_path) == 0:
        print("INFO no input .nc file found")


if __name__ == "__main__":
    print("ready get set go")
    main_process()
    print("and stop")
