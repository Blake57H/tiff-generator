import arcpy
import os.path
import warnings
import numpy as np
import netCDF4 as nC
import datetime
from osgeo import gdal, osr, ogr
import glob

warnings.filterwarnings("ignore")

path = "subjects/"
output_path = "output/"
filePrefix = "pre_"
fileExtension = ".tif"


def tif_filename_generate(filename_s, hour_s, data_field_name_s):
    """
    Generate filename based on field (exclude path)
    :param filename_s: timestamp (in this case) or whatever you like
    :param hour_s: hour coverage
    :param data_field_name_s: data field (TEM/WS/WD or whatever it is)
    :return: f"{filename_s}_{data_field_name_s}_{hour_s}h.tif"
    """
    return "%s_%s_%sh.tif" % (filename_s, data_field_name_s, hour_s)


def generate_timestamp_filename(time):
    utc_timestamp_sec = (time * 3600) + 631152000  # 631152000 seconds between Jan 1 1990 and Jan 1 1970
    date = datetime.datetime.utcfromtimestamp(utc_timestamp_sec)
    return date.strftime("%Y%m%d%H0000")


def check_file_existence(filename, dir_path):
    filepath = os.path.join(dir_path, filename)
    return os.path.exists(filepath)


def save_tif(data_array_2d_s, path_s, geo_transform_s, geo_projection_s, filename_s):
    """
    save 2d matrix to tif
    :param geo_projection_s: geo projection
    :param geo_transform_s: get transform
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param data_array_2d_s: data
    """
    geoTiffDriver = gdal.GetDriverByName("GTiff")
    save_filename = "%s%s" % (path_s, filename_s)
    # print(data_array_2d_s.shape)
    print("Saving image %s: size = %sx%s" % (save_filename, data_array_2d_s.shape[1], data_array_2d_s.shape[0]))
    outDataset = geoTiffDriver.Create(save_filename, data_array_2d_s.shape[1], data_array_2d_s.shape[0], 1,
                                      gdal.GDT_Float32)
    outDataset.SetGeoTransform(geo_transform_s)
    outDataset.SetProjection(geo_projection_s)
    outDataset.GetRasterBand(1).WriteArray(data_array_2d_s)
    outDataset.FlushCache()
    outDataset = None


def judge_leap_year(year):
    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                return 8783
            else:
                return 8759
        else:
            return 8783
    else:
        return 8759


def nc_2_tiff_converter(data):
    """
    convert .nc file to .tif
    :param data:.nc filepath
    :return:None
    """
    print("Processing %s" % data)
    nc_data_obj = nC.Dataset(data)
    data_list = list(nc_data_obj.variables.keys())
    Lon = nc_data_obj.variables["lon"][:]
    Lat = nc_data_obj.variables["lat"][:]
    LonMin, LatMax, LonMax, LatMin = [Lon.min(), Lat.max(), Lon.max(), Lat.min()]
    N_Lat = len(Lat)
    N_Lon = len(Lon)
    Lon_Res = (LonMax - LonMin) / (float(N_Lon) - 1)
    Lat_Res = (LatMax - LatMin) / (float(N_Lat) - 1)
    print("Image size: %sx%s" % (N_Lon, N_Lat))
    # test
    test_switch = False
    if test_switch:
        print("%s ; %s" % (Lon[5], Lon))
        print("%s ; %s" % (Lat[5], Lat))
        print(nc_data_obj['TEM'][2, :, :].shape)
        print(nc_data_obj['TEM'][2, Lat[5], Lon[3]])
        print(nc_data_obj['TEM'][2, 5, 3])
        test = np.zeros((N_Lon, N_Lat))
        print(test.shape)
        exit(0)
    # end of test
    geotransform = (LonMin, Lon_Res, 0, LatMax, 0, -Lat_Res)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    global output_path

    time_middle = nc_data_obj.variables['time'][:]
    # initialize variable
    rain_array = np.zeros((N_Lat, N_Lon))
    rain_filename = ""
    for j in range(len(time_middle)):
        time = generate_timestamp_filename(time_middle[j])
        if len(rain_filename) == 0:
            rain_filename = time
        # process rain data (every 1h)
        # print(f"data={nc_data_obj['PRE_1h'][j, :, :].T.shape}")
        # print(f"summary={rain_array.shape}")
        rain_array = np.add(rain_array, nc_data_obj['PRE_1h'][j, :, :])

        filename = tif_filename_generate(filename_s=rain_filename, hour_s=(j % 24) + 1, data_field_name_s="rain")
        if (j + 1) % 24 == 0:
            save_tif(geo_transform_s=geotransform, geo_projection_s=srs.ExportToWkt(), path_s=output_path,
                     data_array_2d_s=np.flip(rain_array, 0), filename_s=filename)
            rain_array = np.zeros((N_Lat, N_Lon))
            rain_filename = ""

    # about wind and temp (fine max value in a 24h range)
    print("Mapping max value for wind and temperature at %s" % (generate_timestamp_filename(time_middle[0])))
    temp_field = "TEM"
    windspeed_field = "WS"
    time_section = 0  # an hour offset indicating which part of data to extract
    time_step = 24  # every 24 hours
    while time_section < len(time_middle):
        time_section_start = time_section
        time_section_end = np.where(time_section + time_step > len(time_middle), len(time_middle),
                                    time_section + time_step)
        windspeed_array = np.zeros((N_Lat, N_Lon))
        temp_array = np.zeros((N_Lat, N_Lon))
        time = generate_timestamp_filename(time_middle[time_section])
        windspeed_existence = check_file_existence(filename=tif_filename_generate(filename_s=time, hour_s=time_step,
                                                                                  data_field_name_s=windspeed_field),
                                                   dir_path=output_path
                                                   )
        temperature_existence = check_file_existence(filename=tif_filename_generate(filename_s=time, hour_s=time_step,
                                                                                    data_field_name_s=temp_field),
                                                     dir_path=output_path
                                                     )
        for iLon in range(N_Lon):
            print("\r\t%sh~%sh, %s of %s complete" % (time_section, time_section + time_step, iLon, N_Lon), )
            if temperature_existence and windspeed_existence:
                break
            for iLat in range(N_Lat):
                if not temperature_existence:
                    temp_range = np.array(
                        nc_data_obj.variables[temp_field][time_section:time_section + time_step, iLat, iLon])
                    temp_array[iLat, iLon] = temp_range.max()
                if not windspeed_existence:
                    windspeed_range = nc_data_obj.variables[windspeed_field][time_section:time_section + time_step, 0,
                                      iLat, iLon]  # 0 means height level of 10
                    windspeed_array[iLat, iLon] = windspeed_range.max()
                # print(f"{iLat+1} of {N_Lat}", end="\r")
        print("")

        filename = tif_filename_generate(filename_s=time, hour_s=time_step, data_field_name_s=temp_field)
        if not temperature_existence:
            save_tif(geo_transform_s=geotransform, geo_projection_s=srs.ExportToWkt(),
                     data_array_2d_s=np.flip(temp_array, 0), path_s=output_path, filename_s=filename)
        else:
            print("%s exists, ignore .tif save" % filename)

        filename = tif_filename_generate(filename_s=time, hour_s=time_step, data_field_name_s=windspeed_field)
        if not windspeed_existence:
            save_tif(geo_transform_s=geotransform, geo_projection_s=srs.ExportToWkt(),
                     data_array_2d_s=np.flip(windspeed_array, 0), path_s=output_path, filename_s=filename)
        else:
            print("%s exists, ignore .tif save" % filename)

        time_section += time_step


if __name__ == "__main__":
    print("ready get set go")
    nc_files = glob.glob("./source data/*.nc")
    specific_file = "source data/JTE2ZSO3_yb_gdfs-meter_3d2.5km1h_202107091200_202107100800_202107130800.nc"
    if len(nc_files) == 0:
        print("No input file")
    if not os.path.exists(specific_file):
        print("Specific file not found")
    else:
        nc_2_tiff_converter(specific_file)
    for nc_file in nc_files:
        nc_2_tiff_converter(nc_file)
    print("and stop")
