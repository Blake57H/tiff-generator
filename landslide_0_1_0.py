import netCDF4 as nC
import numpy as np
from osgeo import osr

from ftp_file_fetcher_0_2_0 import FTPLoginInfo
from ftp_file_fetcher_0_2_0 import main_process as ftp_download_process
from utility_0_1_1 import *


def get_weather_nc_data(date):
    # this function is used to download today's weather data only (default)
    # subject 'today' is defined as 'at the date of running this script'
    # or can set to some specific date as wish
    if not isinstance(date, datetime.datetime):
        print(f"ERROR [date] input type error, expect {type(datetime.datetime)}, got {type(date)}")
        return
    login_info = FTPLoginInfo(
        port=CannotClassify.ftp_port,
        address=CannotClassify.ftp_address,
        username=CannotClassify.ftp_username,
        password=CannotClassify.ftp_password
    )
    # date = date - datetime.timedelta(days=20)  # test only
    dir_set = Directory()
    remote_path = dir_set.get_expected_remote_weather_data(date)
    local_path = dir_set.get_weather_nc_directory(date)
    ftp_download_process(local_path, login_info, remote_path)


def process_landslide_data(date):
    if not isinstance(date, datetime.datetime):
        print(f"ERROR [date] input type error, expect {type(datetime.datetime)}, got {type(date)}")
        return
    output_directory = Directory.output_directory
    input_directory = Directory().get_weather_nc_directory(date)
    reference_directory = Directory.reference_directory  # background image should be placed here
    temporary_directory = Directory.temporary_directory
    if not give_warning_of_non_exist_directory(output_directory):
        print("INFO output folder is being created")
        os.mkdir(output_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print("ERROR input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print("ERROR reference folder not found")
        exit(2)

    nc_data_path = glob.glob(os.path.join(input_directory, Name.weather_data_file))

    dem = read_tif_data(os.path.join(reference_directory, Name.landslide_DEM))
    slope = read_tif_data(os.path.join(reference_directory, Name.landslide_slope))
    terrain = read_tif_data(os.path.join(reference_directory, Name.landslide_terrain))
    ndvi = read_tif_data(os.path.join(reference_directory, Name.landslide_NDVI))
    river = read_tif_data(os.path.join(reference_directory, Name.landslide_river))
    landslipe = read_tif_data(os.path.join(reference_directory, Name.landslide_history_density))
    # TuRang = read_tif_data("./dataset/")
    Town = read_tif_data(os.path.join(reference_directory, Name.landslide_town))
    Road = read_tif_data(os.path.join(reference_directory, Name.landslide_road))

    template = return_dataset(os.path.join(reference_directory, Name.landslide_road))
    geo_transformation = template.GetGeoTransform()
    geo_projection = template.GetProjection()

    mask_abs_path = os.path.abspath(os.path.join(reference_directory, Name.landslide_DEM))

    # 高程 ＜３００，＞２０００ ３００～５００ ５００～１０００ １０００～１５００ １５００～２０００
    dem[dem == dem[0][0]] = np.nan
    dem[(dem >= 2000) | (dem < 300)] = 1
    dem[(dem >= 300) & (dem < 500)] = 2
    dem[(dem >= 500) & (dem < 1000)] = 3
    dem[(dem >= 100) & (dem < 1500)] = 4
    dem[(dem >= 1500) & (dem < 2000)] = 5

    # 坡度 ＜８，＞５０ ８～１５ １５～２５ ２５～３０ ３０～５０
    slope[slope == slope[0][0]] = np.nan
    slope[(slope >= 50) | (slope < 8)] = 1
    slope[(slope >= 8) & (slope < 15)] = 2
    slope[(slope >= 15) & (slope < 25)] = 3
    slope[(slope >= 25) & (slope < 30)] = 4
    slope[(slope >= 30) & (slope < 50)] = 5

    # 地形起伏度 ０～５０ ５０～１００ １００～１８０ １８０～２５０ ＞２５０
    terrain[terrain == terrain[0][0]] = np.nan
    terrain[(terrain >= 0) & (terrain < 50)] = 1
    terrain[(terrain >= 50) & (terrain < 100)] = 2
    terrain[(terrain >= 100) & (terrain < 180)] = 3
    terrain[(terrain >= 180) & (terrain < 250)] = 4
    terrain[terrain >= 250] = 5

    # NDVI ＞０．７ ０．５～０．７ ０．４～０．５ ０．３～０．４ ＜０．３
    ndvi[ndvi == ndvi[0][0]] = np.nan
    ndvi[ndvi >= 0.7] = 1
    ndvi[(ndvi >= 0.5) & (ndvi < 0.7)] = 2
    ndvi[(ndvi >= 0.4) & (ndvi < 0.5)] = 3
    ndvi[(ndvi >= 0.3) & (ndvi < 0.4)] = 4
    ndvi[ndvi < 0.3] = 5

    # 到河流的距离 ＞５００ ３００～５００ １００～３００ ５０～１００ ＜５０
    river[river == river[0][0]] = np.nan
    river[river < 50] = 5
    river[river >= 500] = 1
    river[(river >= 300) & (river < 500)] = 2
    river[(river >= 100) & (river < 300)] = 3
    river[(river >= 50) & (river < 100)] = 4

    # 滑坡点核密度 <174 174~364 364~560 560~810 >810
    landslipe[landslipe == landslipe[0][0]] = np.nan
    landslipe[landslipe < 174] = 1
    landslipe[(landslipe >= 174) & (landslipe < 364)] = 2
    landslipe[(landslipe >= 364) & (landslipe < 560)] = 3
    landslipe[(landslipe >= 560) & (landslipe < 810)] = 4
    landslipe[landslipe >= 810] = 5

    # 到居民地距离 ＞２００ １５０～２００ １００～１５０ ５０～１００ ＜５０
    Town[Town == Town[0][0]] = np.nan
    Town[Town < 50] = 5
    Town[Town >= 200] = 1
    Town[(Town >= 150) & (Town < 200)] = 2
    Town[(Town >= 100) & (Town < 150)] = 3
    Town[(Town >= 50) & (Town < 100)] = 4

    # 到公路距离  ＞２００ １５０～２００ １００～１５０ ５０～１００ ＜５０
    Road[Road == Road[0][0]] = np.nan
    Road[Road < 50] = 5
    Road[Road >= 200] = 1
    Road[(Road >= 150) & (Road < 200)] = 2
    Road[(Road >= 100) & (Road < 150)] = 3
    Road[(Road >= 50) & (Road < 100)] = 4

    list_of_tif_paths = list()
    for item in nc_data_path:
        print(f"Processing nc data {item}...")
        list_of_tif_paths.extend(forge_landslide_rain_tiff(item, temporary_directory))
    print(list_of_tif_paths)  # test
    for tif in list_of_tif_paths:
        print(f"Processing tif data {tif}...")
        print('Resampling data...')
        path, name, extension, filename = get_file_path_name_extension(tif)
        resampled_filename = name + '-resampled.' + extension
        resampled_filepath = os.path.abspath(os.path.join(path, resampled_filename))
        code = arcpy_resample_helper(os.path.abspath(tif), 0.005, resampled_filepath)
        if code != 0:
            print(f"ERROR resample execution failed, return code {code}")
            continue
        print("Extracting data by mask...")
        _, name_r, extension_r, _ = get_file_path_name_extension(resampled_filename)
        extracted_filename = name_r + '_extracted.' + extension_r
        extracted_filepath = os.path.abspath(os.path.join(path, extracted_filename))
        code = arcpy_extract_by_mask_helper(resampled_filepath, mask_abs_path, extracted_filepath, mask_abs_path)
        if not code == 0:
            print(f"ERROR extract by mask execution failed, returned code {code}")
            continue
        print("Computing landslide possibility...")
        rain = read_tif_data(os.path.join(path, extracted_filename))
        rain[rain == rain[0][0]] = np.nan
        rain[(rain >= 0.1) & (rain < 10)] = 2
        rain[(rain >= 10) & (rain < 25)] = 3
        rain[(rain >= 25) & (rain < 50)] = 4
        rain[rain >= 50] = 5
        rain[rain < 0.1] = 1
        landslide_risk = 0.09 * dem + 0.103 * slope + 0.091 * terrain + 0.102 * ndvi + 0.106 * rain + 0.074 * river + 0.109 * landslipe + 0.054 * Town + 0.043 * Road
        print("Saving result as tif data...")
        output_filename = f'landslide_risk_{name}.{extension}'
        png_output_filename = f'landslide_risk_{name}.png'
        output_filepath = Directory().get_landslide_report_directory(date)
        save_tif(landslide_risk, output_filepath, geo_transformation, geo_projection, output_filename)
        print("Rendering result to png image....")
        code = arcpy_mxd_to_png_helper(
            mxd_absolute_path=os.path.join(Directory().get_landslide_mxd_template_directory(), Name.landslide_mxd_template),
            input_file_absolute_path=os.path.abspath(os.path.join(output_filepath, output_filename)),
            image_output_absolute_path=os.path.abspath(os.path.join(output_filepath, png_output_filename)),
            replace_index=1)
        if not code == 0:
            print(f"ERROR png file export failed, process returned {code}")
            continue
        print(f"Tif file {tif} processing has complete")



def forge_landslide_rain_tiff(nc_file_full_path, directory_to_save):
    """
    Convert nc rain data into tif format for landslide use
    :param nc_file_full_path: relative/absolute full path to nc file
    :param directory_to_save: directory where tifs should be saved
    :return: list of full path to tif files
    """
    item = nc_file_full_path
    tiff_filename = []
    item_date = item.split('_')[-2:-1][0]
    datetime_item_date = datetime.datetime.strptime(item_date, CannotClassify.weather_filename_start_time_format)
    print(f"Start date of NC dataset is: {datetime_item_date}")
    nc_dataset = nC.Dataset(item)  # 读取nc数据集
    nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
    nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
    nc_time_array = np.array(nc_dataset.variables["time"][:])
    nc_lon_array_size = len(nc_lon_array)
    nc_lat_array_size = len(nc_lat_array)
    nc_time_step = 24
    nc_data_obj = nC.Dataset(item)
    nc_start_datetime = utc_timestamp_sec_to_datetime_since_1990(nc_time_array[0])

    nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size - 1], nc_lat_array[0]
    nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / (nc_lat_array_size - 1)
    nc_lat_need_flip = nc_lat_step > 0
    nc_lat_top = nc_lat_index_max if nc_lat_need_flip else nc_lat_index_min
    nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size - 1], nc_lon_array[0]
    nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / (nc_lon_array_size - 1)
    nc_lon_need_flip = nc_lon_step < 0
    nc_lon_left = nc_lon_index_min if not nc_lon_need_flip else nc_lon_index_max
    geo_transformation = (nc_lon_left, nc_lon_step if not nc_lon_need_flip else -nc_lon_step, 0, nc_lat_top, 0,
                          -nc_lat_step if nc_lat_need_flip else nc_lat_step)

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
    geo_projection = srs.ExportToWkt()

    rain_field_tag = DataProperty.Rain.NCFieldTag
    rain_friendly_name = DataProperty.Rain.FriendlyName
    # initialize variable
    rain_array = np.zeros((nc_lat_array_size, nc_lon_array_size))
    # 逐小时遍历时间
    j = 0
    while j + nc_time_step <= len(nc_time_array):
        # process rain data (every 1h)
        # print(f"data={nc_data_obj['PRE_1h'][j, :, :].T.shape}")
        # print(f"summary={rain_array.shape}")
        rain_array = np.zeros((nc_lat_array_size, nc_lon_array_size))
        for i in range(j, j + nc_time_step):
            rain_array = np.add(rain_array, nc_data_obj[rain_field_tag][i, :, :])
        filename = Name().get_nc_dataset_tif_name(nc_start_datetime,
                                                  f"{j}h",
                                                  f"{j + nc_time_step}h",
                                                  rain_friendly_name)
        path = directory_to_save
        if nc_lat_need_flip:
            rain_array = np.flip(rain_array, 0)
        if nc_lon_need_flip:
            rain_array = np.flip(rain_array, 1)
        if give_warning_of_existed_file(os.path.join(path, filename)):
            print(f"WARNING: {filename} exists, replacing file")
        save_tif(geo_transform_s=geo_transformation, geo_projection_s=geo_projection, path_s=path,
                 data_array_2d_s=rain_array, filename_s=filename)
        tiff_filename.append(os.path.join(path, filename))
        j += nc_time_step

    return tiff_filename


def main_process(date=datetime.datetime.now()):
    print("Landslide component starting...")
    get_weather_nc_data(date)
    process_landslide_data(date)
    print("Landslide component finishing...")


if __name__ == '__main__':
    print("Landslide component v0.1.0")
    main_process()
