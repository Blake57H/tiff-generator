import argparse
import arcpy

from arcpy_config_1_0 import *

print("============RESAMPLE TOOL=============")

parser = argparse.ArgumentParser(description=Helps.mxd_to_image_description)
parser.add_argument("-input", "--input", dest="input_shp", help=Helps.shp_input, default="", type=str, required=True)
parser.add_argument("-output", "--output", dest="output_image", help=Helps.img_output, default="", type=str,
                    required=True)
parser.add_argument("-size", "--size", dest="size", help=Helps.cell_size_value, default=-9.9, type=float, required=False)
parser.add_argument("-size_ref", "--size_ref", dest="size_ref", help=Helps.replace_index, default="",
                    type=str, required=False)
parser.add_argument("-method", "--method", dest="method", help=Helps.resample_method, default="BILINEAR",
                    type=str, required=False)

args = parser.parse_args()
input_file_full_path = args.input_shp
output_image_file_full_path = args.output_image
cell_size_value = args.size
cell_size_reference = args.size_ref
method = args.method
print("INFO input file path is {}".format(input_file_full_path))
print("INFO tif image will be saved to {}".format(output_image_file_full_path))

cell_size = None
if cell_size_value != -9.9:
    cell_size = str(cell_size_value)
elif cell_size_reference != "":
    cell_size = cell_size_reference
if method not in ["NEAREST", "BILINEAR", "CUBIC", "MAJORITY"]:
    print("WARNING {0} is not a valid method, will use default method BILINEAR".format(method))
    method = "BILINEAR"
if cell_size is None:
    print("ERROR CODE 1 either argument [-size] or [-size_ref] must be given")
    exit(1)
elif not os.path.exists(input_file_full_path):
    print("ERROR CODE 2 input file {} not found".format(input_file_full_path))
    exit(2)
print("INFO resample method is {}".format(method))
print("INFO resample cell size is set to {}".format(cell_size))
print("Resampling....")
arcpy.Resample_management(input_file_full_path, output_image_file_full_path, cell_size, method)
print("Resample complete")
print("========RESAMPLE TOOL COMPLETE========")
