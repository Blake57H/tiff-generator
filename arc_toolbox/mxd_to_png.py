﻿import argparse

import arcpy

from arcpy_config_1_0 import *

print("============MXD TO PNG TOOL=============")

parser = argparse.ArgumentParser(description=Helps.mxd_to_image_description)
parser.add_argument("-input", "--input", dest="input_shp", help=Helps.shp_input, default="", type=str, required=True)
parser.add_argument("-output", "--output", dest="output_image", help=Helps.img_output, default="", type=str,
                    required=True)
parser.add_argument("-template", "--template", dest="mxd_file", help=Helps.mxd_file, default="", type=str,
                    required=True)
parser.add_argument("-index", dest="replace_index", help=Helps.replace_index, default=0, type=int, required=True)

args = parser.parse_args()
input_shp_file_full_path = args.input_shp
output_image_file_full_path = args.output_image
mxd_file_full_path = args.mxd_file
replace_index = args.replace_index
print("INFO input file path is {}".format(input_shp_file_full_path))
print("INFO mxd template file is {}".format(mxd_file_full_path))
print("INFO png image will be saved to {}".format(output_image_file_full_path))
print("INFO template replacement data layer index is set to {}".format(replace_index))

input_file_extension = input_shp_file_full_path.split('.')[-1:][0]
if input_file_extension == 'shp':
    work_space_type = "SHAPEFILE_WORKSPACE"
elif input_file_extension in ('tif', 'tiff'):
    work_space_type = "RASTER_WORKSPACE"
else:
    print("ERROR unknown input file extension {}".format(input_file_extension))
    exit(-1)
print("INFO using workspace {}".format(work_space_type))
# exit(0)
if not os.path.exists(mxd_file_full_path):
    print("ERROR mxd template {} not found".format(mxd_file_full_path))
    exit(-1)
if not os.path.exists(input_shp_file_full_path):
    print("ERROR shape file {} not found".format(input_shp_file_full_path))
    exit(-1)

print("INFO mxd template is loading....")
mxd = arcpy.mapping.MapDocument(mxd_file_full_path)
df = arcpy.mapping.ListDataFrames(mxd)[0]
lyr = arcpy.mapping.ListLayers(mxd)[replace_index]

print("INFO constructing png image...")
shp_filename = input_shp_file_full_path.split('\\')[-1:][0].split('/')[-1:][0]
shp_file_path = input_shp_file_full_path.replace(shp_filename, '')
lyr.replaceDataSource(shp_file_path, work_space_type, shp_filename.split('.')[0])
print("INFO png image is saving...")
arcpy.mapping.ExportToPNG(mxd, output_image_file_full_path, resolution=100)
print('INFO png image saved as [{}]'.format(output_image_file_full_path))
print("========MXD TO PNG TOOL COMPLETE========")
