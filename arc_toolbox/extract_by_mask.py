import argparse
import os

import arcpy

from arcpy_config_1_0 import *

print("=========EXTRACT BY MASK TOOL==========")

parser = argparse.ArgumentParser(description=Helps.mxd_to_image_description)
parser.add_argument("-input", "--input", dest="input", help=Helps.shp_input, default=None, type=str, required=True)
parser.add_argument("-output", "--output", dest="output", help=Helps.img_output, default=None, type=str, required=True)
parser.add_argument("-mask", "--mask", dest="mask", help=Helps.extract_by_mask_mask, default=None, type=str,
                    required=True)
parser.add_argument("-ref", "--ref", dest="environment_mask_reference", help=Helps.extract_by_mask_env_mask,
                    default=None, type=str, required=False)

args = parser.parse_args()
input_file_full_path = args.input
output_image_file_full_path = args.output
mask_file_full_path = args.mask
env_mask_full_path = args.environment_mask_reference

print("INFO input file path is {}".format(input_file_full_path))
print("INFO tif image will be saved to {}".format(output_image_file_full_path))
print("INFO mask file is {}".format(mask_file_full_path))
print("INFO environment mask is {}".format("not set" if env_mask_full_path is None else env_mask_full_path))

if not os.path.exists(input_file_full_path):
    print("ERROR input file not found")
    exit(-1)
if not os.path.exists(mask_file_full_path):
    print("ERROR mask file not found")
    exit(-2)
if os.path.exists(output_image_file_full_path):
    print("ERROR output file {} exists, delete file and try again".format(output_image_file_full_path))
    exit(-3)
    '''
    print("WARNING file {} already existed".format(output_image_file_full_path))
    file_extension = output_image_file_full_path.split(".")[-1:][0]
    append_number = 1
    temp_filename = output_image_file_full_path[:-(len(file_extension) + 1)]
    while (os.path.exists(output_image_file_full_path)):
        output_image_file_full_path = "{0}-{1}.{2}".format(temp_filename, append_number, file_extension)
        append_number += 1
    print("INFO output file will be renamed to {}".format(output_image_file_full_path))
    '''
if env_mask_full_path is not None:
    if not os.path.exists(env_mask_full_path):
        print("ERROR reference mask for environment not found, skipping environment setup")
    else:
        print("Setting up environment...")
        arcpy.env.extent = env_mask_full_path
        arcpy.env.mask = env_mask_full_path
        arcpy.env.snapRaster = env_mask_full_path
print("Extracting by mask...")
out = arcpy.sa.ExtractByMask(input_file_full_path, mask_file_full_path)
print("Saving {}...".format(output_image_file_full_path))
out.save(output_image_file_full_path)
print("Completed")

print("=====EXTRACT BY MASK TOOL COMPLETE=====")
