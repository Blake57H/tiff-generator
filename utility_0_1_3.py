from __future__ import annotations  # fix annotation compatibility issue for python version earlier than 3.9

import glob
import os
import pathlib
import subprocess
import datetime
import time

import geopandas  # pip install C:\path_to\Fiona-1.8.20-cp38-cp38-win_amd64.whl, pip install geopandas-0.9.0
import osgeo
import pandas
from osgeo import gdal
from osgeo.gdalconst import GA_ReadOnly

import docx_replace_tool_0_1_2 as drt
import config_1_1_6 as config

"""
This python script is developed using python version 3.9.6 64bit
Require python 3.7 or later to execute
U2NyaXB0IHdyaXR0ZW4gYnkgQmxha2U1NyAmIOiCluWzsA==
"""


def get_timestamp_filename(datetime_in):
    """
    yes
    :param datetime_in: yes
    :return: %Y%m%d%H%M%S
    """
    return datetime_in.strftime("%Y%m%d%H%M%S")


def get_file_path_name_extension(the_file):
    ext = the_file.split('.')[-1:][0]
    if '/' in ext or '\\' in ext or ext == the_file:
        a = the_file
        ext = ''
    else:
        a = the_file[:-(len(ext) + 1)]
    name = a.split("/")[-1:][0].split("\\")[-1:][0]
    if name == a:
        path = ''
    else:
        path = a[:-(len(name))]
    return path, name, ext, f"{name}.{ext}"


def save_tif(data_array_2d_s, path_s, geo_transform_s, geo_projection_s, filename_s: str,
             data_type_s: osgeo.gdalconst = gdal.GDT_Float32):
    """
    save 2d matrix to tif
    :param data_type_s: tif raster band data type, default [gdal.GDT_Float32]
    :param geo_projection_s: geo projection
    :param geo_transform_s: get transform
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param data_array_2d_s: data
    """
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    if not filename_s.endswith('.tif'):
        filename_s += '.tif'
    save_filename = os.path.join(path_s, filename_s)
    # print(data_array_2d_s.shape)
    print(f"Saving image {save_filename}: size = {data_array_2d_s.shape[1]}x{data_array_2d_s.shape[0]}")
    out_dataset = geo_tiff_driver.Create(save_filename, data_array_2d_s.shape[1], data_array_2d_s.shape[0], 1,
                                         data_type_s)
    if geo_transform_s is not None and geo_projection_s is not None:
        out_dataset.SetGeoTransform(geo_transform_s)
        out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(data_array_2d_s)
    out_dataset.FlushCache()


def geojson_to_shape_converter_single(input_file_path, input_file_name, output_directory, output_file_name=None):
    """
    Convert geojson data to shape file
    :param input_file_path: path that contains geojson data
    :param input_file_name: geojson data name
    :param output_directory: path that shape file should shored in
    :param output_file_name: shape file's name, use input file's name by default
    :return: shape file's name, or None if error occurred
    """
    if input_file_name is not None:
        input_file_full_path = os.path.join(input_file_path, input_file_name)
    else:
        input_file_full_path = input_file_path
        input_file_name = input_file_path.split('/')[-1:][0].split('\\')[-1:][0]
    geojson_extension = config.Name.thunder_data_file[1:]
    if not os.path.exists(input_file_full_path):
        print_error_text("input file not exist")
        return None
    elif os.path.isdir(input_file_full_path):
        print_error_text("expect input is file, got directory instead")
        return None
    else:
        if not input_file_full_path.endswith(geojson_extension):
            print_warning_text(f"input file extension is not {config.Name.thunder_data_file[1:]}, error may occur")
        if output_file_name is None:
            output_file_name = input_file_name.replace(geojson_extension, '') + config.Name.shape_file[1:]
        output_file_full_path = os.path.join(output_directory, output_file_name)
        if os.path.exists(output_file_full_path):
            print_info_text(f"file {output_file_name} exists, conversion skipped")
            return output_file_full_path
        try:
            print(f"Converting to SHP: {input_file_full_path}...", end='', flush=True)
            data = geopandas.read_file(input_file_full_path)
            if data.size == 0:
                print_warning_text(f"this data set has {data.size} sets of data")
                return ''
            data.to_file(output_file_full_path, driver='ESRI Shapefile', encoding='utf-8')
            print_success_text("complete")
            return output_file_name
        except Exception as ex:
            print()
            print_error_text(f"An error occurred, reason: {ex}")
            return None


def geojson_to_shape_converter_batch(geojson_directory, shape_directory):
    allfiles = glob.glob(os.path.join(geojson_directory, config.Name.thunder_data_file))
    return_name = list()
    for file_full_path in allfiles:
        filename = file_full_path.split('\\')[-1:][0]
        filepath = file_full_path.replace(filename, '')
        name = geojson_to_shape_converter_single(filepath, filename, shape_directory)
        if name is not None:
            return_name.append(name)
        """
        thunder_data_ext = Name().get_file_extension(Name.thunder_data_file)
        shp_filename = filename.replace(thunder_data_ext, '')+Name.get_file_extension(Name.shape_file)
        shp_full_path = os.path.join(shape_directory, shp_filename)
        print('Processing {}...'.format(file_full_path))
        if os.path.exists(shp_full_path):
            print(f'\tIgnoring existed file [{shp_full_path}]')
        try:
            data = geopandas.read_file(file_full_path)
            data.to_file(shp_full_path, driver='ESRI Shapefile', encoding='utf-8')
        except Exception as ex:
            print("Error: {0}, reason {1}".format(file_full_path, ex))
        else:
            print('\tShape file {0} saved'.format(shp_full_path))
        """
    return return_name


def load_docx_template(template_file_full_path: str, docx_sub_path: str = None) -> str:
    """
    extract template .docx file to temporary folder
    :return: path of extracted .docx file
    """
    if not str.endswith(template_file_full_path, drt.file_extension):
        print(f"ERROR {template_file_full_path} is not a .docx file")
        return None
    if docx_sub_path is None:
        docx_path = config.Directory.report_extract_directory
    else:
        docx_path = os.path.join(config.Directory.temporary_directory, docx_sub_path)
    if give_warning_of_non_exist_directory(docx_path) and len(os.listdir(docx_path)) > 0:
        print_info_text("cleaning docx template work folder, possible program crash in last execution")
        config.clean_temporary_folder(docx_path)
    elif not give_warning_of_non_exist_directory(docx_path):
        os.makedirs(docx_path)
    drt.unzip_template(extract_path=docx_path, file_path=template_file_full_path)
    return docx_path


def zip_docx_report(output_path, output_file_name, content_path):
    if not str.endswith(output_file_name, drt.file_extension):
        print_info_text(f"{output_file_name} is not a {drt.file_extension} file")
        output_file_name += drt.file_extension
        print_info_text(f"file will be saved as {output_file_name}")
    drt.zip_directory(content_path, output_path, output_file_name)
    print_success_text(f"{output_file_name} is saved")


def give_warning_of_existed_file(path, no_print=True):
    """
    as it says
    :param no_print: set True to ignore print command and return existence as boolean
    :param path: yes
    :return: boolean
    """
    result = os.path.exists(path)
    if result:
        if os.path.isdir(path) and not no_print:
            print_error_text(f"[{path}] exists as a directory and is not a file")
        elif not no_print:
            print_warning_text(f"File [{path}] exists. File will be over-written")
    return result


def give_warning_of_non_exist_directory(path, no_print=True):
    """
    as it says (if given path exists but is not a directory, it will print a message)
    :param no_print: set True to ignore print command and return existence as boolean
    :param path: yes
    :return: boolean
    """
    is_exist = os.path.exists(path)
    if is_exist and not os.path.isdir(path) and not no_print:
        print_error_text(f"[{path}] exists not as a directory")
    return is_exist


def print_error_text(text):
    print(f"\033[91m ERROR: {text} \033[0m")


def print_info_text(text):
    print(f"\033[94m INFO: {text} \033[0m")


def print_warning_text(text):
    print(f"\033[93m WARNING: {text} \033[0m")


def print_success_text(text):
    print(f"\033[92m{text}\033[0m")


def thunder_day_update(extract_path, given_date):
    if given_date is None or not isinstance(given_date, datetime.datetime):
        today = datetime.datetime.now()
    else:
        today = given_date
    drt.replace_text_content(keyword=drt.DocxKeyWords.thunder_this_year, replace=str(today.year),
                             extract_path=extract_path)
    drt.replace_text_content(keyword=drt.DocxKeyWords.thunder_this_month, replace=str(today.month),
                             extract_path=extract_path)
    drt.replace_text_content(keyword=drt.DocxKeyWords.thunder_this_day, replace=str(today.day),
                             extract_path=extract_path)
    start_time_text = drt.DocxKeyWords.thunder_this_start_time_str.format(today.hour, today.minute)
    drt.replace_text_content(keyword=drt.DocxKeyWords.thunder_this_start_time, replace=start_time_text,
                             extract_path=extract_path)


def report_image_replace(img_path, target_filename, extract_path, img_filename=None):
    img_full_path = img_path
    if img_filename is not None:
        img_full_path = os.path.join(img_full_path, img_filename)
    drt.replace_image(img_full_path=img_full_path, extract_path=extract_path, target_filename=target_filename)


def report_text_replace(keyword: str, replace: str, extract_path: str) -> None:
    drt.replace_text_content(keyword, replace, extract_path)


def arcpy_mxd_to_png_helper(mxd_absolute_path: str,
                            input_file_absolute_path: list[str],
                            image_output_absolute_path: str,
                            replace_index: list[int],
                            text_content: list[str] = None,
                            text_name: list[str] = None,
                            dpi: int = None):
    print("ArcPY PNG export tool loading...")
    input_list = ''
    for item in input_file_absolute_path:
        input_list += f'\"{item}\" '
    input_index_list = ''
    for item in replace_index:
        input_index_list += f"{item} "
    command = f'{config.Paths.arcgis_python_installation} ' \
              f'\"{config.Paths.arc_tool_mxd_to_png}\" ' \
              f'-input {input_list} ' \
              f'-index {input_index_list}' \
              f'-output \"{image_output_absolute_path}\" ' \
              f'-template \"{mxd_absolute_path}\" '
    if text_content is not None and text_name is not None:
        text_content_list = ''
        for item in text_content:
            text_content_list += f'\"{item}\" '
        command += f'-textbox_text {text_content_list}'

        text_name_list = ''
        for item in text_name:
            text_name_list += f'\"{item}\" '
        command += f'-textbox_name {text_name_list} '
    if dpi is not None:
        command += f'-dpi {dpi} '
    return run_subprocess(command, 'PNG output complete', 'PNG output failed')


def arcpy_extract_by_mask_helper(input_absolute_path, mask_absolute_path, output_absolute_path, ref_mask_abs_path=None):
    print("ArcPY extract by mask tool is loading...")
    command = f'{config.Paths.arcgis_python_installation} ' \
              f'\"{config.Paths.arc_tool_extract_by_mask}\" ' \
              f'-input \"{input_absolute_path}\" ' \
              f'-output \"{output_absolute_path}\" ' \
              f'-mask \"{mask_absolute_path}\" '
    if ref_mask_abs_path is not None:
        command += f'-ref \"{ref_mask_abs_path}\" '
    return run_subprocess(command, "Extract by mask complete", "Extract by mask failed")


def arcpy_resample_helper(input_absolute_path, cell_size, output_absolute_path, resample_method=None):
    print("ArcPY resample tool is loading...")
    command = f'{config.Paths.arcgis_python_installation} ' \
              f'\"{config.Paths.arc_tool_resample}\" ' \
              f'-input \"{input_absolute_path}\" ' \
              f'-output \"{output_absolute_path}\" ' \
              f'-size {cell_size} '
    if resample_method is not None:
        command += f'-method \"{resample_method}\" '
    return run_subprocess(command, "Resample complete", "Resample failed")


def run_subprocess(command: str, success: str = 'Operation completed', failed: str = 'Operation failed'):
    with subprocess.Popen(command) as proc:
        if proc.communicate()[0] is not None:
            print(proc.communicate()[0], flush=True)
        if proc.communicate()[1] is not None:
            print(proc.communicate()[1], flush=True)
        if proc.returncode != 0:
            print_error_text(f"{failed}, returned status {proc.returncode}")
        elif proc.returncode == 0:
            print_success_text(success)
        to_return = proc.returncode
    return to_return


def read_tif_data(tif_full_path):
    """
    读取tif数据
    :param tif_full_path: tif图像的路径
    :return: 对应的numpy二维数组
    """
    tif = gdal.Open(tif_full_path, GA_ReadOnly)
    tif_data = tif.GetRasterBand(1)
    tif_data_ndarray = tif_data.ReadAsArray(0, 0, tif.RasterXSize, tif.RasterYSize)
    tif_data_ndarray = tif_data_ndarray.astype(float)
    return tif_data_ndarray


def return_dataset(tif_full_path) -> gdal.Dataset:
    """
    读取tif数据
    :param tif_full_path: tif图像的路径
    :return: tif_dataset
    """
    tif = gdal.Open(tif_full_path, GA_ReadOnly)
    return tif


def utc_timestamp_sec_to_datetime_since_1990(time) -> datetime.datetime:
    utc_timestamp_sec = (time * 3600) + 631152000  # 631152000 seconds between Jan 1 1990 and Jan 1 1970
    date = datetime.datetime.utcfromtimestamp(utc_timestamp_sec)
    return date


def save_data_sheet_to_xlsx(dataset_sheet: list[list[str]], header: list[str], path: str, name: str) -> bool:
    dataset = pandas.DataFrame(dataset_sheet, columns=header)
    full_path = pathlib.Path(path)
    if not full_path.exists():
        print_info_text(f"creating directory {path}")
        os.makedirs(path)
    if not name.endswith('.xlsx'):
        name += '.xlsx'
    full_path = full_path.joinpath(name)
    print(f"saving [{name}]...")
    try:
        writer = pandas.ExcelWriter(full_path)
        dataset.to_excel(writer, index=False)
        writer.save()
    except Exception as ex:
        print_error_text(f"failed to save [{name}], reason: {ex}")
        return False
    print_success_text(f"{name} saved")
    return True


def docx_to_pdf(input_docx_full_path, preferred_method="libre-office", try_other_method=False):
    if not give_warning_of_existed_file(input_docx_full_path, True):
        print_error_text(f"input file [{input_docx_full_path}] not found, abort pdf conversion")
        return None
    path, _, _, name_ext = get_file_path_name_extension(input_docx_full_path)
    if preferred_method == 'libre-office':
        libre_office_path = config.Paths.libre_office_path
        command = f"{libre_office_path} " \
                  f"--headless " \
                  f"--convert-to pdf:writer_pdf_Export " \
                  f"--outdir \"{os.path.abspath(path)}\" " \
                  f"\"{os.path.abspath(input_docx_full_path)}\" "
        print(f"Exporting docx to pdf with method [{preferred_method}]...")
        if not os.path.exists(libre_office_path.strip("\"")):
            to_return = -1
            print_error_text("Libre Office not found")
            if try_other_method:
                print_info_text(
                    "\n to get rid of this error message, you can\n"
                    "\t1. Disable pdf convert in [Switches.convert_pdf] in config file\n"
                    "or\n"
                    "\t2.\n"
                    "\t\t(1)Install Libre Office\n"
                    "\t\t(2)Change [Paths.libre_office_path] to libre office executable file in config file\n"
                    "or\n"
                    f"\t3. Use other method to perform pdf conversion and set that as default. "
                    f"(unfortunately no other method is available rn, do it manually)\n"
                    f"\tcurrent available method: {config.CannotClassify.docx_to_pdf_methods}"
                )
        else:
            to_return = run_subprocess(command, "Export to pdf complete", "Export to pdf failed")
    else:
        print_error_text(f"unrecognizable method {preferred_method}")
        return None
    if to_return != 0 and try_other_method:
        for item in config.CannotClassify.docx_to_pdf_methods:
            if item != preferred_method:
                print_info_text(f"Retry with method {item}...")
                to_return = docx_to_pdf(input_docx_full_path, item, False)
                if to_return == 0:
                    break
        if to_return != 0:
            print_error_text(f"Failed to convert {name_ext} to PDF")
    return to_return


class HashMap:
    def __init__(self):
        self.map = {}

    def put(self, key: str, value) -> None:
        self.map[key] = value

    def get(self, key: str):
        return self.map.get(key)

    def remove(self, key):
        self.map.pop(key)

    def list(self) -> list[any]:
        return [item for item in self.map.values()]

    def clear(self) -> None:
        self.__init__()


def ten_secs_countdown_exit(seconds: int = 10, exit_code: int = 0) -> None:
    assert type(0) == type(exit_code), f"exit code is not int type: {type(exit_code)}"
    for i in range(seconds):
        print(f"\rProgram has finished its job will exit in {10 - i} second(s)...", flush=True, end='')
        time.sleep(1)
    exit(exit_code)


def ten_secs_countdown(seconds: int = 10, exit_code: int = 0) -> None:
    assert type(0) == type(exit_code), f"exit code is not int type: {type(exit_code)}"
    for i in range(seconds):
        print(f"\rProgram is cooling down for {10 - i} second(s)...", flush=True, end='')
        time.sleep(1)
    print()


def lonlat_to_index_v2(reference, target, step, show_warning: bool = False):
    """
    *this is a copy from weather.py
    calculate index by given index[0]'s longitude/latitude and data's geo resolution
    :param reference: index[0]'s longitude/latitude
    :param target: the longitude/latitude need to be convert to index
    :param step: longitude/latitude resolution?
    :param show_warning:
    :return: index
    """
    result = (target - reference) / step
    result = round(result, ndigits=None)
    if result < 0 and show_warning:
        print_warning_text(f"there could be something wrong: {result}=round(({target}-{reference})/{step})<0")
    return result
