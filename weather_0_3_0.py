import os.path

import os.path
from shutil import copy

import netCDF4 as nC
import numpy as np
from PIL import Image
from osgeo import osr

# custom .py files
from config_1_1_6 import *
from utility_0_1_1 import *
from ftp_file_fetcher_0_2_1 import main_process as ftp_download_process
from ftp_file_fetcher_0_2_1 import FTPLoginInfo

"""
This python script is developed using python version 3.9.6 64bit

Package used in this script and related scripts are listed as follows:
(* at the time of doing weather_0_3_1.py)
netCDF4==1.5.7
numpy==1.21.1
Pillow==8.3.1
zip-files==0.4.1
Fiona @ file:///C:/path_to/Fiona-1.8.20-cp39-cp39-win_amd64.whl
GDAL @ file:///C:/path_to/GDAL-3.3.1-cp39-cp39-win_amd64.whl
"""


def update_report_time(specific_time, extract_path):
    if specific_time is None:
        time = datetime.datetime.now()
    elif not isinstance(specific_time, datetime.datetime):
        time = datetime.datetime.now()
        print_info_text(f"type of given time <{type(specific_time)}> is not <{type(datetime.datetime)}>, "
                        f"using current time instead")
    else:
        time = specific_time
    today = time
    pub_date = today + datetime.timedelta(hours=CannotClassify.publish_hour_offset)
    report_text_replace(keyword=ReportProperties.KeyWords.data_hour, replace=str(today.hour), extract_path=extract_path)
    report_text_replace(keyword=ReportProperties.KeyWords.publish_hour,
                        replace=str(pub_date.hour),
                        extract_path=extract_path)
    report_text_replace(keyword=ReportProperties.KeyWords.this_year, replace=str(today.year), extract_path=extract_path)
    for days in range(0, 4):
        date = today + datetime.timedelta(days=days)
        date_str = ReportProperties.Formats.date_month_day_formats.format(date.month, date.day)
        date_keyword = ReportProperties.KeyWords.day_x.replace(ReportProperties.KeyWords.day_x_keyword, str(days))
        report_text_replace(keyword=date_keyword, replace=date_str, extract_path=extract_path)


def update_report(content_path, keyword, updated_content):
    """
    tabun used in legacy method...
    :param content_path:
    :param keyword:
    :param updated_content:
    :return:
    """
    # print(f"keyword={keyword}, updated content={updated_content}, path={content_path}")  # debug only
    report_text_replace(keyword=keyword, replace=updated_content, extract_path=content_path)


def give_majority_range_name(statistic, friend_range_name, friendly_in_between_text, require_end_stat):
    high = CannotClassify.majority_high
    low = CannotClassify.majority_low
    total = np.sum(statistic)
    percentages = np.divide(statistic, total)
    start_index = end_index = None
    something = 0
    length = len(percentages)
    for index in range(length):
        something += percentages[index]
        if something > low and start_index is None:
            start_index = index
        if something > high and end_index is None:
            end_index = index
    if friendly_in_between_text is None or start_index == end_index:
        majority = friend_range_name[end_index] if require_end_stat else friend_range_name[start_index]
    else:
        majority = f"{friend_range_name[start_index]}{friendly_in_between_text}{friend_range_name[end_index]}"

    low_index = high_index = None

    index = start_index - 1
    while index >= 0:
        if percentages[index] > 0:
            low_index = index
        index -= 1
    index = end_index + 1
    while index <= length - 1:
        if percentages[index] > 0:
            high_index = index
        index += 1
    if low_index is None:
        low_part = None
    elif friendly_in_between_text is None or low_index == start_index - 1:
        low_part = friend_range_name[low_index]
    else:
        low_part = f"{friend_range_name[low_index]}{friendly_in_between_text}{friend_range_name[start_index - 1]}"
    if high_index is None:
        high_part = None
    elif friendly_in_between_text is None or end_index + 1 == high_index:
        high_part = friend_range_name[high_index]
    else:
        high_part = f"{friend_range_name[end_index + 1]}{friendly_in_between_text}{friend_range_name[high_index]}"
    return majority, low_part, high_part


def statistic_summarize(data_2d_array, color_break_points):
    """
    :param data_2d_array: content of data
    :param color_break_points: refer to DataProperty.(SomeData).Split in config.py
    :return: range statistic counts
    """

    statistic = np.zeros(len(color_break_points) + 1)
    break_values_len = len(color_break_points)
    for lat_index in range(len(data_2d_array[0])):
        for lon_index in range(len(data_2d_array[1])):
            break_point = 0
            rain_value = data_2d_array[lat_index, lon_index]
            if rain_value < -100:
                '''
                # assuming empty cell's value is way smaller than -100 after being extracted by arcpy
                # this applies to wind, temperature, and rain
                # (rain and wind value won't ne negative in the first place)
                '''
                continue
            while break_point <= break_values_len:
                if rain_value < color_break_points[break_point]:
                    statistic[break_point] += 1
                    break
                break_point += 1
                if break_point == break_values_len:
                    statistic[break_point] += 1
                    break
    return statistic


def fill_report_weather_description(report_extract_path, friendly_additional_text, friendly_range_name,
                                    content_day_x_keyword, content_addition_day_x_keyword,
                                    friendly_in_between_text, data_array, range_break_point, use_end_stat=True):
    day = 0
    for item in data_array:
        date_replacer = ReportProperties.KeyWords.day_x_keyword
        day_x = day
        rain_day_x_keyword = content_day_x_keyword.replace(date_replacer, str(day_x))
        rain_day_x_addition_keyword = content_addition_day_x_keyword.replace(date_replacer, str(day_x))
        summary = statistic_summarize(item, range_break_point)
        maj, low, high = give_majority_range_name(statistic=summary,
                                                  friend_range_name=friendly_range_name,
                                                  friendly_in_between_text=friendly_in_between_text,
                                                  require_end_stat=use_end_stat)
        update_report(content_path=report_extract_path, keyword=rain_day_x_keyword, updated_content=maj)
        if high is None:
            high_str = ""
        else:
            high_str = friendly_additional_text.format(high)
        update_report(content_path=report_extract_path, keyword=rain_day_x_addition_keyword, updated_content=high_str)
        day += 1


def lonlat_to_index_v2(reference, target, step):
    """
    calculate index by given index[0]'s longitude/latitude and data's geo resolution
    :param reference: index[0]'s longitude/latitude
    :param target: the longitude/latitude need to be convert to index
    :param step: longitude/latitude resolution?
    :return: index
    """
    result = (target - reference) / step
    result = round(result, ndigits=None)
    if result < 0:
        print(f"ERROR there could be something wrong: {result}=round(({target}-{reference})/{step})<0")
    return result


def index_to_lonlat_v2(index, reference, step):
    """
    Convert data's geo longitude/latitude value based on given index
    :param index: input index
    :param reference: array's upper-left element's corresponding longitude/latitude
    :param step: must be non-negative number, will automatically convert if is negative
    :return:
    """
    result = reference + index * step
    if result <= 0:
        print(f"ERROR there could be something wrong: {result}={reference}+{index}*{step})<0")
        print(
            "INFO If you expect your longitude/latitude to be a negative value, "
            "delete this statement to get rid of this message. I live in the east so "
            "negative value does not make sense to me.")
    return result


# for now it only map corresponding pixel by width/height index, no resample is applied
# resample feature will apply in the future
# todo looks like some bug here is making image out of position
# todo: data image is one pixel above its supposed position
# not using this until it is fixed
def resize_data_array(resize_width, resize_height, data_2d_array):
    print("...resizing", end="")
    data_height, data_width = data_2d_array.shape
    resize_2d_array = np.zeros((resize_height, resize_width))
    height_scale = data_height / resize_height
    width_scale = data_width / resize_width
    if Switches.INFO:
        print(f"\nwidth scale = {width_scale}, height scale = {height_scale}")
    for height_index in range(resize_height):
        # print(f"Resizing {height_index + 1} of {resize_height} rows")
        for width_index in range(resize_width):
            data_h_index = round(height_index * data_height / resize_height, ndigits=None)
            if data_h_index >= data_height:
                data_h_index = data_height - 1
            data_w_index = round(width_index * data_width / resize_width, ndigits=None)
            if data_w_index >= data_width:
                data_w_index = data_w_index - 1
            resize_2d_array[height_index, width_index] = data_2d_array[data_h_index, data_w_index]
    print("...done", end="")
    return resize_2d_array


def save_rgb_tif(rgb_array, path_s, geo_transform_s, geo_projection_s, filename_s):
    """
    save rgb bands to tif
    :param geo_projection_s: geo projection (can be None)
    :param geo_transform_s: get transform (can be None)
    :param path_s: file path
    :param filename_s: file name (usually time)
    :param rgb_array: rgb array, must be [r_matrix, g_matrix, b_matrix] or U R MESSING WITH ME
    """
    if len(rgb_array) != 3:
        print(f"ERROR array length mismatch, expect 3, got {len(rgb_array)}")
        os.system("pause")
    geo_tiff_driver = gdal.GetDriverByName("GTiff")
    save_filename = os.path.join(path_s, filename_s)
    give_warning_of_existed_file(save_filename, False)
    image_height = rgb_array[0].shape[0]
    image_width = rgb_array[0].shape[1]
    print(f"Saving image {save_filename}: size = {image_width}x{image_height}")
    out_dataset = geo_tiff_driver.Create(save_filename, image_width, image_height, 3, gdal.GDT_Byte)
    if geo_transform_s is not None and geo_projection_s is not None:
        out_dataset.SetGeoTransform(geo_transform_s)
        out_dataset.SetProjection(geo_projection_s)
    out_dataset.GetRasterBand(1).WriteArray(rgb_array[0])
    out_dataset.GetRasterBand(2).WriteArray(rgb_array[1])
    out_dataset.GetRasterBand(3).WriteArray(rgb_array[2])
    out_dataset.FlushCache()


# not sure if it will be useful
def source_index_to_target_index(src_index, src_ref_lonlat, src_step, target_ref_lonlat, target_step):
    """
    find target data array's y/x index by source data array's y/x index with upper left position's longitude/latitude
    :param src_index: source data's x/y index
    :param src_ref_lonlat: source data
    :param src_step:
    :param target_ref_lonlat:
    :param target_step:
    :return:
    """
    the_middle_thing = index_to_lonlat_v2(src_index, src_ref_lonlat, src_step)
    the_thing = lonlat_to_index_v2(target_ref_lonlat, the_middle_thing, target_step)
    return the_thing


def coloring_bg_image_old_school_way(bg_geo_extent_ldru, bg_union_index_ldru, bg_r, bg_g, bg_b, bg_lat_step,
                                     bg_lon_step, data_geo_extent_ldru, data_lat_step, data_lon_step, data_2d_array,
                                     color_break_points, color_ramp, legend_path, legend_offset):
    """
    coloring background image pixels (the way that I do nc to data tiffs)
    :param bg_geo_extent_ldru: background's geo longitude/latitude extent [left, down, right, up]
    :param bg_union_index_ldru: index extent that background shares with data image
    :param bg_r: background image's red band
    :param bg_g: ...green band
    :param bg_b: ...blue band
    :param bg_lat_step: background's latitude per one background image's pixel
    :param bg_lon_step: ...longtitude per that pixel
    :param data_geo_extent_ldru: data's geo longitude/latitude extent [left, down, right, up]
    :param data_lat_step: yes, the latitude size per data's height element
    :param data_lon_step: ...the width element
    :param data_2d_array: content of data
    :param color_break_points: refer to DataProperty.(SomeData).Split in config.py
    :param color_ramp: refer to DataProperty.(SomeData).Color in config.py
    :param legend_path: yes (it can be None though)
    :param legend_offset: position offset in pixel
    :return: colored rgb band: [r, g, b]
    """
    print("coloring image..", end="")
    lon_index_min, lat_index_max, lon_index_max, lat_index_min = bg_union_index_ldru
    out_bg_r = bg_r
    out_bg_g = bg_g
    out_bg_b = bg_b
    statistic = np.zeros(len(color_ramp))
    for lat_index in range(lat_index_min, lat_index_max):
        for lon_index in range(lon_index_min, lon_index_max):
            pixel_color = (
                out_bg_r[lat_index, lon_index], out_bg_g[lat_index, lon_index], out_bg_b[lat_index, lon_index])
            if pixel_color != DefaultColor.REPLACE_TARGET:
                continue
            lat = index_to_lonlat_v2(lat_index, bg_geo_extent_ldru[3], bg_lat_step)
            lon = index_to_lonlat_v2(lon_index, bg_geo_extent_ldru[0], bg_lon_step)
            nc_lat_index = lonlat_to_index_v2(data_geo_extent_ldru[3], lat, data_lat_step)
            nc_lon_index = lonlat_to_index_v2(data_geo_extent_ldru[0], lon, data_lon_step)
            break_point = 0
            rain_value = data_2d_array[nc_lat_index, nc_lon_index]
            break_values_len = len(color_break_points)
            while break_point <= break_values_len:
                if rain_value < color_break_points[break_point]:
                    out_bg_r[lat_index, lon_index], out_bg_g[lat_index, lon_index], out_bg_b[lat_index, lon_index] = \
                        color_ramp[break_point]  # return color_ramp if value match
                    statistic[break_point] += 1
                    break
                break_point += 1
                if break_point == break_values_len:
                    out_bg_r[lat_index, lon_index], out_bg_g[lat_index, lon_index], out_bg_b[lat_index, lon_index] = \
                        color_ramp[break_point]  # return last color_ramp if value is larger than the biggest
                    statistic[break_point] += 1
                    break
    y, x = bg_g.shape
    if legend_path is None or not give_warning_of_existed_file(legend_path, True):
        print(f"\n\tINFO: legend image not found {legend_path}...")
    elif os.path.isdir(legend_path):
        print(f"\n\tINFO: legend image path is a directory {legend_path}...")
    else:
        im = Image.open(legend_path)
        pix = im.load()
        for y_index in range(0, im.size[1]):
            for x_index in range(0, im.size[0]):
                # bg_x_index = x_index + legend_position_offset
                # bg_y_index = bg_height - im.size[1] - legend_position_offset
                bg_x_index = x_index
                bg_y_index = y - im.size[1] - legend_offset + y_index
                out_bg_r[bg_y_index, bg_x_index], out_bg_g[bg_y_index, bg_x_index], out_bg_b[bg_y_index, bg_x_index] = \
                    pix[x_index, y_index][0:3]
            # print(f"legend {y_index} pf {im.size[1]} completed")
    y = y - 1
    x = x - 1
    out_bg_r[y, x], out_bg_g[y, x], out_bg_b[y, x] = (35, 106, 158)  # easter egg
    return out_bg_r, out_bg_g, out_bg_b, statistic


def coloring_bg_image(focusing_index_extent_ldru, bg_r, bg_g, bg_b, data_content, data_color_break_points,
                      data_color_ramp,
                      legend_path, legend_position_offset, bg_height, bg_width):
    """
    coloring background image pixels
    :param focusing_index_extent_ldru: union part's index extent of background array and data array
    :param bg_r: background image's red band
    :param bg_g: ...green band
    :param bg_b: ...blue band
    :param data_content: data array (size must match with focusing_index_extent_ldru, direction must match with background)
    :param data_color_break_points: refer to config's break point
    :param data_color_ramp: refer to config's color ramp
    :param legend_path: yes
    :param legend_position_offset: not using rn
    :param bg_height: background image's height
    :param bg_width: ...width
    :return: colored rgb band: [r, g, b]
    """
    im = Image.open(legend_path)
    pix = im.load()
    last_color_index = len(data_color_break_points)
    statistics = np.zeros(len(data_color_ramp))
    for y_index in range(focusing_index_extent_ldru[3], focusing_index_extent_ldru[1]):
        for x_index in range(focusing_index_extent_ldru[0], focusing_index_extent_ldru[2]):
            if (bg_r[y_index, x_index], bg_g[y_index, x_index],
                bg_b[y_index, x_index]) != DefaultColor.REPLACE_TARGET:
                continue
            for color_index in range(last_color_index):
                if data_content[y_index - focusing_index_extent_ldru[3], x_index - focusing_index_extent_ldru[0]] < \
                        data_color_break_points[color_index]:
                    bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index] = \
                        data_color_ramp[color_index]
                    statistics[color_index] += 1
                    break
                if color_index + 1 == last_color_index:
                    bg_r[y_index, x_index], bg_g[y_index, x_index], bg_b[y_index, x_index] = \
                        data_color_ramp[last_color_index]
                    statistics[color_index] += 1
    for y_index in range(0, im.size[1]):
        for x_index in range(0, im.size[0]):
            # bg_x_index = x_index + legend_position_offset
            # bg_y_index = bg_height - im.size[1] - legend_position_offset
            bg_x_index = x_index
            bg_y_index = y_index
            bg_r[bg_y_index, bg_x_index], bg_g[bg_y_index, bg_x_index], bg_b[bg_y_index, bg_x_index] = \
                pix[x_index, y_index][0:3]
        # print(f"legend {y_index} pf {im.size[1]} completed")
    return bg_r, bg_g, bg_b


def union_index_calculation(array1_geo_ldru, array2_geo_ldru, array1_lon_step, array1_lat_step, array2_lon_step,
                            array2_lat_step, array1_lon_size, array1_lat_size):
    """
    Calculate background array and data array's union part, and return corresponding index range of both array
    *ldru means [left, down, right, up]
    :param array1_geo_ldru: background array longitude and latitude extent
    :param array2_geo_ldru: data array longitude and latitude extent
    :param array1_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param array1_lat_step: size of latitude equivalent to one background image's width pixel (lon resolution?)
    :param array2_lon_step: size of longitude equivalent to one data image's width pixel (lon resolution?)
    :param array2_lat_step: size of latitude equivalent to one data image's width pixel (lon resolution?)
    :param array1_lon_size: background image's width
    :param array1_lat_size: background image's height
    :return: union part's index extent of both array, [[background's index ldru], [data's index ldru]]
    """
    # calculate array1's union index
    array2_lat_max, array2_lat_min = np.where(array2_geo_ldru[3] > array2_geo_ldru[1],
                                              (array2_geo_ldru[3], array2_geo_ldru[1]),
                                              (array2_geo_ldru[1], array2_geo_ldru[3]))
    array1_lat_index_min = round((array1_geo_ldru[3] - array2_lat_max) / abs(array1_lat_step), ndigits=None)
    array1_lat_index_min = np.where(array1_lat_index_min <= 0, 0, array1_lat_index_min - 1)
    array1_lat_index_max = round((array1_geo_ldru[3] - array2_lat_min) / abs(array1_lat_step), ndigits=None)
    array1_lat_index_max = np.where(array1_lat_index_max > array1_lat_size, array1_lat_size, array1_lat_index_max)
    array2_lon_max, array2_lon_min = np.where(array2_geo_ldru[2] > array2_geo_ldru[0],
                                              (array2_geo_ldru[2], array2_geo_ldru[0]),
                                              (array2_geo_ldru[0], array2_geo_ldru[2]))
    array1_lon_index_min = round((array2_lon_min - array1_geo_ldru[0]) / array1_lon_step, ndigits=None)
    array1_lon_index_min = np.where(array1_lon_index_min < 0, 0, array1_lon_index_min)
    array1_lon_index_max = round((array2_lon_max - array1_geo_ldru[0]) / array1_lon_step, ndigits=None)
    array1_lon_index_max = np.where(array1_lon_index_max > array1_lon_size, array1_lon_size, array1_lon_index_max)
    # array2's union index
    array2_lat_index_min = source_index_to_target_index(array1_lat_index_min, array1_geo_ldru[3], array1_lat_step,
                                                        array2_geo_ldru[3], array2_lat_step)
    array2_lat_index_max = source_index_to_target_index(array1_lat_index_max, array1_geo_ldru[3], array1_lat_step,
                                                        array2_geo_ldru[3], array2_lat_step)
    array2_lat_index_min, array2_lat_index_max = np.where(array2_lat_index_min < array2_lat_index_max,
                                                          (array2_lat_index_min, array2_lat_index_max),
                                                          (array2_lat_index_max, array2_lat_index_min))
    array2_lon_index_min = source_index_to_target_index(array1_lon_index_min, array1_geo_ldru[0], array1_lon_step,
                                                        array2_geo_ldru[0], array2_lon_step)
    array2_lon_index_max = source_index_to_target_index(array1_lon_index_max, array1_geo_ldru[0], array1_lon_step,
                                                        array2_geo_ldru[0], array2_lon_step)
    array2_lon_index_min, array2_lon_index_max = np.where(array2_lat_index_min < array2_lat_index_max,
                                                          (array2_lon_index_min, array2_lon_index_max),
                                                          (array2_lon_index_max, array2_lon_index_min))
    return [[array1_lon_index_min, array1_lat_index_max, array1_lon_index_max, array1_lat_index_min],
            [array2_lon_index_min, array2_lat_index_max, array2_lon_index_max, array2_lat_index_min]]


def rain_process_v2(bg_r, bg_g, bg_b,
                    bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                    data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                    data_lon_step, data_lat_step, report_path):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent RELATIVE TO it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :param report_path: path of extracted report file
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    print("Processing rain")
    current_time_index = 0
    rain_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    bg_union_index_ldru, nc_union_index_ldru = union_index_calculation(array1_geo_ldru=bg_extent_ldru,
                                                                       array2_geo_ldru=data_overlay_ldru,
                                                                       array1_lon_step=bg_lon_step,
                                                                       array1_lat_step=bg_lat_step,
                                                                       array2_lon_step=data_lon_step,
                                                                       array2_lat_step=data_lat_step,
                                                                       array1_lon_size=bg_lon_size,
                                                                       array1_lat_size=bg_lat_size)
    while current_time_index + data_time_index_step < len(data_time_array):
        print(f"\r\tcalculating rain {current_time_index}h~{current_time_index + data_time_index_step}h..", end="")
        rain_array = np.zeros(rain_array_shape)
        for time in range(current_time_index, current_time_index + data_time_index_step):
            rain_array = np.add(rain_array, data_3d_array[time, :, :])

        out_bg_r, out_bg_g, out_bg_b, stat = coloring_bg_image_old_school_way(bg_geo_extent_ldru=bg_extent_ldru,
                                                                              bg_union_index_ldru=bg_union_index_ldru,
                                                                              bg_r=np.array(bg_r), bg_g=np.array(bg_g),
                                                                              bg_b=np.array(bg_b),
                                                                              bg_lat_step=bg_lat_step,
                                                                              bg_lon_step=bg_lon_step,
                                                                              data_geo_extent_ldru=data_overlay_ldru,
                                                                              data_lat_step=data_lat_step,
                                                                              data_lon_step=data_lon_step,
                                                                              data_2d_array=rain_array,
                                                                              color_break_points=DataProperty.Rain.Split,
                                                                              color_ramp=DataProperty.Rain.Color,
                                                                              legend_path=DataProperty.Rain.LegendPath,
                                                                              legend_offset=5)
        output_rgb_image_array.append((out_bg_r, out_bg_g, out_bg_b))
        global need_report
        if need_report:
            date_replacer = drt.DocxKeyWords.day_x_keyword
            day_x = int(current_time_index / data_time_index_step)
            rain_day_x_keyword = drt.DocxKeyWords.day_x_rain.replace(date_replacer, str(day_x))
            rain_day_x_addition_keyword = drt.DocxKeyWords.day_x_rain_addition.replace(date_replacer, str(day_x))
            maj, low, high = give_majority_range_name(statistic=stat,
                                                      friend_range_name=DataProperty.Rain.FriendlySplitRangeName,
                                                      friendly_in_between_text=DataProperty.Rain.FriendlySplitTextInBetween,
                                                      require_end_stat=True)
            update_report(content_path=report_path, keyword=rain_day_x_keyword, updated_content=maj)
            if high is None:
                high_str = ""
            else:
                high_str = DataProperty.Rain.AdditionalInformationOfHighest.format(high)
            update_report(content_path=report_path, keyword=rain_day_x_addition_keyword, updated_content=high_str)
        current_time_index += data_time_index_step
        print("done")
        # break
    print()
    remain = len(data_time_array) - current_time_index
    if remain > 0:
        print(f"INFO ignoring remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def temperature_process(bg_r, bg_g, bg_b,
                        bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                        data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                        data_lon_step, data_lat_step, report_path):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent RELATIVE TO it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :param report_path: path of extracted report file
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    print("Processing temperature")
    current_time_index = 0
    temp_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    bg_union_index_ldru, nc_union_index_ldru = union_index_calculation(array1_geo_ldru=bg_extent_ldru,
                                                                       array2_geo_ldru=data_overlay_ldru,
                                                                       array1_lon_step=bg_lon_step,
                                                                       array1_lat_step=bg_lat_step,
                                                                       array2_lon_step=data_lon_step,
                                                                       array2_lat_step=data_lat_step,
                                                                       array1_lon_size=bg_lon_size,
                                                                       array1_lat_size=bg_lat_size)

    time_section = 0  # an hour offset indicating which part of data to extract
    while time_section + data_time_index_step < len(data_time_array):
        time_section_end = time_section + data_time_index_step
        temp_array = np.zeros(temp_array_shape)
        for iLon in range(temp_array_shape[1]):
            print(f"\r\t{time_section}h~{time_section_end}h, {iLon} of {temp_array_shape[1]} complete..", end="")
            for iLat in range(temp_array_shape[0]):
                temp_range = np.array(data_3d_array[time_section:time_section + data_time_index_step, iLat, iLon])
                temp_array[iLat, iLon] = temp_range.max()
            # print(f"{iLat+1} of {N_Lat}", end="\r")
        out_bg_r, out_bg_g, out_bg_b, stat = coloring_bg_image_old_school_way(bg_geo_extent_ldru=bg_extent_ldru,
                                                                              bg_union_index_ldru=bg_union_index_ldru,
                                                                              bg_r=np.array(bg_r), bg_g=np.array(bg_g),
                                                                              bg_b=np.array(bg_b),
                                                                              bg_lat_step=bg_lat_step,
                                                                              bg_lon_step=bg_lon_step,
                                                                              data_geo_extent_ldru=data_overlay_ldru,
                                                                              data_lat_step=data_lat_step,
                                                                              data_lon_step=data_lon_step,
                                                                              data_2d_array=temp_array,
                                                                              color_break_points=DataProperty.Temperature.Split,
                                                                              color_ramp=DataProperty.Temperature.Color,
                                                                              legend_path=DataProperty.Temperature.LegendPath,
                                                                              legend_offset=5)
        output_rgb_image_array.append((out_bg_r, out_bg_g, out_bg_b))
        global need_report
        if need_report:
            day_replacer = drt.DocxKeyWords.day_x_keyword
            day_x = int(time_section / data_time_index_step)
            temp_day_x_keyword = drt.DocxKeyWords.day_x_temperature
            temp_day_x_keyword = temp_day_x_keyword.replace(day_replacer, str(day_x))
            temp_day_x_addition_keyword = drt.DocxKeyWords.day_x_temperature_addition.replace(day_replacer, str(day_x))
            maj, low, high = give_majority_range_name(statistic=stat,
                                                      friend_range_name=DataProperty.Temperature.FriendlySplitRangeName,
                                                      friendly_in_between_text=DataProperty.Temperature.FriendlySplitTextInBetween,
                                                      require_end_stat=True)
            update_report(content_path=report_path, keyword=temp_day_x_keyword, updated_content=maj)
            if high is None:
                high_str = ""
            else:
                high_str = DataProperty.Temperature.AdditionalInformationOfHighest.format(high)
            update_report(content_path=report_path, keyword=temp_day_x_addition_keyword, updated_content=high_str)
        time_section += data_time_index_step
        print("done")
    remain = len(data_time_array) - time_section
    if remain > 0:
        print(f"INFO ignoring remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def windspeed_process(bg_r, bg_g, bg_b,
                      bg_extent_ldru, bg_lon_step, bg_lat_step, bg_lon_size, bg_lat_size,
                      data_overlay_ldru, data_3d_array, data_time_index_step, data_time_array,
                      data_lon_step, data_lat_step, report_path):
    """
    Using .nc dataset to process rain data
    :param bg_r: Background image red band
    :param bg_g: Background image green band
    :param bg_b: Background image blue band
    :param bg_extent_ldru: Background image geo extent [left, down, right, up]
    :param bg_lon_step: size of longitude equivalent to one background image's width pixel (lon resolution?)
    :param bg_lat_step: size of latitude equivalent to one background image's height pixel (lat resolution?)
    :param bg_lon_size: background image's width
    :param bg_lat_size: background image's height
    :param data_overlay_ldru: data geo extent RELATIVE TO it's original layout [left, down, right, up]
    :param data_3d_array: 3 dimensional data array [time, latitude, longitude]
    :param data_time_index_step: time span (how time should be divided? every 1 unit or every 24 units or...)
    :param data_time_array: time unit in form of array (only it's size/length is used so far)
    :param data_lon_step: size of latitude equivalent to one data unit's width pixel (lat resolution?)
    :param data_lat_step: size of latitude equivalent to one data unit's height pixel (lat resolution?)
    :param report_path: path of extracted report file
    :return: array of image rgb bands for image output [(r,g,b), (r,g,b), ....]
    """
    print("Processing wind speed")
    current_time_index = 0
    data_array_shape = data_3d_array[0, :, :].shape
    output_rgb_image_array = []  # an array, each element represent an image, in form of [r_array, g_array, b_array]

    # tif file's current processing index -- smaller the lat index, bigger the lat value
    bg_union_index_ldru, nc_union_index_ldru = union_index_calculation(array1_geo_ldru=bg_extent_ldru,
                                                                       array2_geo_ldru=data_overlay_ldru,
                                                                       array1_lon_step=bg_lon_step,
                                                                       array1_lat_step=bg_lat_step,
                                                                       array2_lon_step=data_lon_step,
                                                                       array2_lat_step=data_lat_step,
                                                                       array1_lon_size=bg_lon_size,
                                                                       array1_lat_size=bg_lat_size)

    time_section = 0  # an hour offset indicating which part of data to extract
    while time_section + data_time_index_step < len(data_time_array):
        time_section_end = time_section + data_time_index_step
        windspeed_array = np.zeros(data_array_shape)
        for iLon in range(data_array_shape[1]):
            print(f"\r\t{time_section}h~{time_section_end}h, {iLon} of {data_array_shape[1]} complete..", end="")
            for iLat in range(data_array_shape[0]):
                temp_range = np.array(data_3d_array[time_section:time_section + data_time_index_step, iLat, iLon])
                windspeed_array[iLat, iLon] = temp_range.max()
            # print(f"{iLat+1} of {N_Lat}", end="\r")
        out_bg_r, out_bg_g, out_bg_b, stat = coloring_bg_image_old_school_way(bg_geo_extent_ldru=bg_extent_ldru,
                                                                              bg_union_index_ldru=bg_union_index_ldru,
                                                                              bg_r=np.array(bg_r), bg_g=np.array(bg_g),
                                                                              bg_b=np.array(bg_b),
                                                                              bg_lat_step=bg_lat_step,
                                                                              bg_lon_step=bg_lon_step,
                                                                              data_geo_extent_ldru=data_overlay_ldru,
                                                                              data_lat_step=data_lat_step,
                                                                              data_lon_step=data_lon_step,
                                                                              data_2d_array=windspeed_array,
                                                                              color_break_points=DataProperty.WindSpeed.Split,
                                                                              color_ramp=DataProperty.WindSpeed.Color,
                                                                              legend_path=DataProperty.WindSpeed.LegendPath,
                                                                              legend_offset=5)
        output_rgb_image_array.append((out_bg_r, out_bg_g, out_bg_b))
        global need_report
        if need_report:
            day_replacer = drt.DocxKeyWords.day_x_keyword
            day_x = int(time_section / data_time_index_step)
            wind_day_x_keyword = drt.DocxKeyWords.day_x_wind.replace(day_replacer, str(day_x))
            wind_day_x_addition_keyword = drt.DocxKeyWords.day_x_wind_addition.replace(day_replacer, str(day_x))
            maj, low, high = give_majority_range_name(statistic=stat,
                                                      friend_range_name=DataProperty.WindSpeed.FriendlySplitRangeName,
                                                      friendly_in_between_text=DataProperty.WindSpeed.FriendlySplitTextInBetween,
                                                      require_end_stat=True)
            update_report(content_path=report_path, keyword=wind_day_x_keyword, updated_content=maj)
            if high is None:
                high_str = ""
            else:
                high_str = DataProperty.WindSpeed.AdditionalInformationOfHighest.format(high)
            update_report(content_path=report_path, keyword=wind_day_x_addition_keyword, updated_content=high_str)
        time_section += data_time_index_step
        print("done")
    remain = len(data_time_array) - time_section
    if remain > 0:
        print(f"INFO ignoring remaining {remain} datasets for quantity less than {data_time_index_step}")
    return output_rgb_image_array


def main_process_old(date=datetime.datetime.now()):
    print("weather 0.3.0 (legacy): ready get set go")
    output_directory = Directory.output_directory
    input_directory = Directory().get_weather_nc_directory(date)
    reference_directory = Directory.reference_directory  # background image should be placed here
    background_files = Name.weather_background_tif_image  # background image file
    if not give_warning_of_non_exist_directory(output_directory):
        print_info_text("output folder is being created")
        os.mkdir(output_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print_error_text("input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print_error_text("reference folder not found")
        exit(2)
    # read background image
    bg_dataset = gdal.Open(os.path.join(reference_directory, background_files))
    bg_geo_transform = bg_dataset.GetGeoTransform()
    bg_geo_projection = bg_dataset.GetProjection()
    bg_red = bg_dataset.GetRasterBand(1).ReadAsArray()
    bg_green = bg_dataset.GetRasterBand(2).ReadAsArray()
    bg_blue = bg_dataset.GetRasterBand(3).ReadAsArray()
    bg_width = bg_dataset.RasterXSize
    bg_height = bg_dataset.RasterYSize
    bg_minx = bg_geo_transform[0]
    bg_maxy = bg_geo_transform[3]
    bg_x_step = bg_geo_transform[1]
    bg_y_step = bg_geo_transform[5]
    bg_maxx = bg_minx + bg_x_step * bg_dataset.RasterXSize
    bg_miny = bg_maxy + bg_y_step * bg_dataset.RasterYSize
    bg_extent_ldru = [bg_minx, bg_miny, bg_maxx, bg_maxy]  # background image geo extent, [left, down, right, up]
    print(f"Background image size: {bg_width}x{bg_height}")
    print(f"Background image extent: left={bg_minx}, right={bg_maxx}, top={bg_maxy}, bottom={bg_miny}")
    # NC input file section
    nc_data_path = glob.glob(os.path.join(input_directory, Name.weather_data_file))
    global need_report
    for item in nc_data_path:
        item_date = item.split('_')[-2:-1][0]
        datetime_item_date = datetime.datetime.strptime(item_date, CannotClassify.weather_filename_start_time_format)
        report_file_name = Name().get_weather_nc_report_name(datetime_item_date)
        report_file_path = Directory().get_weather_nc_report_path(datetime_item_date)
        if not Switches.ignore_existing_report:
            report_file_full_path = os.path.join(report_file_path, report_file_name)
            if give_warning_of_existed_file(report_file_full_path, True):
                print_info_text(f"{report_file_name} exists, skipping {item}")
                continue
        extracted_path = load_docx_template(Directory.report_template_legacy_file_path)
        update_report_time(datetime_item_date, extracted_path)
        print(f"Start date of NC dataset is: {datetime_item_date}")
        nc_dataset = nC.Dataset(item)  # 读取nc数据集
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_time_array = np.array(nc_dataset.variables["time"][:])
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_time_step = 24
        nc_overlay_extent_ldru = [nc_lon_array.min(), nc_lat_array.min(), nc_lon_array.max(), nc_lat_array.max()]
        nc_lat_step = (nc_overlay_extent_ldru[3] - nc_overlay_extent_ldru[1]) / nc_lat_array_size
        nc_lon_step = (nc_overlay_extent_ldru[2] - nc_overlay_extent_ldru[0]) / nc_lon_array_size
        nc_overlay_array_ldru = [nc_lon_array[0], nc_lat_array[len(nc_lat_array) - 1],
                                 nc_lon_array[len(nc_lon_array) - 1], nc_lat_array[0]]
        # rain section
        images = rain_process_v2(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                 bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                 bg_lat_size=bg_height,
                                 data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                 data_3d_array=nc_dataset.variables[DataProperty.Rain.NCFieldTag][:],
                                 data_lat_step=nc_lat_step, data_lon_step=nc_lon_step, data_time_array=nc_time_array,
                                 report_path=extracted_path)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            if save_picture:
                filename_to_save = f"{filename}_{DataProperty.Rain.FriendlyName}_{time_index}-{time_index + nc_time_step}h.tif"
                save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            if need_report:
                filename_to_save = drt.DocxKeyWords.tif_image_day_x.replace(
                    drt.DocxKeyWords.day_x_keyword, str(int(time_index / nc_time_step * 3) + 2)
                )
                filepath_to_save = os.path.join(extracted_path, drt.DefaultDirectories.image_path)
                save_rgb_tif(image_rgb, filepath_to_save, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step

        # temperature section
        images = temperature_process(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                     bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                     bg_lat_size=bg_height,
                                     data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                     data_3d_array=nc_dataset.variables[DataProperty.Temperature.NCFieldTag][:],
                                     data_lat_step=nc_lat_step, data_lon_step=nc_lon_step,
                                     data_time_array=nc_time_array, report_path=extracted_path)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            if save_picture:
                filename_to_save = f"{filename}_{DataProperty.Temperature.FriendlyName}_{time_index}-{time_index + nc_time_step}h.tif"
                save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            if need_report:
                filename_to_save = drt.DocxKeyWords.tif_image_day_x.replace(
                    drt.DocxKeyWords.day_x_keyword, str(int(time_index / nc_time_step * 3) + 3)
                )
                filepath_to_save = os.path.join(extracted_path, drt.DefaultDirectories.image_path)
                save_rgb_tif(image_rgb, filepath_to_save, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step

        # wind speed section
        wind_speed_3d_array = nc_dataset.variables[DataProperty.WindSpeed.NCFieldTag][:, 0, :, :]
        images = windspeed_process(bg_r=bg_red, bg_g=bg_green, bg_b=bg_blue, bg_extent_ldru=bg_extent_ldru,
                                   bg_lon_step=bg_x_step, bg_lat_step=bg_y_step, bg_lon_size=bg_width,
                                   bg_lat_size=bg_height,
                                   data_overlay_ldru=nc_overlay_array_ldru, data_time_index_step=nc_time_step,
                                   data_3d_array=wind_speed_3d_array,
                                   data_lat_step=nc_lat_step, data_lon_step=nc_lon_step, data_time_array=nc_time_array,
                                   report_path=extracted_path)
        time_index = 0
        filename = get_timestamp_filename(nc_time_array[0])
        for image_rgb in images:
            if save_picture:
                filename_to_save = f"{filename}_{DataProperty.WindSpeed.FriendlyName}_{time_index}-{time_index + nc_time_step}h.tif"
                save_rgb_tif(image_rgb, output_directory, bg_geo_transform, bg_geo_projection, filename_to_save)
            if need_report:
                filename_to_save = drt.DocxKeyWords.tif_image_day_x.replace(
                    drt.DocxKeyWords.day_x_keyword, str(int(time_index / nc_time_step * 3) + 4)
                )
                filepath_to_save = os.path.join(extracted_path, drt.DefaultDirectories.image_path)
                save_rgb_tif(image_rgb, filepath_to_save, bg_geo_transform, bg_geo_projection, filename_to_save)
            time_index += nc_time_step
        if need_report:
            zip_docx_report(output_path=report_file_path,
                            output_file_name=report_file_name,
                            content_path=extracted_path)
    clean_temporary_folder()
    if len(nc_data_path) == 0:
        print_info_text("no input .nc file found")
    print("weather: and stop")


def forge_weather_2d_data(data_array_3d, friendly_field_name, time_step,
                          flip_horizontal, flip_vertical, time_array_len, lat_array_size, lon_array_size):
    print(f"Extracting {friendly_field_name} data...")
    j = 0
    data = list()
    if friendly_field_name in [DataProperty.Rain.FriendlyName]:
        while j + time_step <= time_array_len:
            some_array = np.zeros((lat_array_size, lon_array_size))
            for i in range(j, j + time_step):
                some_array = np.add(some_array, data_array_3d[i, :, :])
            if flip_vertical:
                some_array = np.flip(some_array, 0)
            if flip_horizontal:
                some_array = np.flip(some_array, 1)
            data.append(some_array)
            j += time_step
        if time_array_len - j > 0:
            print_info_text(
                f"ditching remaining {time_array_len - j} {'sets' if time_array_len - j > 1 else 'set'} of data "
                f"for quantity less than {time_step}")
    elif friendly_field_name in [DataProperty.Temperature.FriendlyName, DataProperty.WindSpeed.FriendlyName]:
        while j + time_step <= time_array_len:
            some_array = np.zeros((lat_array_size, lon_array_size))
            for i in range(lat_array_size):
                for k in range(lon_array_size):
                    some_array[i, k] = np.max(data_array_3d[j:j + time_step - 1, i, k])
            if flip_vertical:
                some_array = np.flip(some_array, 0)
            if flip_horizontal:
                some_array = np.flip(some_array, 1)
            data.append(some_array)
            j += time_step
        if time_array_len - j > 0:
            print_info_text(
                f"ditching remaining {time_array_len - j} {'sets' if time_array_len - j > 1 else 'set'} of data "
                f"for quantity less than {time_step}")
    else:
        print_error_text(f"cannot generate weather data, unknown field name {friendly_field_name}")
        return None
    print_success_text(f"Extracted {len(data)} {'sets' if len(data) > 1 else 'set'} of {friendly_field_name} data")
    return data


def data_post_process(list_of_data, date, friendly_name, start_timestamp, time_step, time_unit, geo_transformation,
                      geo_projection, temporary_directory, output_directory,
                      report_image_index_offset=None, report_extracted_path=None):
    time_index = 0
    for rain_image_data in list_of_data:
        tif_filename = f"{start_timestamp}_{friendly_name}_{time_index}{time_unit}-{time_index + time_step}{time_unit}.tif"
        save_tif(data_array_2d_s=rain_image_data, path_s=temporary_directory,
                 geo_transform_s=geo_transformation, geo_projection_s=geo_projection, filename_s=tif_filename)

        if (not need_png and not need_report) or need_source_tif:
            copy(
                os.path.join(temporary_directory, tif_filename),
                os.path.join(output_directory, tif_filename)
            )
            time_index += time_step
            if not need_png and not need_report:
                continue

        _, name, ext, _ = get_file_path_name_extension(tif_filename)
        resample_name = f'{name}_resampled'
        resample_filename = f'{resample_name}.{ext}'
        code = arcpy_resample_helper(os.path.abspath(os.path.join(temporary_directory, tif_filename)),
                                     0.01,
                                     os.path.abspath(os.path.join(temporary_directory, resample_filename)))
        if code != 0:
            print_error_text(f"resample failed, cannot continue")
            time_index += time_step
            continue
        _, name, ext, _ = get_file_path_name_extension(tif_filename)
        ebm_filename = f'{resample_name}_extracted-by-mask.{ext}'
        code = arcpy_extract_by_mask_helper(
            os.path.abspath(os.path.join(temporary_directory, resample_filename)),
            os.path.abspath(os.path.join(Directory().get_weather_mxd_template_directory(), Name.weather_mask_file)),
            os.path.abspath(os.path.join(temporary_directory, ebm_filename))
        )
        if code != 0:
            print_error_text(f"extract by mask failed, cannot continue")
            time_index += time_step
            continue
        if friendly_name == DataProperty.Rain.FriendlyName:
            mxd_name = Name.weather_rain_mxd_temlate
        elif friendly_name == DataProperty.Temperature.FriendlyName:
            mxd_name = Name.weather_temperature_mxd_temlate
        elif friendly_name == DataProperty.WindSpeed.FriendlyName:
            mxd_name = Name.weather_wind_speed_mxd_temlate
        else:
            print_error_text(f"unrecognizable friendly name {friendly_name}")
            time_index += time_step
            continue
        mxd_abs_path = os.path.abspath(
            os.path.join(Directory().get_weather_mxd_template_directory(), mxd_name)
        )
        _, name, _, _ = get_file_path_name_extension(tif_filename)
        png_filename = f"{name}.png"
        png_filepath = temporary_directory
        png_file_full_path = os.path.join(png_filepath, png_filename)
        code = arcpy_mxd_to_png_helper(mxd_abs_path,
                                       [os.path.abspath(os.path.join(temporary_directory, ebm_filename))],
                                       os.path.abspath(png_file_full_path),
                                       [1])
        if code != 0:
            print_error_text(f"png image export failed, cannot continue")
            time_index += time_step
            continue
        if need_png:
            copy(png_file_full_path, os.path.join(output_directory, png_filename))
        if need_report:
            filename_to_save = drt.DocxKeyWords.image_day_x.replace(
                drt.DocxKeyWords.day_x_keyword, str(int(time_index / time_step * 3) + report_image_index_offset)
            ).format("png")
            report_image_replace(png_file_full_path, filename_to_save, report_extracted_path)
        time_index += time_step


def get_weather_data(date=datetime.datetime.now()):
    # this function is used to download today's weather data only (default)
    # subject 'today' is defined as 'at the date of running this script'
    # or can set to some specific date as wish
    print(f"Downloading weather data for date {date}")
    login_info = FTPLoginInfo(
        port=CannotClassify.ftp_port,
        address=CannotClassify.ftp_address,
        username=CannotClassify.ftp_username,
        password=CannotClassify.ftp_password
    )
    # date = date - datetime.timedelta(days=20)  # test only
    dir_set = Directory()
    remote_path = dir_set.get_expected_remote_weather_data(date)
    local_path = dir_set.get_weather_nc_directory(date)
    ftp_download_process(local_path, login_info, remote_path)


def main_process(date=datetime.datetime.now(), use_legacy_method=False):
    print("weather component starting...")
    if use_legacy_method:
        return main_process_old(date)

    if not isinstance(date, datetime.datetime):
        print_error_text(f"given date is {type(date)}, expect {type(datetime.datetime)}")
        exit(3)
    output_directory = Directory().get_weather_nc_report_path(date)
    input_directory = Directory().get_weather_nc_directory(date)
    reference_directory = Directory.reference_directory
    temporary_directory = os.path.join(Directory.temporary_directory, 'weather work folder')
    if not give_warning_of_non_exist_directory(output_directory):
        print_info_text("output folder is being created")
        os.makedirs(output_directory)
    if not give_warning_of_non_exist_directory(temporary_directory):
        print_info_text("creating working directory")
        os.makedirs(temporary_directory)
    elif len(os.listdir(temporary_directory)) > 0:
        print_info_text("cleaning working directory")
        clean_temporary_folder(temporary_directory)
    if not give_warning_of_non_exist_directory(input_directory):
        print_error_text("input folder not found")
        exit(1)
    if not give_warning_of_non_exist_directory(reference_directory):
        print_error_text("reference folder not found")
        exit(2)
    nc_data_path = glob.glob(os.path.join(input_directory, Name.weather_data_file))
    for item in nc_data_path:
        print(f"Processing nc dataset {item}")
        item_date = item.split('_')[-2:-1][0]
        datetime_item_date = datetime.datetime.strptime(item_date, CannotClassify.weather_filename_start_time_format)
        report_file_name = Name().get_weather_nc_report_name(datetime_item_date)
        report_file_path = output_directory
        '''
        if not Switches.ignore_existing_report and need_report:
            report_file_full_path = os.path.join(report_file_path, report_file_name)
            if give_warning_of_existed_file(report_file_full_path, True):
                print(f"INFO {report_file_name} exists, skipping {item}")
                continue
        '''
        extracted_path = None
        if need_report:
            extracted_path = load_docx_template(Directory.report_template_file_path)
        print(f"Start date of NC dataset is: {datetime_item_date}")
        nc_dataset = nC.Dataset(item)  # 读取nc数据集
        nc_lon_array = np.array(nc_dataset.variables["lon"][:])  # 经度
        nc_lat_array = np.array(nc_dataset.variables["lat"][:])  # 维度
        nc_time_array = np.array(nc_dataset.variables["time"][:])
        nc_lon_array_size = len(nc_lon_array)
        nc_lat_array_size = len(nc_lat_array)
        nc_time_step = 24
        nc_time_unit = 'h'
        nc_start_datetime = utc_timestamp_sec_to_datetime_since_1990(nc_time_array[0])
        nc_lat_index_max, nc_lat_index_min = nc_lat_array[nc_lat_array_size - 1], nc_lat_array[0]
        nc_lat_step = (nc_lat_index_max - nc_lat_index_min) / (nc_lat_array_size - 1)
        nc_lat_need_flip = nc_lat_step > 0
        nc_lat_top = nc_lat_index_max if nc_lat_need_flip else nc_lat_index_min
        nc_lon_index_max, nc_lon_index_min = nc_lon_array[nc_lon_array_size - 1], nc_lon_array[0]
        nc_lon_step = (nc_lon_index_max - nc_lon_index_min) / (nc_lon_array_size - 1)
        nc_lon_need_flip = nc_lon_step < 0
        nc_lon_left = nc_lon_index_min if not nc_lon_need_flip else nc_lon_index_max
        geo_transformation = (nc_lon_left, nc_lon_step if not nc_lon_need_flip else -nc_lon_step, 0, nc_lat_top, 0,
                              -nc_lat_step if nc_lat_need_flip else nc_lat_step)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)  # 定义输出的坐标系为"WGS 84"，AUTHORITY["EPSG","4326"]
        geo_projection = srs.ExportToWkt()
        timestamp = get_timestamp_filename(utc_timestamp_sec_to_datetime_since_1990(nc_time_array[0]))

        # rain data process
        rain_data = forge_weather_2d_data(data_array_3d=nc_dataset[DataProperty.Rain.NCFieldTag][:, :, :],
                                          friendly_field_name=DataProperty.Rain.FriendlyName,
                                          time_step=nc_time_step, flip_horizontal=nc_lon_need_flip,
                                          flip_vertical=nc_lat_need_flip, time_array_len=len(nc_time_array),
                                          lat_array_size=nc_lat_array_size, lon_array_size=nc_lon_array_size)
        data_post_process(list_of_data=rain_data, date=date, friendly_name=DataProperty.Rain.FriendlyName,
                          start_timestamp=timestamp, time_step=nc_time_step, time_unit=nc_time_unit,
                          geo_transformation=geo_transformation, geo_projection=geo_projection,
                          temporary_directory=temporary_directory, output_directory=output_directory,
                          report_image_index_offset=2, report_extracted_path=extracted_path)
        temperature_data = forge_weather_2d_data(data_array_3d=nc_dataset[DataProperty.Temperature.NCFieldTag][:, :, :],
                                                 friendly_field_name=DataProperty.Temperature.FriendlyName,
                                                 time_step=nc_time_step, flip_horizontal=nc_lon_need_flip,
                                                 flip_vertical=nc_lat_need_flip, time_array_len=len(nc_time_array),
                                                 lat_array_size=nc_lat_array_size, lon_array_size=nc_lon_array_size)
        data_post_process(list_of_data=temperature_data, date=date, friendly_name=DataProperty.Temperature.FriendlyName,
                          start_timestamp=timestamp, time_step=nc_time_step, time_unit=nc_time_unit,
                          geo_transformation=geo_transformation, geo_projection=geo_projection,
                          temporary_directory=temporary_directory, output_directory=output_directory,
                          report_image_index_offset=3, report_extracted_path=extracted_path)
        wind_speed_data = forge_weather_2d_data(data_array_3d=nc_dataset[DataProperty.WindSpeed.NCFieldTag][:, 0, :, :],
                                                friendly_field_name=DataProperty.WindSpeed.FriendlyName,
                                                time_step=nc_time_step, flip_horizontal=nc_lon_need_flip,
                                                flip_vertical=nc_lat_need_flip, time_array_len=len(nc_time_array),
                                                lat_array_size=nc_lat_array_size, lon_array_size=nc_lon_array_size)
        data_post_process(list_of_data=wind_speed_data, date=date, friendly_name=DataProperty.WindSpeed.FriendlyName,
                          start_timestamp=timestamp, time_step=nc_time_step, time_unit=nc_time_unit,
                          geo_transformation=geo_transformation, geo_projection=geo_projection,
                          temporary_directory=temporary_directory, output_directory=output_directory,
                          report_image_index_offset=4, report_extracted_path=extracted_path)
        if need_report:
            update_report_time(datetime_item_date, extracted_path)
            fill_report_weather_description(extracted_path, DataProperty.Rain.AdditionalInformationOfHighest,
                                            DataProperty.Rain.FriendlySplitRangeName,
                                            ReportProperties.KeyWords.day_x_rain,
                                            ReportProperties.KeyWords.day_x_rain_addition,
                                            DataProperty.Rain.FriendlySplitTextInBetween,
                                            rain_data, DataProperty.Rain.Split)
            fill_report_weather_description(extracted_path, DataProperty.Temperature.AdditionalInformationOfHighest,
                                            DataProperty.Temperature.FriendlySplitRangeName,
                                            ReportProperties.KeyWords.day_x_temperature,
                                            ReportProperties.KeyWords.day_x_temperature_addition,
                                            DataProperty.Temperature.FriendlySplitTextInBetween,
                                            temperature_data, DataProperty.Temperature.Split)
            fill_report_weather_description(extracted_path, DataProperty.WindSpeed.AdditionalInformationOfHighest,
                                            DataProperty.WindSpeed.FriendlySplitRangeName,
                                            ReportProperties.KeyWords.day_x_wind,
                                            ReportProperties.KeyWords.day_x_wind_addition,
                                            DataProperty.WindSpeed.FriendlySplitTextInBetween,
                                            wind_speed_data, DataProperty.WindSpeed.Split)
            zip_docx_report(output_path=report_file_path,
                            output_file_name=report_file_name,
                            content_path=extracted_path)
            clean_temporary_folder(extracted_path)
    clean_temporary_folder(temporary_directory)
    print("Weather component is finishing...")


need_report = Switches.need_report
need_png = Switches.save_png_image
need_source_tif = Switches.save_source_tif
save_picture = Switches.save_rgb_tif

if __name__ == "__main__":
    print("weather is saying hi :)")
    get_weather_data()
    main_process()
    exit(0)
